#ifndef PROCESS_H
#define PROCESS_H

#include <QString>

class DetachedProcess{
public:
    DetachedProcess();
    bool start(const QString &command, QStringList &params);
};

#endif // PROCESS_H
