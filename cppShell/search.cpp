#include "search.h"
#include "consoleParams.h"

extern QList<QString> consoleParams;

SimpleSearch::SimpleSearch(QJsonArray fileData, QString contentDir, EncryptedStorage *contentStorage){
    storage = contentStorage;
    delimiters << '\t' << ' ' << '\n' << '\r' << '.' << ',' << ';' << '„' << '-' << '—';

    QMap<int,QString> textDict;
    this->contentDir = contentDir;

    if(consoleParams.contains(ConsoleParams::FROM_FOLDER)){
        for(int i = 0; i < fileData.size(); i++){
            QFile htmlFile(ROOT_PATH + "html/content/" + this->contentDir + "/" + fileData[i].toObject().value("name").toString() + ".html");
            if(htmlFile.open(QIODevice::ReadOnly | QIODevice::Text)){
                QString html;
                while (!htmlFile.atEnd()) {
                    html += htmlFile.readLine();
                }

                textDict[fileData[i].toObject().value("id").toDouble()] = cleaningText(html);
            }else{
                continue;
            }
        }
    } else {
        for(int i = 0; i < fileData.size(); i++){
            QString url = "html/content/" + this->contentDir + "/" + fileData[i].toObject().value("name").toString() + ".html";
            QString html = QString::fromUtf8(storage->getFile(url.toLower()));

            textDict[fileData[i].toObject().value("id").toDouble()] = cleaningText(html);
        }
    }

    preparedText = textDict;
}

QString SimpleSearch::cleaningText(QString html){
    QRegExp rxHeader("<head>(.*)</head>");
    QRegExp rxHtmlCode("&[a-z]*;");

    html.replace("\u00AD", "").replace("\u00A0", " ").replace("&nbsp;", " ");
    html.remove(rxHeader).remove(rxHtmlCode);
    QXmlStreamReader xml(html);
    QString cleanText;
    while (!xml.atEnd()) {
        if ( xml.readNext() == QXmlStreamReader::Characters ) {
            cleanText += xml.text();
        }
    }
    return cleanText;
}

QJsonObject SimpleSearch::find(QString phrase){
    QJsonObject result;
    QStringList wordsList;
    QMapIterator<int,QString> preparedTextIterator(preparedText);
    wordsList = cleanPhrase(phrase);

    while (preparedTextIterator.hasNext()) {
        preparedTextIterator.next();
        QString sample;
        sample = getSample(preparedTextIterator.value(), wordsList);
        if (!sample.isEmpty()) result[QString::number(preparedTextIterator.key())] = sample;
    }

    return result;
}

QString SimpleSearch::getSample(QString sourceText, QStringList wordsList){
    QString sample;
    QMap<QString, int> sentencesPower; //словарик c количеством искомых слов в каждом из найденных предложений
    QList<QPair<int,QString>> sortedSentences;
    QMap<QString, QMap<QString, bool>> words;
    int sentenceCount = 0;
    int start = 0;
    for(int i = 0; i < sourceText.size(); i++){
        if(delimiters.contains(sourceText[i])){
            int wordLen = i-start;
            if(testWord(sourceText.mid(start,wordLen), wordsList)){
                QString sentence = extractSentence(start, sourceText);
                if(!sentencesPower.contains(sentence)){
                    sentenceCount++;
                    sentencesPower[sentence] = 0;
                }
                sentencesPower[sentence]++;
                words[sentence][sourceText.mid(start,wordLen)] = true;
            }
            start = i + 1;
        }
    }

    QMapIterator<QString, int> sentencesPowerIterator(sentencesPower);
    while (sentencesPowerIterator.hasNext()) {
        QPair<int, QString> abc;
        sentencesPowerIterator.next();
        abc.first = sentencesPowerIterator.value();
        abc.second = sentencesPowerIterator.key();
        sortedSentences.append(abc);
    }
    qSort(sortedSentences.begin(), sortedSentences.end(),qGreater<QPair<int, QString>>());

    for(int i = 0; i < sortedSentences.size(); i++){
        if (i > 2) break;
        QString sentence;
        QMapIterator<QString,bool> wordsIterator(words[sortedSentences[i].second]);
        while (wordsIterator.hasNext()) {
            wordsIterator.next();
            sentence = sortedSentences[i].second;
            sentence = sentence.replace(wordsIterator.key(), "<span class=\"search-pattern\">" + wordsIterator.key() + "</span>");
        }
        sample += sentence + " ... ";
    }

    return sample;
}

bool SimpleSearch::testWord(QString word, QStringList wordsList){
    if(!wordIsValid(word))return false;
    word = word.toLower();

    for(int i = 0; i < wordsList.size(); i++){
        if(wordsList[i] == word){
            return true;
        }
    }
    return false;
}

QStringList SimpleSearch::cleanPhrase(QString phrase){
    QStringList cleaned;
    QStringList phraseList = phrase.split(" ");
    for(int i = 0; i < phraseList.size(); i++){
        if(wordIsValid(phraseList[i])){
            cleaned.append(phraseList[i]);
        }
    }
    return cleaned;
}

QString SimpleSearch::extractSentence(int pos, QString text){
    QList<QChar> abc;
    abc << '.' << '?' << '!';
    int length = text.size();
    int start = pos;
    while(!(abc.contains(text[start])) && (start != 0)){
        start--;
    }
    if (start != 0) start += 2;

    int end = pos;
    while(!(abc.contains(text[end])) && (end != length - 1)){
        end++;
    }

    return text.mid(start,end-start);
}

bool SimpleSearch::wordIsValid(QString word){
    int wordSize = word.size();
    QRegExp rx("[А-ЯA-Z]{2}");
    if(wordSize < 2)return false;
    else if((wordSize < 3) && !(rx.exactMatch(word)))return false;
    else return true;
}
