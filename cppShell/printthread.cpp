#include "printthread.h"
#include <QtCore>
#include <QDebug>

PrintThread::PrintThread(QString acroPath, QString printDir, QString printUrl){
    this->acroPath = acroPath;
    this->printDir = printDir;
    this->printUrl = printUrl;
}

void PrintThread::run(){
    QProcess print;
    QDir dir(printDir);
    print.start(acroPath, QStringList() << "/n" << "/p" << printUrl, QIODevice::ReadOnly);
    bool finished = print.waitForFinished(-1);
    qDebug() << finished;
    if (!finished){
        print.close();
    }

    while (!dir.removeRecursively()) {
        qDebug() << "File busy!!!";
    }
}
