#include "core.h"
#include "utils.h"

extern QList<QString> consoleParams;
extern QString ROOT_PATH;
extern QString UPDATE_PATH;

App::App(int argc, char *argv[]){
    for(int i = 1; i < argc; i++){
        consoleParams.append(argv[i]);
    }

    shell = new Shell(argc, argv);

    UPDATE_PATH = QStandardPaths::standardLocations(QStandardPaths::DataLocation)[0] + "/update/";

    if(QDir(UPDATE_PATH).entryList().count() > 2){
        if(DetachedProcess().start(ROOT_PATH + "bin/update.exe", QStringList() << UPDATE_PATH << ROOT_PATH << consoleParams)){
            return;//завершаем текущий процесс при успешном старте дочернего
        }
    }

    setupDefaultUser();

    shell->webView->showMaximized();
    controlObj = new ShellObj(shell, &user);

    shell->launchStartPage(controlObj);
    enableInspector(shell->webView);

    shell->exec();
}

void App::setupDefaultUser(){
    user["id"] = QString("1");
    user["login"] = QString("admin");
    user["pass"] = QString("123456");
    user["name"] = QString("Пупкин Василий");
    user["access"] = QString("1");
}
