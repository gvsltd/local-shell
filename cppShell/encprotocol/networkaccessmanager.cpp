#include "deliveryreply.h"
#include "networkaccessmanager.h"
#include <QtNetwork>


NetworkAccessManager::NetworkAccessManager(QNetworkAccessManager *manager, EncryptedStorage *contentStorage, QObject *parent)
    : QNetworkAccessManager(parent){
    setCache(manager->cache());
    setCookieJar(manager->cookieJar());
    setProxy(manager->proxy());
    setProxyFactory(manager->proxyFactory());

    this->contentStorage = contentStorage;
}

QNetworkReply *NetworkAccessManager::createRequest(QNetworkAccessManager::Operation operation, const QNetworkRequest &request, QIODevice *device){
    if (request.url().scheme() == "encd")
        return new DeliveryReply(request.url(), contentStorage);
    else
        return QNetworkAccessManager::createRequest(operation, request, device);
}
