#include "encryptedstorage.h"

#include <QNetworkAccessManager>

#ifndef NETWORKACCESSMANAGER_H
#define NETWORKACCESSMANAGER_H

class NetworkAccessManager : public QNetworkAccessManager{
    Q_OBJECT

public:
    NetworkAccessManager(QNetworkAccessManager *oldManager, EncryptedStorage *contentStorage, QObject *parent = 0);

protected:
    QNetworkReply *createRequest(Operation operation, const QNetworkRequest &request, QIODevice *device);

private:
    EncryptedStorage *contentStorage;
};

#endif // NETWORKACCESSMANAGER_H
