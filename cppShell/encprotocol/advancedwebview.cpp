#include "advancedwebview.h"
#include "networkaccessmanager.h"

#include <QNetworkAccessManager>
#include <QWebInspector>

AdvancedWebView::AdvancedWebView(EncryptedStorage *contentStorage){
    QNetworkAccessManager *oldManager = page()->networkAccessManager();
    NetworkAccessManager *newManager = new NetworkAccessManager(oldManager, contentStorage, this);
    page()->setNetworkAccessManager(newManager);
}
