#include "encryptedstorage.h"
#include <QFileInfo>
#include <QJsonDocument>

extern QString ROOT_PATH;

EncryptedStorage::EncryptedStorage(){
    storage.setFileName(ROOT_PATH + "container.evl");

    storage.open(QIODevice::ReadOnly);

    char* sizeBytes = decrypt(storage.read(4)).data();
    qint32 size = *(qint32*)(sizeBytes);

    QByteArray mapBytes = storage.read(size);
    mapBytes = decrypt(mapBytes);

    offset = mapBytes.size();
    pathMap = QJsonDocument::fromJson(mapBytes).toVariant().toMap();
}

QByteArray EncryptedStorage::decrypt(QByteArray source){
    int key = 56;
    int letter;
    QByteArray cipher;
    int encSymbol;
    int sourceSize = source.size();

    for (int i = 0; i < sourceSize; ++i) {
        letter = int(source[i]) + 128;
        encSymbol = letter ^ key;
        cipher.append(char(encSymbol - 128));

        key = ((key * key * ((letter * encSymbol) % 213)) + (((letter + encSymbol) % 43) + i)) % 256;
    }

    return cipher;
}

QByteArray EncryptedStorage::getFile(QString path){
    QVariantMap fileData = pathMap["map"].toMap()[path.toLower()].toMap();
    storage.seek(fileData["offset"].toInt() + offset);
    QByteArray encryptedFile = storage.read(fileData["size"].toInt());
    return decrypt(encryptedFile);
}
