#include "encryptedstorage.h"

#include <QNetworkReply>

#ifndef DELIVERYREPLY_H
#define DELIVERYREPLY_H

class DeliveryReply : public QNetworkReply{
    Q_OBJECT

public:
    DeliveryReply(const QUrl &url, EncryptedStorage *contentStorage);
    void abort();
    qint64 bytesAvailable() const;
    bool isSequential() const;

protected:
    qint64 readData(char *data, qint64 maxSize);

    EncryptedStorage *storage;
    QByteArray content;
    qint64 offset;

private:
    void setTypeHeader(QString fileExtension);

private slots:
    void ready();
};

#endif // DELIVERYREPLY_H
