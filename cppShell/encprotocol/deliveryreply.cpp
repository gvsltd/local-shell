#include "deliveryreply.h"

#include <QTimer>

extern QString ROOT_PATH;

DeliveryReply::DeliveryReply(const QUrl &url, EncryptedStorage *contentStorage) : QNetworkReply(){
    storage = contentStorage;

    offset = 0;
    setUrl(url);

    //при работе с защищенным контейнером, сначала проверяем
    //наличие искомого файла в папке hothtml. и если его не
    //сущестует, считываем из контейнера
    QString path = url.url(QUrl::RemoveScheme).mid(2).toLower();
    QFile localFile(ROOT_PATH + "hot" + path);
    if(localFile.exists()){
        if(localFile.open(QIODevice::ReadOnly)){
            content = localFile.readAll();
            localFile.close();
        }
    } else {
        content = storage->getFile(path);
    }
    setTypeHeader(url.fileName().split('.').last().toLower());

    QTimer::singleShot(10, this, SLOT(ready()));
}

void DeliveryReply::setTypeHeader(QString fileExtension){
    if(fileExtension == QString("svg"))
        setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/svg+xml"));
    else if(fileExtension == QString("woff"))
        setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/font-woff"));
}

void DeliveryReply::ready(){
    open(ReadOnly | Unbuffered);

    emit readyRead();
    emit finished();
}

void DeliveryReply::abort(){

}

qint64 DeliveryReply::bytesAvailable() const{
    return content.size() - offset + QIODevice::bytesAvailable();
}

bool DeliveryReply::isSequential() const{
    return true;
}

qint64 DeliveryReply::readData(char *data, qint64 maxSize){
    if (offset < content.size()) {
        qint64 number = qMin(maxSize, content.size() - offset);
        memcpy(data, content.constData() + offset, number);
        offset += number;
        return number;
    } else {
        close();
        return -1;
    }
}
