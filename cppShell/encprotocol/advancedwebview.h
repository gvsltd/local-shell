#ifndef ADVANCEDWEBVIEW_H
#define ADVANCEDWEBVIEW_H

#include <QWebView>
#include "encryptedstorage.h"

class AdvancedWebView : public QWebView{
    Q_OBJECT

public:
    explicit AdvancedWebView(EncryptedStorage *contentStorage);
};

#endif // ADVANCEDWEBVIEW_H
