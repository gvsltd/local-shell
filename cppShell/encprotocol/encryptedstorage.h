#include <QFile>
#include <QObject>
#include <QVariant>

#ifndef ENCRYPTEDSTORAGE_H
#define ENCRYPTEDSTORAGE_H

class EncryptedStorage : QObject{
    Q_OBJECT

public:
    EncryptedStorage();
    QVariantMap pathMap;
    QByteArray getFile(QString path);
    QByteArray decrypt(QByteArray source);

private:
    QFile storage;
    qint64 offset;    
};

#endif // ENCRYPTEDSTORAGE_H
