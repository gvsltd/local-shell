#ifndef PRINTTHREAD_H
#define PRINTTHREAD_H
#include <QtCore>

class PrintThread : public QThread{
    QString acroPath;
    QString printDir;
    QString printUrl;
public:
    PrintThread(QString acroPath, QString printDir, QString printUrl);
    void run();
};

#endif // PRINTTHREAD_H
