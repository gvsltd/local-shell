#ifndef SHELL_H
    #define SHELL_H

    #include <QWebView>
    #include <QWebFrame>
    #include <QApplication>
    #include <QUrl>

#include "encprotocol/advancedwebview.h"
#include "encprotocol/encryptedstorage.h"

    class ShellObj;
    #include "shellObject.h"

    class Shell : public QApplication {
        QJsonObject readProjectConfigs(ShellObj *controlObj);
        void createTrustFlashFile();
    public:
        EncryptedStorage *contentStorage;
        AdvancedWebView *webView;
        QWebFrame *frame;

        Shell(int & argc, char ** argv);
        void prepare(QString name, ShellObj *inObject);
        void loadPage(QString link, ShellObj *controlObject = 0);
        void auth(ShellObj *controlObject);
        void selectProject(ShellObj *controlObj, QJsonObject configs);
        void launchStartPage(ShellObj *controlObj);
    };

#endif // SHELL_H
