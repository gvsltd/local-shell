#ifndef CORE_H
#define CORE_H

#include "shell.h"
#include "detachedProcess.h"
#include "shellObject.h"
#include "encprotocol/encryptedstorage.h"

#include <QJsonObject>
#include <QWebInspector>
#include <QDir>
#include <QDebug>
#include <QStringList>

class App {
    void setupDefaultUser();
public:
    Shell *shell;
    ShellObj *controlObj;
    QJsonObject user;

    App(int argc, char *argv[]);
};

#endif // CORE_H
