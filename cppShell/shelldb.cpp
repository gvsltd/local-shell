#include "shelldb.h"
extern QString ROOT_PATH;
extern int APP_VERSION;

Shelldb *Shelldb::instance = NULL;

QSqlDatabase Shelldb::getConnect(QString path){
    QSqlDatabase con = connections[path];
    if (!con.open()) {
        throw std::runtime_error(QString("database `" + path + "` not found!").toStdString());
    }
    return con;
}

void Shelldb::clearWorkMenu(int userId){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "DELETE FROM bookmarks WHERE user_id=" + QString::number(userId);
    query.exec(str);
    str = "DELETE FROM notes WHERE user_id=" + QString::number(userId);
    query.exec(str);
    str = "DELETE FROM selects WHERE user_id=" + QString::number(userId);
    query.exec(str);
    str = "DELETE FROM selections WHERE user_id=" + QString::number(userId);
    query.exec(str);
    str = "DELETE FROM selections_comments WHERE user_id=" + QString::number(userId);
    query.exec(str);
    connection.close();
}

QString Shelldb::regMe(QString login, QString password, QString fio){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString request;
    QString str = "SELECT * FROM users WHERE login='" + login + "'";
    query.exec(str);
    if (!query.next()){
        query.exec(QString("INSERT INTO users (login, password, name) VALUES ('%1', '%2', '%3')").arg(login).arg(password).arg(fio));
        request = "ok";
    }else{
        request = "exist";
    }
    qDebug() << request;
    connection.close();
    return request;
}

QJsonObject Shelldb::login(QString login, QString password){
    QJsonObject data;
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "SELECT * FROM users WHERE (login='" + login + "' AND password='" + password + "')";
    query.exec(str);
    if(query.next()){
        data["id"] = query.value(0).toString();
        data["login"] = query.value(1).toString();
        data["pass"] = query.value(2).toString();
        data["name"] = query.value(3).toString();
        data["access"] = query.value(4).toString();
    }
    connection.close();
    return data;
}

QJsonObject Shelldb::getNavigation(){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    QJsonObject rp;
    QString str = "SELECT * FROM navigation WHERE (section='ebook') ORDER BY id";
    QJsonArray ebookList;
    query.exec(str);
    while (query.next()) {
        QJsonObject ebookElements;
        ebookElements["id"] = query.value(0).toInt();
        ebookElements["textTitle"] = query.value(2).toString();
        ebookElements["parent"] = query.value(3).toInt();
        ebookElements["section"] = query.value(4).toString();
        ebookElements["preview"] = query.value(5).toString();
        ebookElements["type"] = query.value(6).toString();
        ebookElements["resource"] = query.value(7).toString();
        ebookElements["textName"] = query.value(8).toString();
        ebookElements["class"] = query.value(9).toString();
        ebookList.append(ebookElements);
    }
    rp["ebook"] = ebookList;

    str = "SELECT * FROM navigation WHERE (section='eapp') ORDER BY id";
    QJsonArray eappList;
    query.exec(str);
    while (query.next()) {
        QJsonObject eappElements;
        eappElements["id"] = query.value(0).toInt();
        eappElements["textTitle"] = query.value(2).toString();
        eappElements["parent"] = query.value(3).toInt();
        eappElements["section"] = query.value(4).toString();
        eappElements["preview"] = query.value(5).toString();
        eappElements["type"] = query.value(6).toString();
        eappElements["resource"] = query.value(7).toString();
        eappElements["textName"] = query.value(8).toString();
        eappElements["class"] = query.value(9).toString();
        eappList.append(eappElements);
    }
    rp["eapp"] = eappList;
    connection.close();
    return rp;
}

QJsonArray Shelldb::getBookmark(int userId){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    query.exec("SELECT resource_id FROM bookmarks WHERE user_id=" + QString::number(userId));
    QJsonArray bookmarks;
    while (query.next()) {
        bookmarks.append(query.value("resource_id").toInt());
    }
    connection.close();
    return bookmarks;
}

QJsonArray Shelldb::getBookmarks(int userId){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    query.exec("SELECT resource_id FROM bookmarks WHERE user_id=" + QString::number(userId));
    QJsonArray bookmarks;
    while (query.next()) {
        bookmarks.append(query.value("resource_id").toInt());
    }
    connection.close();
    return bookmarks;
}

void Shelldb::addBookmark(int userId, int resourceId, QString comment){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    query.prepare("INSERT INTO bookmarks (user_id, resource_id,comment) VALUES (?,?,?)");
    query.bindValue(0,userId);
    query.bindValue(1,resourceId);
    query.bindValue(2,comment);
    query.exec();
    connection.close();
}

void Shelldb::delBookmark(int bookmarkId){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "DELETE FROM bookmarks WHERE resource_id=" + QString::number(bookmarkId);
    query.exec(str);
    connection.close();
}

QVariantMap Shelldb::getVocabulary(){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    query.exec("SELECT * FROM vocabulary");
    QVariantMap rs;
    while (query.next()) {
        rs[query.value("term").toString()] = query.value("description").toString();
    }
    connection.close();
    return rs;
}

QJsonArray Shelldb::getPageInfo(int pageId){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    QJsonObject page;
    QJsonObject parent;
    QJsonArray rs;
    QString str = "SELECT * FROM navigation WHERE id=" + QString::number(pageId);
    query.exec(str);
    while (query.next()) {
        page["id"] = query.value(0).toString();
        page["number"] = query.value(1).toString();
        page["text"] = query.value(2).toString();
        page["parent"] = query.value(3).toString();
        page["section"] = query.value(4).toString();
        page["image"] = query.value(5).toString();
        page["type"] = query.value(6).toString();
        page["resource"] = query.value(7).toString();
        page["desc"] = query.value(8).toString();
        page["class"] = query.value(9).toString();
    }
    rs.append(page);
    str = "SELECT * FROM navigation WHERE id='" + QString::number(page["parent"].toString().toInt()) + "'";
    query.exec(str);
    while (query.next()) {
        parent["id"] = query.value(0).toString();
        parent["number"] = query.value(1).toString();
        parent["text"] = query.value(2).toString();
        parent["parent"] = query.value(3).toString();
        parent["section"] = query.value(4).toString();
        parent["image"] = query.value(5).toString();
        parent["type"] = query.value(6).toString();
        parent["resource"] = query.value(7).toString();
        parent["desc"] = query.value(8).toString();
        parent["class"] = query.value(9).toString();
    }
    rs.append(parent);
    connection.close();
    return rs;
}

void Shelldb::updateBookmark(int bookmarkId, QString newComment){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "UPDATE bookmarks SET (comment='" + newComment + "') WHERE id=" + QString::number(bookmarkId);
    query.exec(str);
    connection.close();
}

void Shelldb::addNote(int userId, int resourceId, QString text){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    query.prepare("INSERT INTO notes (user_id, resource_id, text) VALUES (?, ?, ?)");
    query.bindValue(0, userId);
    query.bindValue(1, resourceId);
    query.bindValue(2, text);
    query.exec();
    connection.close();
}

void Shelldb::updateNote(int noteId, QString newText){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "UPDATE notes SET (text='" + newText + "') WHERE id=" + QString::number(noteId);
    query.exec(str);
    connection.close();
}

QString Shelldb::getNote(int rId, int userId){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "SELECT text FROM notes WHERE resource_id=" + QString::number(rId) + " AND user_id=" + QString::number(userId);
    query.exec(str);
    QString rs;
    if(query.next()) {
        rs = query.value("text").toString();
    }
    connection.close();
    return rs;
}

QJsonArray Shelldb::getNotes(int userId){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "SELECT * FROM notes WHERE user_id=" + QString::number(userId);
    QJsonArray notes;
    query.exec(str);
    while (query.next()) {
        QJsonObject note;
        note["id"] = query.value(0).toInt();
        note["user_id"] = query.value(1).toInt();
        note["resource_id"] = query.value(2).toInt();
        note["text"] = query.value(3).toString();
        notes.append(note);
    }
    connection.close();
    return notes;
}

void Shelldb::delNote(int noteId){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "DELETE FROM notes WHERE resource_id=" + QString::number(noteId);
    query.exec(str);
    connection.close();
}

QJsonObject Shelldb::getResources(){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    query.exec("SELECT * FROM resources");

    QJsonObject resources;
    while (query.next()) {
        QString type = query.value("type").toString();
        if(resources[type].isNull())
            resources[type] = QJsonArray();

        if(!query.value("navID").isNull()){
            QJsonObject rec = _getNavResource(query.value("navID").toInt());
            rec["id"] = query.value("id").toString();
            resources[type].toArray().append(rec);
        } else {
            QJsonObject rec;
            rec["id"] = query.value("id").toString();
            rec["name"] = query.value("name").toString();
            rec["link"] = query.value("link").toString();
            rec["class"] = query.value("class").toString();
            resources[type].toArray().append(rec);
        }
    }
    connection.close();
    return resources;
}

QJsonObject Shelldb::_getNavResource(int id){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    query.exec("SELECT * FROM navigation WHERE id=" + QString::number(id) + " LIMIT 1");

    QJsonObject res;
    if(query.next()){
        res["name"] = query.value("text").toString();
        res["link"] = query.value("resource").toString();
    }
    connection.close();
    return res;
}

QJsonArray Shelldb::getJournal(int user_id){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    query.prepare("SELECT * FROM journal WHERE user_id=:user_id");
    query.bindValue(":user_id", storage->decrypt(QByteArray::number(user_id)));
    query.exec();

    QJsonArray res;
    while(query.next()){
        QJsonObject rec;
        rec["id"] = QString::fromUtf8(storage->decrypt(query.value("id").toByteArray()));
        rec["target_item"] = QString::fromUtf8(storage->decrypt(query.value("target_item").toByteArray()));
        rec["date"] = QString::fromUtf8(storage->decrypt(query.value("date").toByteArray()));
        rec["time_end"] = QString::fromUtf8(storage->decrypt(query.value("time_end").toByteArray()));
        rec["time_start"] = QString::fromUtf8(storage->decrypt(query.value("time_start").toByteArray()));
        rec["questions_total"] = QString::fromUtf8(storage->decrypt(query.value("questions_total").toByteArray()));
        rec["questions_correct"] = QString::fromUtf8(storage->decrypt(query.value("questions_correct").toByteArray()));
        rec["user_id"] = QString::fromUtf8(storage->decrypt(query.value("user_id").toByteArray()));
        res.append(rec);
    }
    connection.close();
    return res;
}

void Shelldb::addJournal(QJsonArray journalArr){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    query.prepare("INSERT INTO journal (target_item, date, time_end, time_start, questions_total, questions_correct, user_id) VALUES (?,?,?,?,?,?,?)");
    query.bindValue(0, storage->decrypt(QByteArray::number(journalArr[0].toInt())));
    query.bindValue(1, storage->decrypt(journalArr[1].toString().toUtf8()));
    query.bindValue(2, storage->decrypt(journalArr[2].toString().toUtf8()));
    query.bindValue(3, storage->decrypt(journalArr[3].toString().toUtf8()));
    query.bindValue(4, storage->decrypt(QByteArray::number(journalArr[4].toInt())));
    query.bindValue(5, storage->decrypt(QByteArray::number(journalArr[5].toInt())));
    query.bindValue(6, storage->decrypt(QByteArray::number(journalArr[6].toInt())));
    query.exec();
    connection.close();
}

void Shelldb::delJournal(int user_id){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    query.prepare("DELETE FROM journal WHERE user_id=:user_id");
    query.bindValue(":user_id", storage->decrypt(QByteArray::number(user_id)));
    query.exec();
    connection.close();
}

void Shelldb::updateJournal(QJsonArray journalArr){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    query.prepare("UPDATE journal SET date=:date, time_end=:time_end, time_start=:time_start, questions_correct=:questions_correct WHERE target_item=:target_item AND user_id=:user_id");
    query.bindValue(":date", storage->decrypt(journalArr[1].toString().toUtf8()));
    query.bindValue(":time_end", storage->decrypt(journalArr[2].toString().toUtf8()));
    query.bindValue(":time_start", storage->decrypt(journalArr[3].toString().toUtf8()));
    query.bindValue(":questions_correct", storage->decrypt(QByteArray::number(journalArr[5].toInt())));
    query.bindValue(":target_item", storage->decrypt(QByteArray::number(journalArr[0].toInt())));
    query.bindValue(":user_id", storage->decrypt(QByteArray::number(journalArr[6].toInt())));
    query.exec();
    connection.close();
}

QJsonObject Shelldb::getLanguagePack(QString language){
    QSqlDatabase connection = getConnect(sharedDB);
    QSqlQuery query(connection);
    query.exec("SELECT * FROM language_pack");

    QJsonObject terms;
    while (query.next()){
        if(language == "rus"){
            terms[query.value(0).toString()] = query.value(1).toString();
        } else if(language == "eng"){
            terms[query.value(0).toString()] = query.value(2).toString();
        } else {
            throw std::runtime_error("unknown language");
        }
    }
    connection.close();

    return terms;
}

void Shelldb::getTest(char name[]){
    //past
}

void Shelldb::getScript(char name[]){
    //past
}

void Shelldb::getChilds(int parentId){
    //past
}

void Shelldb::getQuestion(int scriptId, char step[]){
    //past
}

void Shelldb::getQuestions(int scriptId){
    //past
}

QJsonArray Shelldb::getAnswers(int questId){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    QString str = "SELECT * FROM answers WHERE question_id=" + QString::number(questId);
    QJsonArray answers;
    query.exec(str);
    while (query.next()) {
        QJsonObject answer;
        answer["id"] = query.value(0).toInt();
        answer["question_id"] = query.value(1).toInt();
        answer["answer"] = query.value(2).toString();
        answer["cost"] = query.value(3).toInt();
        answer["meta"] = query.value(3).toString();
        answers.append(answer);
    }
    return answers;
}

QString Shelldb::idToQuest(int questid){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    QString str = "SELECT * FROM questions WHERE id='" + QString::number(questid) + "'";
    query.exec(str);
    QString rs;
    if(query.next()) {
        rs = query.value("question").toString();
    }
    connection.close();
    return rs;
}

QString Shelldb::idToAnswer(int answerid){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    QString str = "SELECT * FROM answers WHERE id='" + QString::number(answerid) + "'";
    query.exec(str);
    QString rs;
    if(query.next()) {
        rs = query.value("answer").toString();
    }
    connection.close();
    return rs;
}

void Shelldb::getRightAnswers(int scriptId){
    //past
}

int Shelldb::getAnswerId(int questid, QString answer){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    QString str = "SELECT id FROM answers WHERE question_id=" + QString::number(questid) + " AND answer='" + answer + "'";
    query.exec(str);
    int rs = 0;
    if(query.next()) {
        rs = query.value("id").toInt();
    }
    connection.close();
    return rs;
}

int Shelldb::checkAnswer(int answerid){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    QString str = "SELECT cost FROM answers WHERE id=" + QString::number(answerid);
    query.exec(str);
    int cost;
    int right = 0;
    if(query.next()) {
        cost = query.value("cost").toInt();
        if(cost > 0) {
            right = 1;
        }
    }
    connection.close();
    return right;
}

QJsonObject Shelldb::getHrefs(){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    query.exec("SELECT * FROM hrefs");
    QJsonObject res;
    while(query.next()){
        QJsonObject rec;
        rec["img"] = query.value("img").toString();
        rec["description"] = query.value("description").toString();
        rec["href"] = query.value("href").toString();
        rec["title"] = query.value("title").toString();
        res[query.value("id").toString()] = rec;
    }
    connection.close();
    return res;
}

void Shelldb::saveSelection(int item, QString htmltext, int user_id){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "SELECT id FROM selections WHERE user_id=" + QString::number(user_id) + " AND pageId=" + QString::number(item);
    query.exec(str);
    if(query.next()){
        query.prepare("UPDATE selections SET htmltext=:htmltext WHERE user_id=:user_id AND pageId=:pageId");
        query.bindValue(":pageId", item);
        query.bindValue(":htmltext", storage->decrypt(htmltext.toUtf8()));
        query.bindValue(":user_id", user_id);
        query.exec();
    }else{
        query.prepare("INSERT INTO selections (pageId,htmltext,user_id) VALUES (:pageId,:htmltext,:user_id)");
        query.bindValue(":pageId", item);
        query.bindValue(":htmltext", storage->decrypt(htmltext.toUtf8()));
        query.bindValue(":user_id", user_id);
        query.exec();
    }
    connection.close();
}

QJsonArray Shelldb::getSelections(int userId){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "SELECT * FROM selects WHERE user_id=" + QString::number(userId);
    QJsonArray selections;
    query.exec(str);
    while (query.next()) {
        QJsonObject selection;
        selection["id"] = query.value("id").toInt();
        selection["pageId"] = query.value("page_id").toString();
        selection["key"] = query.value("key").toString();
        selection["selection"] = query.value("selection").toString();
        selections.append(selection);
    }
    return selections;
}

QString Shelldb::getSelection(int userId, int pageID){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "SELECT * FROM selections WHERE user_id=" + QString::number(userId) + " AND pageId=" + QString::number(pageID);
    query.exec(str);
    QString rs;
    if(query.next()) {
        rs = QString::fromUtf8(storage->decrypt(query.value("htmltext").toByteArray()));
    }
    connection.close();
    return rs;
}

void Shelldb::delSelections(int item, int user_id){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "DELETE FROM selections WHERE user_id=" + QString::number(user_id) + " AND pageId=" + QString::number(item);
    query.exec(str);
    str = "DELETE FROM selections_comments WHERE user_id=" + QString::number(user_id) + " AND page_id=" + QString::number(item);
    query.exec(str);
    connection.close();
}

void Shelldb::recSel(int userId, int pageId, QString key, QString selection){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    if(!connection.record("selects").contains("selection")){
        QString str = "ALTER TABLE selects ADD COLUMN selection TEXT DEFAULT ''";
        query.exec(str);
    }

    QString str = "SELECT * FROM selects WHERE page_id=" + QString::number(pageId) + " AND key='" + key + "' AND user_id=" + QString::number(userId);
    query.exec(str);
    if (!query.next()){
        str = "INSERT INTO selects (user_id, page_id, key, selection) VALUES (" + QString::number(userId) + ", " + QString::number(pageId) + ", '" + key + "', '" + selection + "')";
        query.exec(str);
    }
    connection.close();
}

void Shelldb::delRecSel(int userId, int pageId, QString key){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "DELETE FROM selects WHERE user_id=" + QString::number(userId) + " AND page_id=" + QString::number(pageId) + " AND key='" + key + "'";
    query.exec(str);
    connection.close();
}

void Shelldb::saveHighlightComment(int pageId, QString nom, QString comment, int userId, QString selection){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    if(!connection.record("selections_comments").contains("selection")){
        QString str = "ALTER TABLE selections_comments ADD COLUMN selection TEXT DEFAULT ''";
        query.exec(str);
    }

    QString str = "SELECT * FROM selections_comments WHERE page_id=" + QString::number(pageId) + " AND key='" + nom + "' AND user_id=" + QString::number(userId);
    query.exec(str);
    if(query.next()){
        str = "UPDATE selections_comments SET comment='" + comment + "' WHERE key='" + nom + "' AND page_id=" + QString::number(pageId) + " AND user_id=" + QString::number(userId);
        query.exec(str);
    }else{
        str = "INSERT INTO selections_comments (key, comment, page_id, user_id, selection) VALUES ('" + nom + "','" + comment + "'," + QString::number(pageId) + "," + QString::number(userId) + ", '" + selection + "')";
        query.exec(str);
    }
    connection.close();
}

QString Shelldb::getHighlightComment(int userId, int pageId, QString nom){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "SELECT * FROM selections_comments WHERE key='" + nom + "' AND user_id=" + QString::number(userId) + " AND page_id=" + QString::number(pageId);
    query.exec(str);
    QString rs;
    if(query.next()) {
        rs = query.value("comment").toString();
    }
    connection.close();
    return rs;
}

QJsonArray Shelldb::getComments(int userId) {
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "SELECT * FROM selections_comments WHERE user_id=" + QString::number(userId);
    QJsonArray data;
    query.exec(str);
    while (query.next()) {
        QJsonObject datum;
        datum["id"] = query.value("id").toInt();
        datum["pageId"] = query.value("page_id").toInt();
        datum["key"] = query.value("key").toString();
        datum["comment"] = query.value("comment").toString();
        datum["selection"] = query.value("selection").toString();
        data.append(datum);
    }
    return data;
}

void Shelldb::delSel(int nom){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "DELETE FROM selections WHERE id=" + QString::number(nom);
    query.exec(str);
    connection.close();
}

void Shelldb::delHighlightComment(int page, QString nom, int userId){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    QString str = "DELETE FROM selections_comments WHERE user_id=" + QString::number(userId) + " AND page_id=" + QString::number(page) + " AND key='" + nom + "'";
    query.exec(str);
    connection.close();
}

QString Shelldb::getProductVersion(){
    QSqlDatabase connection = getConnect(pathS);
    QSqlQuery query(connection);
    QString str = "SELECT version FROM version";
    QString request;
    query.exec(str);
    if (!query.next()){
        request = "err";
    }else{
        request = query.value(0).toString();
    }
    connection.close();
    return request;
}

void Shelldb::saveUserSettings(QJsonObject settings, int userID){
    QJsonObject savedSettings = getUserSettings(userID);

    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);

    if(savedSettings.isEmpty()){
        QString keys = "( '";
        QString vals = "( '";
        for(QJsonObject::iterator i = settings.begin(); i != settings.end(); i++){
            keys += i.key() + "', '";
            vals += i.value().toString() + "', '";
        }
        keys += "user_id')";
        vals += QString::number(userID) + "')";
        query.exec("INSERT INTO userSettings " + keys + " VALUES " + vals);
        connection.close();
        return;
    }

    bool needUpdate;
    for(QJsonObject::iterator i = settings.begin(); i != settings.end(); i++){
        if(i.value() != savedSettings[i.key()]){
            needUpdate = true;
            break;
        }
    }

    if(!needUpdate){
        connection.close();
        return;
    }

    QString sql = "UPDATE userSettings SET ";
    for(QJsonObject::iterator i = settings.begin(); i != settings.end(); i++){
        sql += i.key() + "='" + i.value().toString() + "',";
    }
    sql = sql.remove(sql.length()-1, 1) + " WHERE user_id=" + QString::number(userID);
    query.exec(sql);
    connection.close();
}

QJsonObject Shelldb::getUserSettings(int userID){
    QSqlDatabase connection = getConnect(pathD);
    QSqlQuery query(connection);
    query.exec("SELECT * FROM userSettings WHERE user_id=" + QString::number(userID));
    QJsonObject res;
    if(query.next()){
        res["font"] = query.value("font").toString();
        res["theme"] = query.value("theme").toString();
        res["user_id"] = query.value("user_id").toString();
        res["lastPageID"] = query.value("lastPageID").toString();
        res["lastPageParent"] = query.value("lastPageParent").toString();
        res["contentFontSize"] = query.value("contentFontSize").toString();
        res["lastPageType"] = query.value("lastPageType").toString();
        res["interfaceFontSize"] = query.value("interfaceFontSize").toString();
    }
    connection.close();
    return res;
}

void Shelldb::setDBCat(QString cat){
    QList<QString> oldPaths = QList<QString>() << pathS << pathD << sharedDB;

    QString safeDBCat(QStandardPaths::standardLocations(QStandardPaths::AppDataLocation)[0]);
    pathS = getExisting("db/" + cat + "_static.db", "html/content/" + cat + "/db/shelldb_static.db");
    if(!pathS.size()){
        pathS = safeDBCat + "/" + cat + "/db/shelldb_static.db";
    }
    pathD = getExisting("db/" + cat + "_dynamic.db", "html/content/" + cat + "/db/shelldb_dynamic.db");
    if(!pathD.size()){
        pathD = safeDBCat + "/" + cat + "/db/shelldb_dynamic.db";
    }

    if(!QFile::exists(safeDBCat + "/" + cat + "/db/shelldb_dynamic.db")){
        QDir dir(safeDBCat);
        dir.mkpath(cat + "/db");
        QFile::copy(pathD, safeDBCat + "/" + cat + "/db/shelldb_dynamic.db");
    }

    pathD = safeDBCat + "/" + cat + "/db/shelldb_dynamic.db";
    sharedDB = getExisting("db/common.db", "html/content/common.db");

    QList<QString> paths = QList<QString>() << pathS << pathD << sharedDB;
    initConnections(paths, oldPaths);
}

QString Shelldb::getExisting(QString file1, QString file2){
    if(QFile::exists(ROOT_PATH + file1)) {
        return ROOT_PATH + file1;
    } else if(QFile::exists(ROOT_PATH + file2)) {
        return ROOT_PATH + file2;
    } else {
        return "";
    }
}

void Shelldb::initConnections(QList<QString> paths, QList<QString> oldPaths){
    for(int i = 0; i < paths.size(); i++){
        QString oldPath = oldPaths[i];
        QString path = paths[i];
        if(oldPath == path) continue;

        if(connections.size()){
            connections.remove(oldPath);
            QSqlDatabase::removeDatabase(oldPath);
        }

        connections[path] = QSqlDatabase::addDatabase("QSQLITE", path);
        connections[path].setDatabaseName(path);
    }
}

void Shelldb::cleanConnections(){
    QList<QString> oldPaths = QList<QString>() << pathS << pathD << sharedDB;

    for(int i = 0; i < oldPaths.size(); i++){
        QString oldPath = oldPaths[i];
        if(connections.size()){
            connections.remove(oldPath);
            QSqlDatabase::removeDatabase(oldPath);
        }
    }
}
