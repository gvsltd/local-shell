#ifndef SHELLOBJECT_H
#define SHELLOBJECT_H

#include <QObject>
#include <QJsonObject>
#include <QString>
#include <QAction>
#include <QDir>
#include <QDesktopServices>
#include <QPrinter>
#include <QPrintDialog>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QCryptographicHash>
#include "search.h"
#include "shell.h"
#include "shelldb.h"
#include "utils.h"
#include <iostream>
using namespace std;
class SimpleSearch;
class Shell;

class ShellObj : public QObject {
    Q_OBJECT

Shell *shell;
Shelldb *db;
SimpleSearch *finder;
QJsonObject *user;
QJsonArray defBookmarks;
QJsonArray bookmarks;
QString contentDir;
QString acroPath;
QString printDir;
QString updateDir;
QString salt;
QList<int> targetItemList;

QString encodeJson(const QJsonObject &json);
QString encodeJson(const QJsonArray &json);
QJsonObject decodeJson(QString json);

void updateBookmarks();
void createPdf(QWebView *page);
void adobeNotFoundErr();
QString getUpdateLink(bool needContent);

public:
    QJsonObject projectConfigs;

    ShellObj(Shell *shell, QJsonObject *user);

public slots:
    void openWorkPage(QString contentDir = NULL);
    bool login(QString login, QString pass);
    QString regMe(QString login, QString pass, QString fio);
    QString getBookmarks();
    QString getLanguagePack(QString language);
    QString getUserSettings();
    QString getHrefs();
    void saveUISettings(QString theme, QString interfaceFontSize, QString contentFontSize, QString font);
    QString getNavi();
    QString getJournal();
    void addJournal(QString journalArr);
    void delJournal();
    void addNote(int pageID, QString comment);
    void delNote(int noteId);
    QString getResources();
    QString getVocabulary();
    void setLastViewedPage(QString type, QString parent, QString id);
    QString getLastViewedPage();
    QString getWorkmenu();
    void clearWorkmenu();
    void addBookmark(int pageID, QString comment);
    void delBookmark(int pageID);
    void appendBookmark(int pageID);
    void removeBookmark(int pageID);
    void saveSel(int pageID, QString htmltext);
    QString getSelection(int pageID);
    void delSelections(int pageID);
    void saveHighlightComment(int pageID, QString nom, QString comment, QString selection);
    QString getHighlightComment(int pageID,QString nom);
    void recSel(int pageID, QString nom, QString selection);
    QString getNote(int pageID);
    void goHome();
    QString inBookmarks(int pageID);
    void delRecSel(int pageID, QString nom);
    void delHighlightComment(int pageID, QString nom);
    void copyToClipboard();
    void openUrl(QString url);
    void openFile(QString file);
    void print_work_contents();
    void html2printer();
    void printContentPage(int PageId);
    QString getStrUserData();
    void initSearch(QString fileData);
    QString find(QString phrase);
    void checkForUpdate();
    void doUpdate();
    void saveUpdateArchive(QByteArray bytes);
    void extractUpdateArchive();
    void sendStatus(int status);

};

#endif // SHELLOBJECT_H


