#ifndef UTILS_H
#define UTILS_H

#include <QWebView>


void enableInspector(QWebView *webView, bool show = false);

#endif // UTILS_H
