#-------------------------------------------------
#
# Project created by QtCreator 2014-07-10T17:09:49
#
#-------------------------------------------------

QT       += core webkit webkitwidgets gui sql printsupport network

TARGET = Academia
#CONFIG   += console
CONFIG   += c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    shell.cpp \
    core.cpp \
    shellObject.cpp \
    settings.cpp \
    shelldb.cpp \
    printthread.cpp \
    search.cpp \
    encprotocol/advancedwebview.cpp \
    encprotocol/deliveryreply.cpp \
    encprotocol/encryptedstorage.cpp \
    encprotocol/networkaccessmanager.cpp \
    consoleParams.cpp \
    detachedProcess.cpp \
    utils.cpp

HEADERS += \
    shellObject.h \
    shell.h \
    core.h \
    shelldb.h \
    printthread.h \
    search.h \
    encprotocol/advancedwebview.h \
    encprotocol/deliveryreply.h \
    encprotocol/encryptedstorage.h \
    encprotocol/networkaccessmanager.h \
    consoleParams.h \
    detachedProcess.h \
    utils.h

INCLUDEPATH += ./../quazip/src
LIBS += -L./../quazip/dll -lquazip

win32:RC_ICONS += ./../icon.ico
