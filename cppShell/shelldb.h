#ifndef SHELLDB_H
#define SHELLDB_H

#include <QtSql>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include "encprotocol/encryptedstorage.h"

extern QString ROOT_PATH;
extern int APP_VERSION;

class Shelldb{
    Shelldb static *instance;
    QString pathD = "C:/Users/kazurov.vs/Desktop/GIT-HTML/html/content/templates/db/shelldb_dynamic.db";
    QString pathS = "C:/Users/kazurov.vs/Desktop/GIT-HTML/html/content/templates/db/shelldb_static.db";
    QString sharedDB;

    QMap<QString, QSqlDatabase> connections;

    Shelldb(){
    }
    QString getExisting(QString file1, QString file2);
    void initConnections(QList<QString> paths, QList<QString> oldPaths);

public:
    EncryptedStorage *storage;

    Shelldb static *getInstance(){
        if(!instance) instance = new Shelldb();
        return instance;
    }

    QSqlDatabase getConnect(QString path);

    void clearWorkMenu(int userId);

    QString regMe(QString login, QString password, QString fio);

    QJsonObject login(QString login, QString password);

    QJsonObject getNavigation();

    QJsonArray getBookmark(int userId);

    QJsonArray getBookmarks(int userId);

    void addBookmark(int userId, int resourceId, QString comment);

    void delBookmark(int bookmarkId);

    QVariantMap getVocabulary();

    QJsonArray getPageInfo(int pageId);

    void updateBookmark(int bookmarkId, QString newComment);

    void addNote(int userId, int resourceId, QString text);

    void updateNote(int noteId, QString newText);

    QString getNote(int rId, int userId);

    QJsonArray getNotes(int userId);

    void delNote(int noteId);

    QJsonObject getResources();

    QJsonObject _getNavResource(int id);

    QJsonArray getJournal(int user_id);

    void addJournal(QJsonArray journalArr);

    void delJournal(int user_id);

    void updateJournal(QJsonArray journalArr);

    QJsonObject getLanguagePack(QString language);

    void getTest(char name[]);

    void getScript(char name[]);

    void getChilds(int parentId);

    void getQuestion(int scriptId, char step[]);

    void getQuestions(int scriptId);

    QJsonArray getAnswers(int questId);

    QString idToQuest(int questid);

    QString idToAnswer(int answerid);

    void getRightAnswers(int scriptId);

    int getAnswerId(int questid, QString answer);

    int checkAnswer(int answerid);

    QJsonObject getHrefs();

    void saveSelection(int item, QString htmltext, int user_id);

    QJsonArray getSelections(int userId);

    QString getSelection(int userId, int pageID);

    void delSelections(int item, int user_id);

    void saveHighlightComment(int pageId, QString nom, QString comment, int userId, QString selection);

    void recSel(int userId, int pageId, QString key, QString selection);

    void delRecSel(int userId, int pageId, QString key);

    QString getHighlightComment(int userId, int pageId, QString nom);

    QJsonArray getComments(int userId);

    void delSel(int nom);

    void delHighlightComment(int page, QString nom, int userId);

    QString getProductVersion();

    void saveUserSettings(QJsonObject settings, int userID);

    QJsonObject getUserSettings(int userID);

    void setDBCat(QString cat);

    void init();

    void cleanConnections();
};

#endif // SHELLDB_H
