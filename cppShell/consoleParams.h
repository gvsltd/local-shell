#ifndef CONSOLEPARAMS_H
#define CONSOLEPARAMS_H

#include <QString>

struct ConsoleParams{
    static const QString FROM_FOLDER;
    static const QString SKIP_ENTER;
};

#endif // CONSOLEPARAMS_H
