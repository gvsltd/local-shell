#include "utils.h"

#include <QWebInspector>

void enableInspector(QWebView *webView, bool show){
    webView->page()->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
    QWebInspector *webInspector = new QWebInspector();
    webInspector->setMinimumWidth(900);
    webInspector->setPage(webView->page());

    if(show) webInspector->setVisible(true);
}

