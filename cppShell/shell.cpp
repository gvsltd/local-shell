#include "shell.h"
#include "shelldb.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QSize>
#include <QScreen>
#include <QDebug>
#include "consoleParams.h"

extern QString ROOT_PATH;
extern int APP_VERSION;
extern QList<QString> consoleParams;

Shell::Shell(int & argc, char ** argv) : QApplication(argc, argv){
    QString workPath(QApplication::applicationDirPath());
    if(QDir(workPath + "/../html").exists()){
        ROOT_PATH = workPath + "/../";
    } else {
        ROOT_PATH = workPath + "/../../";
    }
    ROOT_PATH = QDir(ROOT_PATH).absolutePath() + '/';

    contentStorage = new EncryptedStorage;

    webView = new AdvancedWebView(contentStorage);
//    webView->setWindowTitle("Электронный учебно-методический комплекс");
    webView->setWindowTitle("Электронный учебник");
    QSize screenSize = QGuiApplication::screens().at(0)->availableSize();
    webView->resize(screenSize);
    webView->setWindowState(Qt::WindowFullScreen);

    webView->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
    webView->setAttribute(Qt::WA_ContentsPropagated, true);
    webView->page()->settings()->setAttribute(QWebSettings::PluginsEnabled, true);
    frame = webView->page()->mainFrame();
    createTrustFlashFile();
}

void Shell::createTrustFlashFile(){
    QString appDataPath(QDir::homePath() + "/AppData/Roaming/");//win7-8 path
    if(!QDir(appDataPath).exists()) appDataPath = QDir::homePath() + "/Application Data/";//xp path
    qDebug() << QDir::homePath();

    QString path = appDataPath + "Macromedia/Flash Player/#Security/FlashPlayerTrust";
    if (!QDir(path).exists()) QDir().mkpath(path);
    QFile file(path + "/MediaTutorial.cfg");
    qDebug() << path;

    QString trustedPath(QDir(ROOT_PATH + "html/content/").absolutePath());
    bool pathAdded = false;
    if(file.exists()){
        file.open(QIODevice::ReadWrite);
        while (file.bytesAvailable()) {
            QString line(QString::fromUtf8(file.readLine()).trimmed());
            if(line == trustedPath){
                pathAdded = true;
                break;
            }
        }
        if(!pathAdded){
            file.write((trustedPath + '\n').toUtf8());
        }
        file.close();
    } else {
        file.open(QIODevice::WriteOnly);
        file.write((trustedPath + '\n').toUtf8());
        file.close();
    }
}

void Shell::prepare(QString name, ShellObj *inObject){
    QObject::connect(frame, &QWebFrame::javaScriptWindowObjectCleared, [this, name, inObject](){
        frame->addToJavaScriptWindowObject(name, inObject);
    });
}

void Shell::loadPage(QString link, ShellObj *controlObject){
    if(controlObject) {
        QObject::connect(frame, &QWebFrame::loadStarted, [this, controlObject](){
            prepare("shellObj", controlObject);
        });
    }

    if(consoleParams.contains(ConsoleParams::FROM_FOLDER))
        webView->load(QUrl("file:///" + ROOT_PATH + link));
    else
        webView->load(QUrl("encd://" + link));
}

void Shell::launchStartPage(ShellObj *controlObj){
    QJsonObject projectConfigs = readProjectConfigs(controlObj);

    controlObj->projectConfigs = projectConfigs;

    if(consoleParams.contains(ConsoleParams::SKIP_ENTER)){
        controlObj->openWorkPage();
        return;
    }

    //выбираем, показывать полку или форму авторизации
    if(projectConfigs.size() == 1){
        QString dir(projectConfigs.keys()[0]);
        Shelldb::getInstance()->setDBCat(dir);
        auth(controlObj);
    } else {
        selectProject(controlObj, projectConfigs);
    }
}

QJsonObject Shell::readProjectConfigs(ShellObj *controlObj){
    QJsonObject projectConfigs;

    if(controlObj->projectConfigs.isEmpty()){
        if(consoleParams.contains(ConsoleParams::FROM_FOLDER)){
            QString path(ROOT_PATH + "html/content/");

            QDir dirs(path);
            dirs.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

            QFileInfoList list = dirs.entryInfoList();
            for (int i = 0; i < list.size(); ++i) {
                QString dirName(list.at(i).fileName());
                QFile file(path + dirName + "/config.json");

                if(file.open(QIODevice::ReadOnly)){
                    QJsonObject json = QJsonDocument::fromJson(file.readAll()).object();
                    json["dir"] = dirName;
                    projectConfigs[dirName] = json;
                    file.close();
                } else {
                    QJsonObject defaultData;
                    defaultData["title"] = dirName + "'s config not found";
                    defaultData["dir"] = dirName;
                    projectConfigs[dirName] = defaultData;
                }
            }
        } else {
            foreach (QVariant dirName, contentStorage->pathMap["dirs"].toList()) {
                QString dir = dirName.toString();
                if(contentStorage->getFile("html/content/" + dir + "/config.json").isEmpty()){
                    QJsonObject defaultData;
                    defaultData["title"] = dir + "'s config not found";
                    defaultData["dir"] = dir;
                    projectConfigs[dir] = defaultData;
                }else {
                    QJsonObject config = QJsonDocument::fromJson(contentStorage->getFile("html/content/" + dir + "/config.json")).object();
                    config["dir"] = dir;
                    projectConfigs[dir] = config;
                }
            }
        }
    } else {
        projectConfigs = controlObj->projectConfigs;
    }

    return projectConfigs;
}

void Shell::auth(ShellObj *controlObject){
    loadPage("html/auth.html", controlObject);
}

void Shell::selectProject(ShellObj *controlObj, QJsonObject configs){
    QObject::connect(frame, &QWebFrame::loadFinished, [this, configs](){
        QString conf = QString::fromUtf8(QJsonDocument::fromVariant(configs.toVariantMap()).toJson());
        frame->evaluateJavaScript("app.buildProjectList(" + conf + ")");
    });
    loadPage("html/selectProject.html", controlObj);
}
