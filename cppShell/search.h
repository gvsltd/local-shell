#ifndef SEARCH_H
#define SEARCH_H
#include <QtCore>
#include <QWebView>

#include "encprotocol/encryptedstorage.h"

extern QString ROOT_PATH;

class SimpleSearch
{
    EncryptedStorage *storage;
    QList<QChar> delimiters;
    QString contentDir;
    QMap<int,QString> preparedText;
    QString cleaningText(QString html);
public:
    SimpleSearch(QJsonArray fileData, QString contentDir, EncryptedStorage *contentStorage);
    QJsonObject find(QString phrase);
    QString getSample(QString sourceText, QStringList wordsList);
    bool testWord(QString word, QStringList wordsList);
    QStringList cleanPhrase(QString phrase);
    QString extractSentence(int pos, QString text);
    bool wordIsValid(QString word);
};

#endif // SEARCH_H
