#include "shellObject.h"
#include "printthread.h"
#include "JlCompress.h"
#include "detachedProcess.h"

#include <QWebInspector>

extern QList<QString> consoleParams;
extern QString ROOT_PATH;
extern QString UPDATE_PATH;

ShellObj::ShellObj(Shell *shell, QJsonObject *user) : QObject(0){
    this->shell = shell;
    this->user = user;
    db = Shelldb::getInstance();
    db->storage = shell->contentStorage;

    salt = "QgE4Nn?KEsT[kO,uyx!{";
}

//public
//////////////

bool ShellObj::login(QString login, QString pass){
    QString passwordHash = QString(QCryptographicHash::hash((pass+salt).toUtf8(),QCryptographicHash::Md5).toHex());
    QJsonObject userData = db->login(login, passwordHash);

    if(bool(userData.size())){
        *user = userData;
    } else {
        qDebug() << "login error";
    }

    return bool(userData.size());
}

QString ShellObj::regMe(QString login, QString pass, QString fio){
    QString passwordHash = QString(QCryptographicHash::hash((pass+salt).toUtf8(),QCryptographicHash::Md5).toHex());
    return db->regMe(login,passwordHash,fio);
}

void ShellObj::openWorkPage(QString contentDir){
    if(contentDir.isEmpty()){
        contentDir = projectConfigs.keys()[0];
        this->contentDir = projectConfigs.keys()[0];
    }else{
        this->contentDir = contentDir;
    }

    QSettings acroSettings("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\AcroRd32.exe", QSettings::NativeFormat);
    acroPath = acroSettings.value("Default").toString();
    printDir = QStandardPaths::standardLocations(QStandardPaths::DataLocation)[0] + "/print/";
    updateDir = QStandardPaths::standardLocations(QStandardPaths::DataLocation)[0] + "/update/";

    db->setDBCat(contentDir);

    defBookmarks = db->getBookmark(user->value("id").toString().toInt());
    bookmarks = defBookmarks;

    connect(shell->frame, &QWebFrame::loadFinished, [this, contentDir]{
        QString config = encodeJson(projectConfigs[contentDir].toObject());
        shell->frame->evaluateJavaScript("app.initIndex(" + config + ")");
        shell->frame->disconnect();
    });
    shell->loadPage("html/desktop_index.html", this);
}

QString ShellObj::getBookmarks(){
    return encodeJson(db->getBookmark(user->value("id").toString().toInt()));
}

QString ShellObj::getLanguagePack(QString language){
    return encodeJson(db->getLanguagePack(language));
}

void ShellObj::saveUISettings(QString theme, QString interfaceFontSize, QString contentFontSize, QString font){
    QJsonObject params;
    params["theme"] = theme;
    params["interfaceFontSize"] = interfaceFontSize;
    params["contentFontSize"] = contentFontSize;
    params["font"] = font;
    db->saveUserSettings(params, user->value("id").toString().toInt());
}

QString ShellObj::getUserSettings(){
    QJsonObject userSettings = db->getUserSettings(user->value("id").toString().toInt());
    QString rp = "null";
    if(!userSettings.isEmpty()){
        rp = encodeJson(userSettings);
    }
    return rp;
}

QString ShellObj::getHrefs(){
    return encodeJson(db->getHrefs());
}

QString ShellObj::getNavi(){
    return encodeJson(db->getNavigation());
}

QString ShellObj::getJournal(){
    QJsonArray journalRecs = db->getJournal(user->value("id").toString().toInt());
    QList<int> temporaryTargetItemList;
    for(int i = 0; i < journalRecs.size(); i++){
        temporaryTargetItemList.append(journalRecs[i].toObject()["target_item"].toString().toInt());
    }
    targetItemList = temporaryTargetItemList;
    return encodeJson(journalRecs);
}

void ShellObj::addJournal(QString journalArr){
    QJsonArray journal = QJsonDocument::fromJson(journalArr.toUtf8()).array();
    journal.append(user->value("id").toString().toInt());
    if( (!targetItemList.isEmpty()) && targetItemList.contains(journal[0].toInt())){
        db->updateJournal(journal);
    }else{
        db->addJournal(journal);
    }
}

void ShellObj::delJournal(){
   db->delJournal(user->value("id").toString().toInt());
}

void ShellObj::addNote(int pageID, QString comment){
    db->addNote(user->value("id").toString().toInt(),pageID,comment);
}

void ShellObj::delNote(int noteId){
    db->delNote(noteId);
}

QString ShellObj::getResources(){
    return encodeJson(db->getResources());
}

QString ShellObj::getVocabulary(){
    return QString::fromUtf8(QJsonDocument::fromVariant(db->getVocabulary()).toJson());
}

void ShellObj::setLastViewedPage(QString type, QString parent, QString id){
    QJsonObject settings;
    settings["lastPageType"] = type;
    settings["lastPageParent"] = parent;
    settings["lastPageID"] = id;
    db->saveUserSettings(settings,user->value("id").toString().toInt());
}

QString ShellObj::getLastViewedPage(){
    QJsonObject data = db->getUserSettings(user->value("id").toString().toInt());

    if(data["lastPageID"].isNull() || !data["lastPageID"].toString().size()){
        return "null";
    }

    QJsonObject res;
    res["section"] = data["lastPageType"];
    res["parent"] = data["lastPageParent"];
    res["id"] = data["lastPageID"];
    return encodeJson(res);
}

void ShellObj::updateBookmarks(){
    for(int i = 0; i < bookmarks.size(); i++){
        if(!defBookmarks.contains(bookmarks[i])){
            defBookmarks.append(bookmarks[i]);
            addBookmark(bookmarks[i].toDouble(), "");
        }
    }
    for(int i = 0; i < defBookmarks.size(); i++){
        if(!bookmarks.contains(defBookmarks[i])){
            delBookmark(defBookmarks[i].toDouble());
            defBookmarks.removeAt(i);

        }
    }
}

QString ShellObj::getWorkmenu(){
    QJsonObject menu;
    int userID = user->value("id").toString().toInt();
    menu["bookmarks"] = db->getBookmarks(userID);
    menu["notes"] = db->getNotes(userID);
    menu["sels"] = db->getSelections(userID);
    menu["comments"] = db->getComments(userID);
    return encodeJson(menu);
}

void ShellObj::clearWorkmenu(){
    db->clearWorkMenu(user->value("id").toString().toInt());
}

void ShellObj::addBookmark(int pageID, QString comment){
    db->addBookmark(user->value("id").toString().toInt(), pageID, comment);
}

void ShellObj::delBookmark(int pageID){
    db->delBookmark(pageID);
}

void ShellObj::appendBookmark(int pageID){
    if (!bookmarks.contains(pageID)){
        bookmarks.append(pageID);
    }
    updateBookmarks();
}

void ShellObj::removeBookmark(int pageID){
    if(bookmarks.contains(pageID)){
        for(int i = 0; i < bookmarks.size(); i++){
            if(bookmarks[i].toDouble() == pageID){
                bookmarks.removeAt(i);
            }
        }
    }
    updateBookmarks();
}

void ShellObj::saveSel(int pageID, QString htmltext){
    db->saveSelection(pageID, htmltext, user->value("id").toString().toInt());
}

QString ShellObj::getSelection(int pageID){
    return db->getSelection(user->value("id").toString().toInt(), pageID);
}

void ShellObj::delSelections(int pageID){
    db->delSelections(pageID,user->value("id").toString().toInt());
}

void ShellObj::saveHighlightComment(int pageID, QString nom, QString comment, QString selection){
    db->saveHighlightComment(pageID, nom, comment, user->value("id").toString().toInt(), selection);
}

QString ShellObj::getHighlightComment(int pageID,QString nom){
    return db->getHighlightComment(user->value("id").toString().toInt(),pageID,nom);
}

void ShellObj::recSel(int pageID, QString nom, QString selection){
    db->recSel(user->value("id").toString().toInt(), pageID, nom, selection);
}

QString ShellObj::getNote(int pageID){
    return db->getNote(pageID, user->value("id").toString().toInt());
}

QString ShellObj::inBookmarks(int pageID){
    for(int i = 0; i < bookmarks.size(); i++){
        if(bookmarks[i].toDouble() == pageID) return "true";
    }
    return "false";
}

void ShellObj::delRecSel(int pageID, QString nom){
    db->delRecSel(user->value("id").toString().toInt(),pageID,nom);
    db->delHighlightComment(pageID,nom,user->value("id").toString().toInt());
}
void ShellObj::goHome(){
    shell->launchStartPage(this);
}

void ShellObj::delHighlightComment(int pageID, QString nom){
    db->delHighlightComment(pageID,nom,user->value("id").toString().toInt());
}

void ShellObj::copyToClipboard(){
    shell->webView->page()->action(QWebPage::Copy)->activate(QAction::Trigger);
}

void ShellObj::openUrl(QString url){
     QDesktopServices::openUrl(QUrl(url, QUrl::TolerantMode));
}

void ShellObj::openFile(QString file){
     openUrl("file:///" + ROOT_PATH + file);
}

void ShellObj::createPdf(QWebView *page){
    QString printUrl = printDir + "print.pdf";
    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(printUrl);
    page->print(&printer);
    PrintThread *printing = new PrintThread(acroPath, printDir, printUrl);
    printing->start();
//    QPrinter printer;
//    QPrintDialog *dialog = new QPrintDialog(&printer);
//    if (dialog->exec() != QDialog::Accepted) return;
//    page->print(&printer);
}

void ShellObj::print_work_contents(){
    if(acroPath.isEmpty()){
        adobeNotFoundErr();
    }else{
        QString script = "app.getWorkMenuForPrint('" + user->value("name").toString() + "')";
        QString html = shell->frame->evaluateJavaScript(script).toString();

        QDir dir(printDir);
        if(dir.removeRecursively()){
            dir.mkpath(printDir);
        }else{
            dir.mkpath(printDir);
        }
        QWebView *page = new QWebView();
        QWebFrame *frame = page->page()->mainFrame();

        connect(frame, &QWebFrame::loadFinished, [this, page, frame] (){
            //page->show();
            createPdf(page);
            frame->disconnect();
        });

        page->setHtml(html, QUrl("file:///" + ROOT_PATH + "html/templates/"));
    }
}

void ShellObj::html2printer(){
    QString printPagePath = "file:///" + ROOT_PATH + "html/journal_print.html";

    if(acroPath.isEmpty()){
        adobeNotFoundErr();
    }else{
        QDir dir(printDir);
        if(dir.removeRecursively()){
            dir.mkpath(printDir);
        }else{
            dir.mkpath(printDir);
        }
        QWebView *webView = new QWebView();
        QWebFrame *frame = webView->page()->mainFrame();
        frame->addToJavaScriptWindowObject("shellObj", this);
        webView->load(QUrl(printPagePath));
        //webView->show();

        webView->page()->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
        QWebInspector *webInspector = new QWebInspector();
        webInspector->setMinimumWidth(900);
        webInspector->setPage(webView->page());

        connect(frame, &QWebFrame::loadFinished, [this, webView, frame] (){
            createPdf(webView);
            frame->disconnect();
        });
    }
}

void ShellObj::printContentPage(int PageId){
    QString pagePath = db->getPageInfo(PageId)[0].toObject()["resource"].toString();
    QString printPagePath = "file:///" + ROOT_PATH + "html/content/" + contentDir + "/" + pagePath + ".html";

    if(acroPath.isEmpty()){
        adobeNotFoundErr();
    }else{
        QDir dir(printDir);
        if(dir.removeRecursively()){
            dir.mkpath(printDir);
        }else{
            dir.mkpath(printDir);
        }
        QWebView *page = new QWebView();
        page->resize(1100, page->size().height());
        QWebFrame *frame = page->page()->mainFrame();
        page->load(QUrl(printPagePath));
        //page->show();
        //enableInspector(page, true);

        connect(frame, &QWebFrame::loadFinished, [this, page, frame] (){
            createPdf(page);
            frame->disconnect();
        });
    }
}

void ShellObj::adobeNotFoundErr(){
    QWebView *printErrBox = new QWebView();
    QWebFrame *printErrBoxFrame = printErrBox->page()->mainFrame();
    printErrBox->resize(650, 270);
    printErrBox->setWindowTitle("Error");
    printErrBoxFrame->addToJavaScriptWindowObject("shellObj",this);
    printErrBox->load(QUrl("file:///" + ROOT_PATH + "html/adobe_err.html"));
    connect(printErrBoxFrame, &QWebFrame::loadFinished, [printErrBox, printErrBoxFrame] (){
        printErrBox->show();
        printErrBoxFrame->disconnect();
    });
}

QString ShellObj::getStrUserData(){
    QJsonArray array;
    array.append(QDate::currentDate().toString("dd.MM.yyyy"));
    array.append(user->value("name").toString());
    return encodeJson(array);
}

void ShellObj::initSearch(QString fileData){
    finder = new SimpleSearch(QJsonDocument::fromJson(fileData.toUtf8()).array(),this->contentDir, shell->contentStorage);
}

QString ShellObj::find(QString phrase){
    if(finder->find(phrase).isEmpty()){
        return "{}";
    }else{
        return encodeJson(finder->find(phrase));
    }
}

void ShellObj::checkForUpdate(){
    sendStatus(102);
    QNetworkRequest request = QNetworkRequest(QUrl(getUpdateLink(false)));
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QNetworkReply *reply = manager->get(request);

    connect(reply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), [this, reply]{
        reply->disconnect();
        sendStatus(500);
        reply->deleteLater();
    });

    connect(reply, &QNetworkReply::finished, [this, reply, manager](){
        QString response = QString::fromUtf8(reply->readAll());
        reply->disconnect();
        manager->deleteLater();
        reply->deleteLater();
        int status;

        if(response == "false"){
            status = 200;
        } else {
            status = 103;
        }
        sendStatus(status);
    });
}

void ShellObj::doUpdate(){
    QNetworkRequest request = QNetworkRequest(QUrl(getUpdateLink(true)));
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QNetworkReply *reply = manager->get(request);

    connect(reply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), [this, reply]{
        reply->disconnect();
        sendStatus(500);
        reply->deleteLater();
    });

    connect(reply, &QNetworkReply::downloadProgress, [this](qint64 bytesReceived, qint64 bytesTotal){
        int status = round(bytesReceived * 100 / bytesTotal);

        sendStatus(status);
    });

    connect(reply, &QNetworkReply::finished, [this, reply, manager](){
        QByteArray data(reply->readAll());
        sendStatus(101);

        reply->disconnect();
        manager->deleteLater();
        reply->deleteLater();

        saveUpdateArchive(data);
        extractUpdateArchive();

        db->cleanConnections();

        if(DetachedProcess().start(ROOT_PATH + "bin/update.exe", QStringList() << UPDATE_PATH << ROOT_PATH << consoleParams)){
            QApplication::quit();//завершаем текущий процесс при успешном старте дочернего
        }
    });
}

QString ShellObj::getUpdateLink(bool needContent){
    QString rs_str = "http://academia-moscow.ru/academia_scripts/updater/index.php?";
    QStringList dirList = projectConfigs.keys();
    for (int i = 0; i < dirList.size(); ++i) {
        QString dirName = dirList[i];
        QString ver;
        QFile configFile(ROOT_PATH + "html/content/" + dirName + "/config.json");
        if(configFile.open(QIODevice::ReadOnly)){
            QJsonObject json = QJsonDocument::fromJson(configFile.readAll()).object();
            ver = json.value("ver").toString();
            if(ver.isEmpty()) ver = "1";
            configFile.close();
        }
        rs_str += "iz=" + dirName + "&vc=" + ver + "&";
    }
    rs_str += "dl=" + QString::number((int)needContent);
    //rs_str += "vs=" + QString::number(APP_VERSION);
    //rs_str = "http://academia-moscow.ru/academia_scripts/updater/index.php?iz=601117263&vc=1&dl=" + QString::number((int)needContent);
    qDebug() << rs_str;
    return rs_str;
}

void ShellObj::saveUpdateArchive(QByteArray bytes){
    if(QDir(updateDir).exists()) QDir(updateDir).removeRecursively();
    QDir().mkpath(updateDir);
    QFile file(updateDir + "update.zip");
    if(file.open(QIODevice::WriteOnly)){
        file.write(bytes);
        file.close();
    }
}

void ShellObj::extractUpdateArchive(){
    QString updateArchiveDir = updateDir + "update.zip";
    JlCompress::extractDir(updateArchiveDir,updateDir);
    QFile(updateArchiveDir).remove();
}

void ShellObj::sendStatus(int status){
    shell->frame->evaluateJavaScript(QString("app.printDownloadUpdateDataStatus(%1)").arg(status));
}

//utils
////////////

QString ShellObj::encodeJson(const QJsonObject &json){
    return QString::fromUtf8(QJsonDocument::fromVariant(json.toVariantMap()).toJson());
}

QString ShellObj::encodeJson(const QJsonArray &json){
    return QString::fromUtf8(QJsonDocument::fromVariant(json.toVariantList()).toJson());
}

QJsonObject ShellObj::decodeJson(QString json){
    return QJsonDocument::fromJson(json.toUtf8()).object();
}

