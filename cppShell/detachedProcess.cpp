#include "detachedProcess.h"
#include <QDebug>

#include <QProcess>
#ifdef Q_OS_WIN
#include <qt_windows.h>
#include <shellapi.h>
#endif

DetachedProcess::DetachedProcess(){
}

bool DetachedProcess::start(const QString &command, QStringList &params){
    bool success;
    QString comCopy = command;
    comCopy = comCopy.replace("/", "\\");

    #ifdef Q_OS_WIN
    for(int i = 0; i < params.size(); i++){
        params[i] = '"' + params[i] + '"';
    }
    QString qparams = params.join(' ');

    LPCWSTR strCommand = (wchar_t*)(comCopy.utf16());
    LPCWSTR strParams = (wchar_t*)(qparams.utf16());

    int result = (int)::ShellExecute(0, L"open", strCommand, strParams, 0, SW_SHOWNORMAL);
    if (SE_ERR_ACCESSDENIED == result){
        // Requesting elevation
        result = (int)::ShellExecute(0, L"runas", strCommand, strParams, 0, SW_SHOWNORMAL);
    }
    success = result > 32;
    if(success){
        qDebug() << "success" << result;
    }else{
        qDebug() << "error. start process failed. status:" << QString::number(result);
    }
    #else
    QProcess update;
    success = update.startDetached(command, params);
    #endif

    return success;
}
