
var width = 0;
var height = 0;
var scrollTop = 0;
var scrollLeft = 0;

$(document).ready(function() {
	
	$( '#navigator-item-1' ).addClass( 'active' );
	//$( '.sections' ).css( 'width', $( '.section' ).length * 100 + '%' );
	//$( 'html,body' ).animate({ scrollLeft: 0 }, 'slow' );
	
});
function switchItem( ThisLink, target ) {
	
	if ( $( '#' + target ).is( ':visible' ) ) {
		$( '#' + target ).animate( { 'opacity': 0 }, 700, function() {
			$( '#' + target ).animate( { 'height': 0 + 'px' }, 400, function () {
				$( '#' + target ).css( 'display', 'none' );
				$( ThisLink ).removeClass( 'active' );
			} );
		} );
	} else {
		$( '#' + target ).css( 'display', 'block' );
		$( '#' + target ).css( 'height', 0 );
		$( '#' + target ).css( 'opacity', 0 );
		$( ThisLink ).addClass( 'active' );
		$( '#' + target ).animate( { 'height': $( '#' + target )[0].scrollHeight + 'px' }, 400, function() {
			$( this ).css( 'height', 'auto' );
			$( this ).animate( { 'opacity': 1 }, 700, function() {
			} );
		} );
	}
	
}
function scrollPageTo( number ) {
	
	if ( $( '#navigator-item-' + number ).hasClass( 'active' ) ) return;
	$( '#navigator-item-' + number ).parent().find( '.active' ).removeClass( 'active' );
	$( '#navigator-item-' + number ).addClass( 'active' );
	var pos = - ( number - 1 ) * 100 + '%';
	$( '.sections' ).animate({ 'left': pos }, 300 );
	
}
function openDialogue( A ) {
	
	var LinkID = $( A ).attr( 'id' );
	var Array = LinkID.split( '-', 2 );
	
	width = $( A ).width();
	height = $( A ).height();
	scrollTop = $( window ).scrollTop();
	scrollLeft = $( window ).scrollLeft();
	
	var WW = $( window ).width();
	var HH = $( window ).height();
	
	$( 'body' ).append( '<div id="window-' + Array[1] + '" class="window" />' );
	$( '#window-' + Array[1] ).css( 'display', 'table' );
	$( '#window-' + Array[1] ).html( '<div class="window-header"><div class="button window-close" onclick="closeDialogue(\'window-' + Array[1] + '\')"></div><div class="window-name">' + $( A ).find( '.name' ).html() + '</div></div><div class="window-content"></div>' );
	$( '#window-' + Array[1] ).find( '.window-content' ).append( $( A ).find( '.content' ).html() );
	$( '#window-' + Array[1] ).css( 'opacity', 1 );
	$( '#window-' + Array[1] ).css( 'width', WW );
	$( '#window-' + Array[1] ).css( 'height', HH );
	$( '#window-' + Array[1] ).css( 'left', 0 );
	$( '#window-' + Array[1] ).css( 'top', '100%' );
	
	/*var modals = $( '.window' );
	if ( modals.length <= 1 ) {
		$( '.sections' ).css( 'position', 'fixed' );
		$( '.sections' ).css( 'left', -scrollLeft );
	}*/
	
	$( '#window-' + Array[1] ).animate( { 'top': 0 }, 300, "linear", function() {
		$( '#window-' + Array[1] ).css( 'width', '100%' );
		$( '#window-' + Array[1] ).css( 'height', '100%' );
	});
	
}
function closeDialogue( WindowID ) {
	
	var Array = WindowID.split( '-', 2 );
	var WW = $( window ).width();
	var HH = $( window ).height();
	
	/*var modals = $( '.window' );
	if ( modals.length <= 1 ) {
		$( '.sections' ).css( 'position', 'relative' );
		$( '.sections' ).css( 'left', 0 );
	}*/
	
	$( window ).scrollTop( scrollTop );
	$( window ).scrollLeft( scrollLeft );
	
	$( '#' + WindowID ).animate( { 'top': HH }, 300, function() {
		$( '#' + WindowID ).removeClass( 'active' );
		$( '#' + WindowID ).css( 'display', 'none' );
		$( '#' + WindowID ).css( 'opacity', 0 );
		$( '#hidden' ).append( $( '#' + Array[1] ) )
		$( '#' + WindowID ).remove();
	});
	
}

/* END */
