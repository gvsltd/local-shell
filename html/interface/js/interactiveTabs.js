/**
 * Created with PyCharm.
 * User: Alex
 * Date: 12.03.14
 * Time: 11:47
 * To change this template use File | Settings | File Templates.
 */
(function(){
	function InteractiveTabs(container){
		var nav = container.find('#nav li');
		var content = container.find('#contents div');
		content.eq(0).show();
		var lastIndex = 0;
		var lastBtn = nav.eq(0).addClass('active');

		nav.click(function(){
			lastBtn.removeClass('active');
			$(this).addClass('active');

			var itemIndex = $(this).index();
			content.eq(lastIndex).hide();
			content.eq(itemIndex).show();

			lastIndex = itemIndex;
			lastBtn = $(this);
		});

		container.find('#nav > ul').add(container.find('#contents > .contentbox')).mCustomScrollbar({
			autoDraggerLength: true,
			scrollInertia: 300,
			mouseWheelPixels: 300,
			advanced: {
				updateOnBrowserResize: true,
				updateOnContentResize: true
			}
		});
	}

	window.InteractiveTabs = InteractiveTabs;
})();