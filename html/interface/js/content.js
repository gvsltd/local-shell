var container;
var content;

var collapsibles;
var proportionals;

var collapsibleFormulas = [];
var collapsible_images = [];
var collapsible_tables = [];

var defaultFont = 'Verdana';
var defaultFontSize = 17;

$(window).resize(function() {
	if (parent['toolPanel']) {
		parent.toolPanel.updatePosition();
	}

	if (collapsible_tables && collapsible_tables.length) {
		reviewCollapsibleTables();
	}

	if (collapsibleFormulas && collapsibleFormulas.length) {
		reviewCollapsibleImages(collapsibleFormulas);
	}
	if (collapsible_images && collapsible_images.length) {
		reviewCollapsibleImages(collapsible_images);
	}
	// проверка для мультитеста container =~ question current;
	if ((typeof container) != 'undefined' && container != null) {
		switch (true) {
			case (container.find(".plankImg").size() != 0):
				imgResize(container);
				//checkImgHeight(currentImg);
				break;
			case (container.find(".plankText").size() != 0):
				resizeTextBlocks(container);
				break;
			default:
				collapse_overpluses();
				break;
		}
	}
});

$(function() {

	var head = document.getElementsByTagName('head')[0];

	/* Default Theme CSS */

	if (!parent['set_content_theme']) {

		var link_theme_style = document.createElement('link');
		link_theme_style.setAttribute('id', 'theme');
		link_theme_style.setAttribute('rel', 'stylesheet');
		link_theme_style.setAttribute('type', 'text/css');
		link_theme_style.setAttribute('href', '../../interface/themes/DefaultTheme/css/content.css');
		link_theme_style.setAttribute('media', 'all');
		head.appendChild(link_theme_style);

	}

	/* Custom Project CSS */

	var link_custom_style = document.createElement('link');
	link_custom_style.setAttribute('rel', 'stylesheet');
	link_custom_style.setAttribute('type', 'text/css');
	link_custom_style.setAttribute('href', 'design/custom.css');
	link_custom_style.setAttribute('media', 'all');
	head.appendChild(link_custom_style);

	/* Default Font CSS */

	if (!parent['set_content_font_family']) {

		var link_font_style = document.createElement('link');
		link_font_style.setAttribute('id', 'font');
		link_font_style.setAttribute('rel', 'stylesheet');
		link_font_style.setAttribute('type', 'text/css');
		link_font_style.setAttribute('href', '../../interface/fonts/PTSerif/stylesheet.css');
		link_font_style.setAttribute('media', 'all');
		head.appendChild(link_font_style);

		var body = document.getElementsByTagName('body')[0];
		body.style.fontFamily = defaultFont;
		body.style.fontSize = defaultFontSize + 'px';
	}

	if ($('#wrapper').hasClass('title')) {

		var link_2 = document.createElement('link');
		link_2.setAttribute('id', 'theme');
		link_2.setAttribute('rel', 'stylesheet');
		link_2.setAttribute('type', 'text/css');
		link_2.setAttribute('href', '../../interface/themes/DefaultTheme/css/title.css');
		link_2.setAttribute('media', 'all');
		head.appendChild(link_2);

		setTimeout(function() {
			$(".name-IN").fitText();
		}, 100);

	}

	/* PROPORTIONAL IMAGES */

	var proportional_images = $('.proportional');
	proportional_images.each(function() {
		var e = $(this);
		e.load(function() {
			if (e.width() != 0) {
				e.css('width', (e.width() / 16).toFixed(1) + 'em');
			} else {
				setTimeout(function() {
					e.css('width', (e.width() / 16).toFixed(1) + 'em');
					// alert(e.width());
				}, 100);
			}
		});
	});

	var proportional_images33 = $('.proportional-33');
	proportional_images33.each(function() {
		var e = $(this);
		e.load(function() {
			$(this).css('width', ($(this).width() / 33).toFixed(1) + 'em');
		});
	});

	$('.link-anchor').click(function(event) {
		event.preventDefault();
		var e = $(this).attr('href');
		var new_scroll = $(e).position().top;
		$('html, body').animate({
			scrollTop: new_scroll
		}, 300);
	});

	$('.link-tooltip').each(function() {
		var e = $($(this).attr('href'));
		$(this).removeAttr('href');
		$(this).attr('title', e.html());
	});

	$('.link-tooltip').tooltipster({
		maxWidth: 500,
		animation: 'fade',
		contentAsHTML: true,
		trigger: 'hover',
		speed: 50,
		interactive: true,
		functionReady: function(e) {
			$(e).addClass('pressed');
		},
		functionAfter: function(e) {
			$(e).removeClass('pressed');
		}
	});

	$('.scheme-tooltip').each(function() {
		var e = $($(this).attr('href'));
		$(this).removeAttr('href');
		$(this).attr('title', e.html());
	});
	$('.scheme-tooltip').tooltipster({
		maxWidth: 800,
		//positionTracker: true,
		animation: 'fade',
		contentAsHTML: true,
		trigger: 'click',
		speed: 250,
		position: 'bottom',
		functionReady: function(e) {
			$(e).addClass('pressed');
		},
		functionAfter: function(e) {
			$(e).removeClass('pressed');
		}
	});

	$('.tabs-tooltip').each(function() {
		var e = $($(this).attr('href'));
		$(this).removeAttr('href');
		$(this).attr('title', e.html());
	});

	$('.tabs-tooltip').tooltipster({
		maxWidth: 500,
		//positionTracker: true,
		animation: 'fade',
		contentAsHTML: true,
		trigger: 'click',
		speed: 250,
		functionReady: function(e) {
			$(e).addClass('pressed');
		},
		functionAfter: function(e) {
			$(e).removeClass('pressed');
		}
	});

    $('.int-picture-tooltip').each(function() {
        var e = $($(this).attr('href'));
        $(this).removeAttr('href');
        $(this).attr('title', e.html());
    });
    $('.int-picture-tooltip').tooltipster({
        maxWidth: 500,
        //positionTracker: true,
        animation: 'fade',
        contentAsHTML: true,
        trigger: 'click',
        speed: 250,
        //timer: 4000,
        functionReady: function(e) {
            $(e).addClass('pressed');
            $('.box_close_button').click(function(){
                $(e).tooltipster('hide');
            });
        },
        functionAfter: function(e) {
            $(e).removeClass('pressed');
        }
    });

	setupAnswerBtns();
});

function initCollapsibles(){
	initCollapsibleFormulas();
	initCollapsibleImages();
	initCollapsibleTables();
}

function setupAnswerBtns() {
	$('.show-answers').click(function() {
		$(this).next().slideToggle();
	});
}

function initCollapsibleFormulas() {
	var collapsibles = $('.formula-content').find('img.collapsible');

	collapsibles.each(function() {
		var collapsible = $(this);

		var iWrapper = $('<div class="collapsible-image-wrapper" />').addClass('collapsed');
		iWrapper.insertAfter(collapsible);
		iWrapper.append(collapsible);

		var zoom = $('<div class="zoom" />');
		iWrapper.append(zoom);

		var thumb = $('<img class="thumb" />');
		thumb.attr('src', collapsible.attr('src'));
		zoom.append(thumb);

		collapsibleFormulas.push(iWrapper);

		zoom.click(function() {
			showModal(collapsible.clone().removeClass('collapsible'), function(wrapper, body, modal) {
				imageModalCloseHandler(zoom, wrapper, body, modal);
			}, zoom);
		});
	});

	setTimeout(function() {
		reviewCollapsibleImages(collapsibleFormulas);
	}, 100);
}

function initCollapsibleImages() {
	var collapsibles = $('.figure').has('.collapsible');

	collapsibles.each(function() {
		var collapsible = $(this);
		var image = collapsible.find('.figure-content');
		var caption = collapsible.find('.figure-caption');

		var iWrapper = $('<div class="collapsible-image-wrapper" />').addClass('collapsed');
		iWrapper.insertAfter(image);
		iWrapper.append(collapsible.contents());

		var zoom = $('<div class="zoom" />');
		iWrapper.append(zoom);

		var thumb = $('<div class="zoom-caption"/>');
		thumb.text(caption.text().split(':')[0]);
		zoom.append(thumb);

		collapsible_images.push(iWrapper);

		zoom.click(function() {
			showModal(image.add(caption).clone().removeClass('collapsible'), function(wrapper, body, modal) {
				imageModalCloseHandler(zoom, wrapper, body, modal);
			}, zoom);
		});
	});

	setTimeout(function() {
		reviewCollapsibleImages(collapsible_images);
	}, 100);
}

function reviewCollapsibleImages(images) {
	for (var i = 0; i < images.length; i++) {
		var iWrapper = images[i];
		var image = $(iWrapper).find('img.collapsible');
		$(iWrapper).addClass('collapsed');

		if (image.width() < iWrapper.width() - 25) {
			$(iWrapper).removeClass('collapsed');
		}
	}

	if (parent['toolPanel']) parent.toolPanel.updatePosition();
}

function imageModalCloseHandler(zoom, wrapper, body, modal) {
	$('html, body').scrollTop(getScrollPos(zoom));
	wrapper.css({
		position: 'relative',
		left: '100%'
	});
	$('body').css({
		'overflow': 'auto',
		'padding-right': 0
	});
	wrapper.animate({
		left: 0
	}, 200, function() {
		wrapper.removeAttr('style').removeClass('frozen');
		zoom.parent().removeClass('pressed');
		if (parent['toolPanel']) parent.toolPanel.updatePosition();
	});
	modal.animate({
		'left': '-100%'
	}, 200, function() {
		modal.remove();
	});
}

function initCollapsibleTables() {
	var collapsibles = $('table.collapsible');

	collapsibles.each(function(i) {
		var collapsibleTable = $(this);

		var tWrapper = $('<div class="collapsible-table-wrapper collapsed" />');
		tWrapper.insertAfter(collapsibleTable);
		tWrapper.append(collapsibleTable);

		var zoom = $('<div class="zoom" />');
		zoom.attr('data-number', i);
		var a = collapsibleTable.find('caption');
		if (a.html() != null && a.html() != '') {
			zoom.html(a.html());
		} else {
			zoom.html('Таблица')
		}
		tWrapper.append(zoom);
		collapsible_tables.push(tWrapper);

		// if ( collapsibleTable.width() < tWrapper.width() - 25 ) {
		// 	$( tWrapper ).removeClass('collapsed');
		// }

		zoom.click(function() {
			collapsibleTable.css('display', 'table');
			showModal(collapsibleTable, function(wrapper, body, modal) {
				tableModalCloseHandler(zoom, wrapper, body, modal, tWrapper, collapsibleTable);
			}, zoom);
		});
	});

	setTimeout(reviewCollapsibleTables, 300);
}

function showModal(content, closeHandler, zoom) {
	zoom.parent().addClass('pressed');

	var modal = createModal();
	var body = $('body');
	body.append(modal);

	var wrapper = $('#wrapper');
	wrapper.css({
		position: 'relative',
		left: '0'
	});

	$('html, body').animate({
		scrollTop: getScrollPos(zoom)
	}, 200, function() {
		body.css({
			'overflow': 'hidden',
			'padding-right': '17px'
		});
		wrapper.animate({
			left: '100%'
		}, 400, function() {
			wrapper.removeAttr('style').addClass('frozen');
			if (parent['toolPanel']) parent.toolPanel.updatePosition();
		});
		modal.animate({
			left: 0
		}, 400, function() {
			modal.removeAttr('style');
		})
	});

	var modal_content = modal.find('.modal-content');
	modal_content.append(content);

	modal.find('.modal-close').click(function() {
		closeHandler(wrapper, body, modal);
	});
}

function createModal() {
	var string = '';
	string += '<div class="modal-IN">';
	string += '<div class="modal-main">';
	string += '<div class="modal-container">';
	string += '<div class="modal-container-IN">';
	string += '<div class="modal-content">';
	string += '</div>';
	string += '</div>';
	string += '</div>';
	string += '</div>';
	string += '<div class="modal-close">';
	string += '</div>';
	string += '</div>';

	var modal = $('<div class="modal"/>');
	modal.css({
		position: 'fixed',
		left: '-100%'
	});
	modal.html(string);

	return modal;
}

function tableModalCloseHandler(zoom, wrapper, body, modal, tWrapper, collapsibleTable) {
	$('html, body').scrollTop(getScrollPos(zoom));
	wrapper.css({
		position: 'relative',
		left: '100%'
	});
	body.css({
		'overflow': 'auto',
		'padding-right': 0
	});
	wrapper.animate({
		left: 0
	}, 200, function() {
		wrapper.removeAttr('style').removeClass('frozen');
		zoom.parent().removeClass('pressed');
		if (parent['toolPanel']) parent.toolPanel.updatePosition();
	});
	modal.animate({
		'left': '-100%'
	}, 200, function() {
		tWrapper.append(collapsibleTable);
		collapsibleTable.hide();
		modal.remove();
	});
}

function reviewCollapsibleTables() {
	for (var i = 0; i < collapsible_tables.length; i++) {
		var tWrapper = collapsible_tables[i];
		var table = $(tWrapper).find('table');
		$(tWrapper).addClass('collapsed');

		if (table.width() < tWrapper.width() - 25) {
			$(tWrapper).removeClass('collapsed');
		}
	}

	if (parent['toolPanel']) parent.toolPanel.updatePosition();
}

function getScrollPos(zoom) {
	return Math.floor(zoom.parent().offset().top) - Math.floor($(window).height() / 2) + Math.floor(zoom.parent().height() / 2);
}

/* FOR MULTITEST */

function init_collapsibles() {
	// debugger

	if ((typeof container) != 'undefined' && container != null) {

		content = $(container).find('.collapsibles-content');

		init_proportionals();

		collapsibles = container.find('.collapsible-2');
		collapsibles.find('.loupe').remove();
		var loupes = $('<a class="loupe" />').appendTo(collapsibles);

		// collapse_overpluses();

		loupes.on("click", loupesClick);
		loupes.each(function() {
			this.addEventListener('touchend', loupesClick, false);
				// body...
		});

		function loupesClick() {
			var parent = $(this).parent();
			var direction = 'to-left';
			var right = Math.floor(container.width() * 7 / 10);
			var left = -right;
			if ($(this).parent().hasClass('from-left')) {
				direction = 'to-right';
				left = Math.floor(container.width() * 7 / 10);
				right = -left;
			}
			var string = '';
			var text = $(this).parent().find('.text-2').html();
			var image = $(this).parent().find('.image').html();
			if ((typeof text) != 'undefined' && text != null) string += text;
			if ((typeof image) != 'undefined' && image != null) string += image;
			container.attr('style', 'position: absolute');
			parent.addClass('pressed');
			container.animate({
				left: left,
				right: right
			}, 300, function() {
				container.removeAttr('style');
				container.addClass('minimized').addClass(direction);
				var zoomback = $('<div class="zoomback"/>').appendTo(container);
				zoomback.css('opacity', 0);
				zoomback.animate({
					'opacity': 1
				}, 300, function() {
					zoomback.removeAttr('style')
				});
				var expanded = $('<div class="expanded"/>').appendTo(container.parent());
				expanded.css('opacity', 0);
				expanded.html('<div class="expanded-table"><div class="expanded-cell"><div class="substrate">' + string + '</div></div></div>');
				// expanded.mCustomScrollbar({
				// 	autoDraggerLength: true,
				// 	scrollInertia: 800,
				// 	mouseWheelPixels: 300,
				// 	advanced: {
				// 		updateOnBrowserResize: true,
				// 		updateOnContentResize: true
				// 	}
				// });
				expanded.animate({
					'opacity': 1
				}, 300, function() {
					expanded.removeAttr('style')
				});

				zoomback.add($(".expanded-table")).on("click", zoombackGetBack);

				zoomback[0].addEventListener('touchend', zoombackGetBack, false);

				function zoombackGetBack() {
					// debugger
					var zb = zoomback;
					expanded.animate({
						'opacity': 0
					}, 300, function() {
						expanded.remove();
						container.animate({
							right: 0,
							left: 0
						}, 300, function() {
							container.removeAttr('style');
							container.removeClass('minimized').removeClass(direction);
							parent.removeClass('pressed');
							zoomback.remove();
						});
					});

				}
			});
		}

	}

}

function collapse_overpluses() {

	if ((typeof container) != 'undefined' && container != null) {

		collapse_proportionals();

		collapsibles.removeClass('collapsed');
		var collapsed = 0;
		while (((container.offset().top + container.height()) < (content.offset().top + content.height())) && collapsibles.length > collapsed) {
			max = collapsibles[0];
			collapsibles.each(function() {
				if ($(this).height() >= $(max).height()) max = $(this);
			});
			$(max).addClass('collapsed');
			collapsed++
		}
	}

}


function init_proportionals() {

	/*var inline_images = $( '.inline-image' );
	 inline_images.each( function() {
	 var e = $( this );
	 e.load( function() { e.css( 'height', e.height()/10 + 'em' ) });
	 });*/

	proportionals = $(content).find('.outline-image');
	collapse_proportionals();
	//alert( proportionals );
	/*proportionals.each( function() {
	 var e = $( this ).find( 'img' );
	 e.load( function() { e.css( 'width', e.width()/10 + 'em' ) });
	 });*/

}

function collapse_proportionals() {

	proportionals.each(function() {
		var e = $(this);
		var image = e.find('img');
		if (image.width() > e.width()) {
			e.parent().parent().addClass('x-collapsed');
		} else {
			e.parent().parent().removeClass('x-collapsed');
		}
	});

}