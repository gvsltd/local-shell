﻿var path = 'content';
var surfingHistory = new History();
var resourceTypeData;

var toolPanel;

var themes_list = {
	'DefaultTheme': 'Тема по умолчанию (светлая тема)'
};

var fonts_list = {
	'Verdana': 'Verdana',
	'PTSans': 'PTSans',
	'PTSerif': 'PTSerif',
	'CaviarDreams': 'CaviarDreams'
};

var size_medium = 15,
	size_large = 16,
	size_x_large = 18;

var last_tool = 'work'; // Вкладка на панели инструментов (была активной перед последним выходом)
var current_tool = ''; // Переменная для хранения текущей вкладки на панели инструментов

var current_theme = 'DefaultTheme'; // Текущая тема оформления
var current_content_font_family = 'PTSerif'; // Текущий шрифт контента
var current_interface_font_size = 16; // Размер шрифта интерфейса, влияет на масштаб элементов интерфейса
var current_content_font_size = 15; // Размер шрифта контента, в том числе и модулей и ресурсов

var navigation_collapsed = false;
var toolbar_collapsed = false;

var ebookProgressItem;
var eappProgressItem;


var browserName = navigator.appName;

function progressBarHack() {
	if (browserName == "Microsoft Internet Explorer") {
		var progress = $('.progress');
		progress.each(function() {
			var exclude = 0;
			var wide_row = $(this).parent().parent();
			wide_row.parent().children().each(function() {
				if ($(this).attr('class') != 'wide-row') exclude += $(this).height();
			});
			$(this).height($(window).height() - exclude);
		});
		var contents = $('.contents');
		contents.each(function() {
			var exclude = 0;
			var wide_row = $(this).parent().parent();
			wide_row.parent().children().each(function() {
				if ($(this).attr('class') != 'wide-row') exclude += $(this).height();
			});
			$(this).height($(window).height() - exclude);
		});
		var tab_content = $('.tab-content');
		tab_content.each(function() {
			var exclude = 0;
			var wide_row = $(this).parent().parent();
			wide_row.parent().children().each(function() {
				if ($(this).attr('class') != 'wide-row') exclude += $(this).height();
			});
			$(this).height($(window).height() - exclude);
		});
		var toolbar_menu_item = $('.toolbar-menu-item');
		var H = ($(window).height() - $('.call-navbar-wrapper').height()) / $(toolbar_menu_item).length;
		$(toolbar_menu_item).height(H);

	}
}

/* MAIN FUNCTION */

function init() {

	// Установка размера шрифта интерфейса и выбор темы оформления

	backendBridge.getUserSettings(function(userSettings) {
		//возможность пользовательских настроек отключена
		if (false && userSettings.theme.length) {
			current_theme = userSettings.theme;
			current_interface_font_size = userSettings.interfaceFontSize;
			current_content_font_size = userSettings.contentFontSize;
			current_content_font_family = userSettings.font;
		}

		if (current_theme) set_interface_theme(current_theme);

		//themes_list_constructor();
                    
        fonts_list_constructor();

		var zoom_buttons = $('#zoom-buttons');

		var zoom_medium = $(zoom_buttons).find('#zoom-medium');
		var zoom_large = $(zoom_buttons).find('#zoom-large');
		var zoom_x_large = $(zoom_buttons).find('#zoom-x-large');

		var medium_large = size_medium + Math.floor((size_large - size_medium) / 2);
		var large_x_large = size_large + Math.floor((size_x_large - size_large) / 2);
		var font_size = Math.min(current_content_font_size, current_interface_font_size);

		switch (true) {
			case font_size <= medium_large:
				font_size = size_medium;
				zoom_medium.prop('checked', true);
				break;
			case medium_large < font_size && font_size <= large_x_large:
				font_size = size_large;
				zoom_large.prop('checked', true);
				break;
			case large_x_large < font_size:
				font_size = size_x_large;
				zoom_x_large.prop('checked', true);
				break;
		}
		current_content_font_size = font_size;
		current_interface_font_size = font_size;

		var mediumLabel = $('#label-' + zoom_medium.attr('id'));
		var largeLabel = $('#label-' + zoom_large.attr('id'));
		var xLargeLabel = $('#label-' + zoom_x_large.attr('id'));

		mediumLabel.attr('size', size_medium);
		largeLabel.attr('size', size_large);
		xLargeLabel.attr('size', size_x_large);
		mediumLabel.css('font-size', size_medium + 'px');
		largeLabel.css('font-size', size_large + 'px');
		xLargeLabel.css('font-size', size_x_large + 'px');

		zoom_buttons.find('label').click(function() {
			var size = parseInt($(this).attr('size'));
			current_content_font_size = size;
			current_interface_font_size = size;
			set_interface_font_size(size);

			var ebook_i = document.getElementById('ebook').getElementsByTagName('iframe')[0];
			var eapp_i = document.getElementById('eapp').getElementsByTagName('iframe')[0];
			var modal = document.getElementById('modal-iframe-container');
            
            if (ebook_i) set_content_font_size(ebook_i, size);
			if (eapp_i) set_content_font_size(eapp_i, size);
			if (modal) set_content_font_size(modal, size);
		});

		set_interface_font_size(current_interface_font_size);
	});

	/* INTERFACE BUTTONS */

	$('#dashboard').click(function() {
		alert('Dashboard');
	});
	$('#home').click(function() {
		shellObj.goHome();
	});

	var navbar = $('#navbar');
	$('#navbar-trigger').click(function() { // Кнопка collapse и expand для меню "Навигации" - доступна в полноэкранных режимах M (Medium) и L (Large)
		if (toolPanel) toolPanel.updatePosition();
		if (navbar.hasClass('collapsed')) {
			navbar.removeClass('opened');
			navbar.removeClass('collapsed');
		} else {
			navbar.removeClass('opened');
			navbar.addClass('collapsed');
		}
		progressBarHack();
	});

	var toolbar = $('#toolbar');
	$('#toolbar-trigger').click(function() { // Кнопка collapse и expand для меню "Инструментов" - доступна в полноэкранных режимах M (Medium) и L (Large)
		if (toolPanel) toolPanel.updatePosition();
		if (toolbar.hasClass('collapsed')) {
			toolbar.removeClass('opened');
			toolbar.removeClass('collapsed');
		} else {
			toolbar.removeClass('opened');
			toolbar.addClass('collapsed');
		}
		progressBarHack();
	});
	$('#call-toolbar').click(function() { // Переключалка между типами меню "Навигация" или "Инструменты" - доступна в малоэкранном режиме S (Small)
		navbar.removeClass('opened');
		navbar.addClass('hidden');
		toolbar.addClass('opened');
		progressBarHack();
	});
	$('#call-navbar').click(function() { // Переключалка между типами меню "Навигация" или "Инструменты" - доступна в малоэкранном режиме S (Small)
		toolbar.removeClass('opened');
		navbar.removeClass('hidden');
		navbar.addClass('opened');
		progressBarHack();
	});
	$('#group').click(function() { // Кнопка раскрытия меню "Навигации" при свернутом режиме
		toolbar.removeClass('opened');
		navbar.addClass('opened');
		progressBarHack();
	});
	$('#show-navigation').click(function() { // Кнопка раскрытия меню "Навигации" при в режиме xS (X-small)
		toolbar.removeClass('opened');
		navbar.removeClass('hidden');
		navbar.addClass('opened');
		progressBarHack();
	});
	$('.overlay').click(function() { // Кнопка "Закрыть содержание" - открывается как оверлей поверх всего, но под списком содержания
		navbar.removeClass('hidden');
		navbar.removeClass('opened');
		toolbar.removeClass('opened');
	});

	// Установка коллапсов интерфейса
	if (navigation_collapsed) navbar.addClass('collapsed');
	if (toolbar_collapsed) toolbar.addClass('collapsed');

	/* TOOLBAR INIT */

	links_list_constructor();

	open_toolbar(last_tool);

	$('.toolbar-menu-item').click(function() {
		if ($(this).hasClass('disabled')) return;
		var new_tool = $(this).attr('id').split('-')[0];
		open_toolbar(new_tool);
	});

	$('.popup-close').click(function() {
		$('.popup-overlay').removeClass('active');
	});

	var updateStatus = $('#updateStatus');
	$('#update').click(function() {
		$('#update-the-box').hide();
		updateStatus.show();
		updateStatus.html('Установка соединения с сервером.');
		backendBridge.checkForUpdate();
	});

	setTimeout(progressBarHack, 100);

}

/* CONTENT CONSTRUCTORS */

function resources_list_constructor(resourceData) {
	if ($.isEmptyObject(resourceData)) {
		$('#resources-menu').parent().parent().remove();
		return;
	}

	resourceTypeData = {
		'gallery': {
			'label': 'Слайд-шоу',
			'preview': 'interface/themes/' + current_theme + '/images/resources/galleries.svg',
			'dir': path
		},
		'picture': {
			'label': 'Рисунки',
			'preview': 'interface/themes/' + current_theme + '/images/resources/pictures.svg',
			'dir': path
		},
		'image': {
			'label': 'Изображения',
			'preview': 'interface/themes/' + current_theme + '/images/resources/images.svg',
			'dir': path
		},
		'audio': {
			'label': 'Аудио',
			'preview': 'interface/themes/' + current_theme + '/images/resources/audios.svg',
			'dir': path
		},
		'video': {
			'label': 'Видео',
			'preview': 'interface/themes/' + current_theme + '/images/resources/videos.svg',
			'dir': path
		},
		'tabulas': {
			'label': 'Вкладки',
			'preview': 'interface/themes/' + current_theme + '/images/resources/tabs.svg',
			'dir': path
		},

		'page': {
			'label': 'Теория',
			'preview': 'interface/themes/' + current_theme + '/images/resources/pages.svg',
			'dir': path
		},
		'multitest': {
			'label': 'Тесты',
			'preview': 'interface/themes/' + current_theme + '/images/resources/tests.svg',
			'dir': path
		}
	};

	var target_nav = 'resources';

	var tab = $('#resources-tab');
	var contents = tab.find('.tab-content');

	var contents_string = '<ul class="list">';

	for (var type in resourceData) {
		// Элемент содержания бывает двух типов: group и separator
		contents_string += '<li class="resources-group">';

		// формируем group-head
		contents_string += '<a href="#" class="group-head" target_nav="' + target_nav + '" target_group="' + type + '">';

		var element_preview = resourceTypeData[type]['preview'];
		if (element_preview) contents_string += '<div class="preview"><img src="' + element_preview + '" /></div>';
		contents_string += '<div class="text">' + resourceTypeData[type]['label'] + '</div>';
		contents_string += '<div class="arrow"><span></span></div>';

		contents_string += '</a>';

		// формируем items (встречается только у group)

		var items = resourceData[type];
		contents_string += '<ul class="items">';
		for (var i in items) {
			var item = items[i];
			contents_string += '<li class="resources-item">';
			contents_string += '<a href="#" class="item-head ' + item['class'] + '" link="' + item['link'] + '">' + item['name'] + '</a>';
			contents_string += '</li>'; // закрывает item
		}
		contents_string += '</ul>'; // закрывает items

		contents_string += '</li>'; // закрывает group или separator
	}

	contents_string += '</ul>';

	contents.html(contents_string);

	var opened;
	contents.find('.list > li').click(function() {
		var item = $(this);
		if (item.hasClass('opened')) {
			item.removeClass('opened');
			opened = null;
		} else {
			if (opened) opened.removeClass('opened');
			item.addClass('opened');
			opened = item;
		}
	});

	/* buttons */

	contents.find('.item-head').click(function() {
		var resourceType = contents.find('.list > li').has($(this)).find('.group-head').attr('target_group');
		open_modal($(this).attr('link'), resourceType);
	});

	contents.mCustomScrollbar({
		autoDraggerLength: true,
		scrollInertia: 800,
		mouseWheelPixels: 300,
		advanced: {
			updateOnBrowserResize: true,
			updateOnContentResize: true
		}
	});
}

function work_list_constructor() {
	app.getWorkMenu(function(workMenu) {
		var tab = $('#work-tab');
		var contents = tab.find('#work-contents');
		
		var contents_string = 
		'<div class="filter">\
			<label for="work-filter">' + langPack['sShow'] + '</label>\
			<select id="work-filter">\
				<option id="work-All" value="work-All">' + langPack['sall'] + '</option>\
				<option id="work-Bookmarks" value="work-Bookmarks">' + langPack['sbookmarks'] + '</option>\
				<option id="work-Notes" value="work-Notes">' + langPack['snotes'] + '</option>\
				<option id="work-Highlights" value="work-Highlights">' + langPack['sselected'] + '</option>\
			</select>\
		</div>\
		\
		<ul class="list">';

		contents_string += workMenu;

		contents_string += '</ul>';
		
		contents.html(contents_string);

		contents.find('.head').click(function() {
			var $this = $(this);

			if ($this.parent().hasClass("highlight")) {
				var key = this.parentNode.dataset.key;
				setTimeout(function() {
					var iframeDoc = $($('iframe')[0].contentDocument);
					var body = iframeDoc.find('body');
					var targetElem = iframeDoc.find('.' + key);

					$(body).scrollTop(targetElem.offset().top - 10);

					$('#highlight-note').remove();
					app.getHighlightComment(key, function(comment) {
						if (comment.length) app.addHighlightComment(key, 600, 130);
					});
				}, 1000);
			}
			open_navbar($this.attr('target_nav'), $this.attr('target_group'), $this.attr('target_id'));
			backendBridge.setLastViewedPage($this.attr('target_nav'), $this.attr('target_group'), $this.attr('target_id'));
		});

		contents.mCustomScrollbar({
			autoDraggerLength: true,
			scrollInertia: 800,
			mouseWheelPixels: 300,
			advanced: {
				updateOnBrowserResize: true,
				updateOnContentResize: true
			}
		});

		$("#work-filter").on("change", function(e) {
			function switchAll() {
				$("#work-contents .item").show();
				console.log("Все")
			}

			function switchBookmarks() {
				$("#work-contents .item").hide();
				$("#work-contents .item.bookmark").show();
				console.log("Закладки")
			}

			function switchNotes() {
				$("#work-contents .item").hide();
				$("#work-contents .item.note").show();
				console.log("Комментарии")
			}

			function switchHighlights() {
				$("#work-contents .item").hide();
				$("#work-contents .item.highlight").show();
				console.log("Выделенное")
			}

			console.log("TargetNumber " + e.target.options.selectedIndex.toString());
			switch (e.target.options.selectedIndex) {
				case 0:
					switchAll();
					break;
				case 1:
					switchBookmarks();
					break;
				case 2:
					switchNotes();
					break;
				case 3:
					switchHighlights();
					break;
			}
		});
	});
}

function vocabularyConstructor(vocabularyData) {
	if ($.isEmptyObject(vocabularyData)) {
		$('#vocabular-menu').parent().parent().remove();
		return;
	}

	var vocabulary = [];
	for (var term in vocabularyData) {
		var letter = term.charAt(0);
		if (letter == '«') letter = term.charAt(1);

		if (!vocabulary[letter]) vocabulary[letter] = [];
		vocabulary[letter].push(term);
	}

	for (letter in vocabulary) {
		vocabulary[letter].sort();
	}

	var alphabet = $('#vocabular-alphabet').empty();
	var contents = $('#vocabular-contents').empty();

	var list = $('<ul class="list"/>').appendTo(contents);

	var sortedLetters = Object.keys(vocabulary).sort();
	for (var i in sortedLetters) {
		letter = sortedLetters[i];
		alphabet.append('<a href="#anchor-' + letter + '" id="' + letter + '" class="alphabet-letter">' + letter + '</a>');
		list.append('<li id="anchor-' + letter + '" class="vocabular-letter">' + letter + '</li>');
		var terms = vocabulary[letter];
		for (term in terms) {
			var string = '';
			string += '<li class="vocabular-item">';
			string += '<a href="#" class="term">' + terms[term] + '</a>';
			string += '<span class="description">' + vocabularyData[terms[term]] + '</span>';
			string += '</li>';
			list.append(string);
		}
	}

	contents.mCustomScrollbar({
		autoDraggerLength: true,
		scrollInertia: 800,
		mouseWheelPixels: 300,
		advanced: {
			updateOnBrowserResize: true,
			updateOnContentResize: true
		}
	});

	alphabet.find('.alphabet-letter').click(function() {
		contents.mCustomScrollbar('scrollTo', '#anchor-' + $(this).attr('id'));
	});
	contents.find('.vocabular-item').click(function() {
		if ($(this).hasClass('opened')) {
			$(this).removeClass('opened');
		} else {
			$(this).addClass('opened');
		}
	});

	/* ebanko */

	contents.find('.vocabular-item:last-child').find('.term').click(function() {
		if (!$(this).hasClass('active')) return;
		contents.mCustomScrollbar("scrollTo", 'bottom', {
			scrollInertia: 0
		});
	});
}

function links_list_constructor() {
	var contents = $('#links-contents');
	contents.html('');
	var contents_string = '<ul class="list">';

	backendBridge.getHrefs(function(hrefData) {
		if ($.isEmptyObject(hrefData)) {
			$('#links-menu').parent().parent().remove();
			return;
		}

		for (var key in hrefData) {
			var element_data = hrefData[key];
			contents_string += '<li class="link-item">';

			contents_string += '<a class="link-head" href="' + $.trim(element_data['href']) + '">';
			if (element_data['img']) contents_string += '<div class="preview"><img src="' + element_data['img'] + '"/></div>';
			contents_string += '<div class="text">';
			contents_string += '<h3>' + element_data['title'] + '</h3>';
			contents_string += '<h4>' + element_data['description'] + '</h4>';
			contents_string += '</div>';
			contents_string += '</a>';

			contents_string += '</li>';
		}
		contents_string += '</ul>';

		contents.append(contents_string);

		contents.find('a').click(function(e) {
			e.preventDefault();
			backendBridge.openUrl($(this).attr('href'));
		});

		// Custom scrollbar

		contents.mCustomScrollbar({
			autoDraggerLength: true,
			scrollInertia: 800,
			mouseWheelPixels: 300,
			advanced: {
				updateOnBrowserResize: true,
				updateOnContentResize: true
			}
		});
	});

}

function themes_list_constructor() {
	var themes = $('#themes');
	themes.empty();
	var string = '<ul class="list">';

	for (var theme in themes_list) {
		string += '<li>';
		string += '<a id="' + theme + '" class="theme" href="#">';
		//string += '<img src="interface/themes/' + theme + '/preview.png"/>';
		string += '<span>' + themes_list[theme] + '</span>';
		string += '</a>';
		string += '</li>';
	}

	string += '</ul>';

	themes.html(string);

	themes.find('.theme').click(function() {
		var themeTag = $(this);
		if (!themeTag.hasClass('current')) {

			$('.theme').removeClass('current');
			current_theme = themeTag.attr('id');

			set_interface_theme(current_theme);

			var ebook_i = document.getElementById('ebook').getElementsByTagName('iframe')[0];
			var eapp_i = document.getElementById('eapp').getElementsByTagName('iframe')[0];
			var modal = document.getElementById('modal-iframe-container');

			if (ebook_i) set_content_theme(ebook_i, current_theme);
			if (eapp_i) set_content_theme(eapp_i, current_theme);
			if (modal) set_content_theme(modal, current_theme);

		}
	});

	// Custom scrollbar

	themes.find('ul').mCustomScrollbar({
		autoDraggerLength: true,
		scrollInertia: 800,
		mouseWheelPixels: 300,
		advanced: {
			updateOnBrowserResize: true,
			updateOnContentResize: true
		}
	});
}

function fonts_list_constructor() {
	var fonts = $('#fonts').empty();

	var string = '<ul class="list">';
	for (var font in fonts_list) {
		var current = '';
		if (font == current_content_font_family) current = ' current';
		string += '<li>';
		string += '<a id="' + font + '" class="font' + current + '" href="#">' + fonts_list[font] + '</a>';
		string += '</li>';
	}

	string += '</ul>';

	fonts.html(string);

	fonts.find('.font').click(function() {
		var fontTag = $(this);
		if (!fontTag.hasClass('current')) {
			$('.font').removeClass('current');
			fontTag.addClass('current');
			var new_font = fontTag.attr('id');
			current_content_font_family = new_font;
			var ebook_i = document.getElementById('ebook').getElementsByTagName('iframe')[0];
			var eapp_i = document.getElementById('eapp').getElementsByTagName('iframe')[0];
			var modal = document.getElementById('modal-iframe-container');
			if (ebook_i) set_content_font_family(ebook_i, new_font);
			if (eapp_i) set_content_font_family(eapp_i, new_font);
			if (modal) set_content_font_family(modal, new_font);
		}
	});

	// Custom scrollbar

	/*fonts.find('ul').mCustomScrollbar({
	 autoDraggerLength: true,
	 scrollInertia: 800,
	 mouseWheelPixels: 300,
	 advanced: {
	 updateOnBrowserResize: true,
	 updateOnContentResize: true
	 }
	 });*/

}

/* GENERAL NAVIGATION */

function open_navbar(navigationType, groupID, pageID) {
	close_modals();

	var navItem = app.getNavItemByID(pageID);
	var pageName = navItem ? navItem.resource : null;

	var navbar = $('#navbar');
	var toolbar = $('#toolbar');

	if (navigationType != app.currentSection) open_navigation();
	if (groupID && !pageName) open_group();
	if (pageName && pageID != app.currentPageID) open_item();

	function open_navigation() {
		navbar.find('.navbar-menu-item').removeClass('active');
		navbar.find('.contents').removeClass('active');
		navbar.find('.progress').removeClass('active');
		$('#' + navigationType + '-menu').addClass('active');
		$('#' + navigationType + '-contents').addClass('active');
		$('#' + navigationType + '-progress').addClass('active');
		app.currentSection = navigationType;
		$('#viewer').find('div').removeClass('active');
		$('#' + navigationType).addClass('active');
	}

	function open_group() {
		var group_element = $('#id_' + groupID);
		// danpan disabled разворачивание главы
		if (group_element.hasClass('disabled')) {
			return;
		}
		// конец disabled
		if (group_element.hasClass('opened')) {
			var items = group_element.find('.items');
			items.css('height', items.height()).css('overflow', 'hidden');
			items.animate({
				'height': 0
			}, 500, function() {
				group_element.removeClass('opened');
				items.css('height', 'auto').css('overflow', 'auto');
			});
		} else {
			var old_group_element = $('#' + navigationType + '-contents').find('.opened');
			var old_items = old_group_element.find('.items');
			old_items.css('height', old_items.height()).css('overflow', 'hidden');
			old_items.animate({
				'height': 0
			}, 400, function() {
				old_group_element.removeClass('opened');
				old_items.css('height', 'auto').css('overflow', 'auto');
			});
			var items = group_element.find('.items');
			var height = items.height();
			items.css('overflow', 'hidden').css('height', 0);
			group_element.addClass('opened');
			items.animate({
				'height': height
			}, 400, function() {
				items.css('height', 'auto').css('overflow', 'auto');
			});
		}
	}

	function open_item() {
		// Danpan disabled
		if ($('#id_' + pageID).find('a').hasClass('disabled')) {
			return;
		}
		// Danpan disabled
		navbar.removeClass('opened');
		toolbar.removeClass('opened');
		$('#' + navigationType + '-contents').find('.group').removeClass('current').removeClass('opened');
		$('#id_' + groupID).addClass('current').addClass('opened');
		$('#' + navigationType + '-contents').find('.item').removeClass('current');
		$('#' + navigationType + '-progress').find('.progress-item').removeClass('current');
		$('.contents').find('#id_' + pageID).addClass('current');
		$('.progress').find('#id_' + pageID).addClass('current');
		app.currentPageID = pageID;
		app.currentGroup = groupID;
		load_content($('#' + navigationType), pageName);
	}

	if (navigationType == 'ebook') {
		if (ebookProgressItem) ebookProgressItem.removeClass('current');
		ebookProgressItem = $('#ebook-progress').find('#' + $('#ebook-contents').find('.item.current').attr('id'));
		ebookProgressItem.addClass('current');
	} else if (navigationType == 'eapp') {
		if (eappProgressItem) eappProgressItem.removeClass('current');
		eappProgressItem = $('#eapp-progress').find('#' + $('#eapp-contents').find('.item.current').attr('id'));
		eappProgressItem.addClass('current');
	}
}

function close_help() {
	$('body').find('#help-window').remove();
}

function open_toolbar(tool, group, item) {
	close_modals();

	group = group || false;
	item = item || false;

	var navbar = $('#navbar');
	var toolbar = $('#toolbar');

	if (tool) open_tool();
	if (group && !item) open_group();
	if (item) open_item();

	function open_tool() {
		switch (true) {
			case tool == 'settings' || tool == 'special':
				var wrapper = $('#wrapper');
				$('#izdNom').html(window.frames[0].window.location.href.split('content/')[1].split('/')[0]);
				navbar.removeClass('opened');
				navbar.removeClass('hidden');
				toolbar.removeClass('opened');
				$('#' + tool + '-window').addClass('active');
				break;
			case tool == 'help':
				var wrapper = $('#wrapper');
				navbar.removeClass('opened');
				navbar.removeClass('hidden');
				toolbar.removeClass('opened');
				var help_win = $('<div id="help-window"/>').appendTo($('body'));
				help_win.append('<div class="help-overlay"><div class="help-cell"><iframe src="interface/themes/' + current_theme + '/help/guide.html" /></div></div>');
				break;
			default:
				//switch_tool();
				switch (true) {

					// ПОЛНОЭКРАННЫЙ РЕЖИМ - LARGE

					case (toolbar.hasClass('L')):
						if (toolbar.hasClass('collapsed')) {
							if (toolbar.hasClass('opened')) switch_tool();
							else {
								navbar.removeClass('opened');
								toolbar.addClass('opened');
								switch_tool();
							}
						} else switch_tool();
						break;

					// ПОЛНОЭКРАННЫЙ РЕЖИМ - MEDIUM

					case (toolbar.hasClass('M')):
						if (toolbar.hasClass('opened')) switch_tool();
						else {
							if (current_tool != '') {
								navbar.removeClass('opened');
								toolbar.addClass('opened');
							}
							switch_tool();
						}
						break;

					// МАЛОЭКРАННЫЙ РЕЖИМ - SMALL

					case (toolbar.hasClass('S')):
						if (toolbar.hasClass('opened')) {
							navbar.addClass('hidden');
							switch_tool();
						} else {
							if (current_tool != '') {
								navbar.removeClass('opened');
								toolbar.addClass('opened');
							}
							switch_tool();
						}
						break;

					// МАЛОЭКРАННЫЙ РЕЖИМ - X-SMALL

					case (toolbar.hasClass('xS')):
						if (toolbar.hasClass('opened')) {
							navbar.addClass('hidden');
							switch_tool();
						} else {
							if (current_tool != '') {
								navbar.removeClass('opened');
								toolbar.addClass('opened');
							}
							switch_tool();
						}
						break;

				}
				if (tool != current_tool) switch_tool();
				break;
		}

	}

	/* add */

	function switch_tool() {
		toolbar.find('.toolbar-menu-item').removeClass('active');
		toolbar.find('.tab').removeClass('active');
		$('#' + tool + '-menu').addClass('active');
		$('#' + tool + '-tab').addClass('active');
		current_tool = tool;
	}

	function open_group() {
		var group_element = $('#' + tool + '-group_' + group);
		if (group_element.hasClass('opened')) group_element.removeClass('opened');
		else {
			$('#' + tool + '-tab').find('.group').removeClass('opened');
			group_element.addClass('opened');
		}
	}

	function open_item() {
		navbar.removeClass('opened');
		toolbar.removeClass('opened');
		alert('open toolbar item');
	}
}

function open_modal(path, contentType) {
	close_modals();

	var modals = $('#modals');
	modals.addClass('active');

	$('#navbar').removeClass('opened');
	$('#toolbar').removeClass('opened');

	var item_header = '<div class="header-IN"><div class="close-modal button"></div><div class="title">' + /*target_data[ 'head' ] +*/ '</div><div class="toolspanel-wrapper"></div></div>';

	var win = $('<div class="modal"/>').appendTo(modals);

	modals.hide();
	win.css({
		'opacity': 0,
		'transform': 'scale(0.1,0.1)'
	});
	modals.fadeIn(600);
	win.animate({
		'transform': 'scale(1,1)',
		'opacity': 1
	}, 600);

	var string = '<div class="modal-IN"><div class="header">' + item_header + '</div><div class="content"><div id="modal-iframe-container"></div></div></div>';

	win.html(string);

	app.currentPopupModuleID = app.getPopupModuleID(path);

	load_content($('#modal-iframe-container'), path, contentType);

	win.find('.close-modal').click(function() {
		$('#container').removeClass('fullscreen');
		$('#navbar').removeClass('hidden');
		$('#toolbar').removeClass('hidden');
		win.remove();
		if (modals.is(':empty')) modals.removeClass('active');
	});
}

function close_modals() {
	$('#container').removeClass('fullscreen');
	$('#navbar').removeClass('hidden');
	$('#toolbar').removeClass('hidden');
	var modals = $('#modals');
	modals.html('');
	modals.removeClass('active');
	app.currentPopupModuleID = undefined;
}

/* CONTENT_LOADER */

function load_content(target, id, contentType) {
	var dir = path;
	if (contentType) dir = resourceTypeData[contentType]['dir'];

	var viewer = document.getElementById('viewer');

	var preloader_old = document.getElementById('preloader');
	if ((typeof preloader_old) != 'undefined' && preloader_old != null) {
		viewer.removeChild(preloader_old);
	}

	var preloader = document.createElement('div');
	preloader.setAttribute('id', 'preloader');
	preloader.innerHTML = '<div id="floatingCirclesContainer"><div id="floatingCirclesG"><div class="f_circleG" id="frotateG_01"></div><div class="f_circleG" id="frotateG_02"></div><div class="f_circleG" id="frotateG_03"></div><div class="f_circleG" id="frotateG_04"></div><div class="f_circleG" id="frotateG_05"></div><div class="f_circleG" id="frotateG_06"></div><div class="f_circleG" id="frotateG_07"></div><div class="f_circleG" id="frotateG_08"></div></div></div>';
	viewer.appendChild(preloader);
	$(preloader).hide().fadeIn(200);

	target.animate({
		'opacity': 0
	}, 200, function() {

		var iframe = document.createElement('iframe');
		iframe.setAttribute('src', dir + '/' + id + '.html');
		iframe.setAttribute('allowFullScreen', 'true');
		iframe.setAttribute('style', 'position: absolute; left: 25%; top: 25%; width: 50%; height: 50%;');
		target.empty().append(iframe);

		iframe.onload = function() {
			var iframeDoc = $(iframe.contentDocument);
			var iframeWin = iframe.contentWindow;
			
            try{iframeWin.initCollapsibles()}
            catch(e){};
			
			if (app.currentSection == "ebook") {
				backendBridge.getSelection(app.currentPageID, function(pageSels) {
					if (pageSels) {
						iframeDoc.find("#content").html(pageSels);
						iframeDoc.find("#contextmenu").remove();
					}
				});
			}

			var navItem = app.getNavItemByID(app.currentPageID);

			var prevNavItem = app.getAdjacentItem(navItem, 'prev');
			var prevItemParent = prevNavItem ? app.getNavItemByID(prevNavItem.parent) : null;

			var nextNavItem = app.getAdjacentItem(navItem, 'next');
			var nextItemParent = nextNavItem ? app.getNavItemByID(nextNavItem.parent) : null;

			var wrapper = iframeDoc.find('#wrapper');

			if (prevNavItem) {
				var previous_link_row = document.createElement('div');
				previous_link_row.setAttribute('id', 'previous-link-row');
				var previous_link = document.createElement('a');
				previous_link.setAttribute('id', 'previous-link');
				previous_link.setAttribute('href', '#');
				previous_link.innerHTML = '<div class="chapter-text">' + app.getIfExists(prevItemParent.textName, '<span class="title">' + prevItemParent.textName + '</span>') + prevItemParent.textTitle + '</div><div class="paragraph-text">' + app.getIfExists(prevNavItem.textName, '<span class="title">' + prevNavItem.textName + '</span>') + prevNavItem.textTitle + '</div>';

				previous_link_row.appendChild(previous_link);
				wrapper.prepend(previous_link_row);

				previous_link.onclick = function() {
					open_navbar(prevNavItem.section, prevNavItem.parent, prevNavItem.id);
					backendBridge.setLastViewedPage(prevNavItem.section, prevNavItem.parent, prevNavItem.id);
				};

				var previousBtn = $('#previous');
				previousBtn.off('click');
				previousBtn.click(previous_link.onclick);
			}

			if (nextNavItem) {
				var next_link_row = document.createElement('div');
				next_link_row.setAttribute('id', 'next-link-row');
				var next_link = document.createElement('a');
				next_link.setAttribute('id', 'next-link');
				next_link.setAttribute('href', '#');
				next_link.innerHTML = '<div class="chapter-text">' + app.getIfExists(nextItemParent.textName, '<span class="title">' + nextItemParent.textName + '</span>') + nextItemParent.textTitle + '</div><div class="paragraph-text">' + app.getIfExists(nextNavItem.textName, '<span class="title">' + nextNavItem.textName + '</span>') + nextNavItem.textTitle + '</div>';

				next_link_row.appendChild(next_link);
				wrapper.append(next_link_row);

				next_link.onclick = function() {
					open_navbar(nextNavItem.section, nextNavItem.parent, nextNavItem.id);
					backendBridge.setLastViewedPage(nextNavItem.section, nextNavItem.parent, nextNavItem.id);
				};

				var nextBtn = $('#next');
				nextBtn.off('click');
				nextBtn.click(next_link.onclick);
			}

			set_content_theme(iframe, current_theme);

			setTimeout(function(){initHyperlinks(iframe)}, 500);
            
			if (app.currentSection == 'ebook') {
				var content = iframe.contentDocument.getElementById('content');
                console.log("content = ", content);
                if (content == null){
                    current_content_font_size = 18;
                    current_content_font_family = "Verdana";
                    content = iframe.contentDocument.getElementById('container');  
                }
				else{
					current_content_font_size = 17;
                    current_content_font_family = "PTSerif";
					set_content_font_size(iframe, current_content_font_size);
				}
                if (content == null) content = iframe.contentDocument.getElementById('app');
				
                toolPanel = new ToolPanel(target, iframe.contentDocument, content);
				surfingHistory.setToolPanel(toolPanel);
			}
            
            set_content_font_family(iframe, current_content_font_family);

			iframe.removeAttribute('style');

			$(preloader).fadeOut(200, function() {
				$(this).remove();
				target.animate({
					'opacity': 1
				});
			});
		};
	});

	/* LOADER FUNCTIONS */

	function initHyperlinks(target_iframe) {
		var links_external = target_iframe.contentWindow.document.getElementsByClassName('link-external');
		var links_vocabular = target_iframe.contentWindow.document.getElementsByClassName('link-vocabular');
		var links_redirect = target_iframe.contentWindow.document.getElementsByClassName('link-redirect');
		var links_modal = target_iframe.contentWindow.document.getElementsByClassName('link-modal');

		var i;
		var link;

		// External Links
		for (i = 0; i < links_external.length; i++) {
			link = $(links_external[i]);
			link.addClass('enabled');
			link.click(function() {
				backendBridge.openUrl($(this).attr('href'));
			});
		}

		// Vocabular Links
		for (i = 0; i < links_vocabular.length; i++) {
			link = $(links_vocabular[i]);
			link.addClass('enabled');
			link.click(function() {
				alert('Show in "Vocabular"');
			});
		}

		// Redirect Links
		for (i = 0; i < links_redirect.length; i++) {
			link = $(links_redirect[i]);
			link.addClass('enabled');
			link.click(function() {
				var target_resource = this.getAttribute('target_id');
				var navItem = app.getNavItemByName(target_resource);

				surfingHistory.addPage(app.currentSection, app.currentGroup, app.currentPageID);
				open_navbar(navItem.section, navItem.parent, navItem.id);
			});
		}

		// Modal Links
		for (i = 0; i < links_modal.length; i++) {
			link = $(links_modal[i]);
			link.addClass('enabled');
			link.click(function() {
				var target_id = this.getAttribute('target_id');
				open_modal(target_id);
			});
		}
	}
}

function ToolPanel(target_div, iframe, content) {
	var lastToolPos;
	var $iframe = $(iframe);
	var wrapper = $iframe.find('#wrapper');
	var $content = $(content);

	this.updatePosition = function() {
		/*
		toolspanel_wrapper.css('left', $content.offset().left + $content.outerWidth() - toolspanel_wrapper.outerWidth());

		var offset = $content.position().top - $iframe.scrollTop();
		var newToolPos = (offset > 0) ? offset : 0;
		if (newToolPos != lastToolPos)
			toolspanel_wrapper.css('top', newToolPos);

		lastToolPos = newToolPos;
		*/
	};

	this.reset = function() {
		note_button.removeClass('noted').removeClass('pressed');
		toolspanel_wrapper.find('.note-box').remove();

		var bmark = toolspanel_wrapper.find('.bookmark');
		if (bmark.hasClass("bookmarked")) {
			bmark.removeClass("bookmarked");
		}
	};

	var toolspanel_wrapper = $('<div class="toolspanel-wrapper"/>').appendTo(target_div).css('display', 'none');
	var that = this;
	setTimeout(function() {
		toolspanel_wrapper.fadeIn();
		that.updatePosition();
	}, 300);

	$iframe.scroll(this.updatePosition);

	var toolspanel = $('<div class="toolspanel"/>').appendTo(toolspanel_wrapper);

	//back

	if (!surfingHistory.isEmpty()) {
		var prev = $('<div class="toolspanel-item"/>').appendTo(toolspanel);
		var prevBtn = $('<div class="back toolspanel-button"/>').appendTo(prev);
		prevBtn.click(function() {
			surfingHistory.stepPrev();
		});

		prevBtn.mousedown(function() {
			var timerID = setTimeout(function() {
				if (prevBtn.hasClass('pressed')) {
					prevBtn.removeClass('pressed');
					toolspanel_wrapper.find('.back-box').remove();
				} else {
					prevBtn.addClass('pressed');

					var historyList = '';
					historyList += '<div class="back-box">';
					historyList += '<ul>';

					var historyStack = surfingHistory.getStore();
					for (var i = 0; i < historyStack.length; i++) {
						var itemData = app.getNavItemByID(historyStack[i].item);
						historyList += '<li><a nav_id=' + historyStack[i].nav + ' group_id=' + historyStack[i].group + ' item_id=' + historyStack[i].item + '>' + itemData.textName + ' ' + itemData.textTitle + '</a></li>';
					}

					historyList += '</ul>';
					historyList += '</div>';
					toolspanel_wrapper.append(historyList);

					toolspanel_wrapper.find('.back-box').find('li').click(function() {
						var item = $(this).find('a');
						open_navbar(item.attr('nav_id'), item.attr('group_id'), item.attr('item_id'));
						for (var i = 0; i < historyStack.length; i++) {
							if (historyStack[i].item == item.attr('item_id')) {
								historyStack.splice(i);
							}
						}
					});
				}
			}, 300);

			$(window).mouseup(function() {
				$(window).off('mouseup');
				clearTimeout(timerID);
			});
		});
	}

	// Note

	var toolspanel_note = $('<div class="toolspanel-item"/>').appendTo(toolspanel);
	var note_button = $('<div class="note toolspanel-button"/>').appendTo(toolspanel_note);
	app.getNote(function(note) {
		if (note.length) {
			note_button.addClass("noted");
		}
	});

	note_button.click(function() {
		if ($(this).hasClass('pressed')) {
			note_button.removeClass('pressed');
			toolspanel_wrapper.find('.note-box').remove();
		} else {
			note_button.addClass('pressed');

			app.getNote(function(note) {
				var string = '';
				string += '<div class="note-box">';
				string += '<textarea>' + note + '</textarea>';
				string += '<div class="note-buttons"><input type="button" id="note-Save" value="Сохранить" /><input type="button" id="note-Delete" value="Очистить" /></div>';
				string += '</div>';
				string += '</div>';
				toolspanel_wrapper.append(string);

				$(toolspanel_wrapper).find('#note-Delete').click(function() {
					$(".note-box > textarea").val("");
					toolspanel_wrapper.find('.note-box').remove();
					note_button.removeClass('pressed');
					note_button.removeClass('noted');
					app.delNote(work_list_constructor);
				});

				$(toolspanel_wrapper).find('#note-Save').click(function() {
					app.delNote(function() {
						app.addNote(function(){
							toolspanel_wrapper.find('.note-box').remove();
							note_button.removeClass('pressed');
							work_list_constructor();
						});
					});
				});
			});
		}
	});

	// Bookmark

	var toolspanel_bookmark = $('<div class="toolspanel-item"/>').appendTo(toolspanel);
	var bookmark_button = $('<div class="bookmark toolspanel-button"/>').appendTo(toolspanel_bookmark);
	bookmark_button.click(function() {
		app.toggleBookmark();
	});

	backendBridge.inBookmarks(app.currentPageID, function(contains) {
		if (contains) {
			var toolspanelButtonBookmark = $(".toolspanel-button.bookmark");
			if (!(toolspanelButtonBookmark.hasClass("bookmarked"))) {
				toolspanelButtonBookmark.addClass("bookmarked");
			}
		}
	});

	// Print

	var toolspanel_print = $('<div class="toolspanel-item"/>').appendTo(toolspanel);
	var print_button = $('<div class="print toolspanel-button"/>').appendTo(toolspanel_print);
	print_button.click(function() {
		app.printContentPage();
	});
}

/* STYLING */

function set_interface_theme(theme) {
	app.saveUISettings(current_theme, current_interface_font_size, current_content_font_size, current_content_font_family);

	$('#theme').remove();

	var head = $('head').eq(0);
	var link = $('<link/>');
	link.attr({
		'id': 'theme',
		'rel': 'stylesheet',
		'type': 'text/css',
		'href': 'interface/themes/' + theme + '/css/main.css'
	});
	head.append(link);
}

function set_content_theme(target_iframe, theme) {
	if (!theme) return;

	app.saveUISettings(current_theme, current_interface_font_size, current_content_font_size, current_content_font_family);

	if (app.currentSection == 'ebook') {
		$(target_iframe.contentWindow.document).find('#theme').remove();
	}

	var head = target_iframe.contentWindow.document.getElementsByTagName('head')[0];

	var link = target_iframe.contentWindow.document.createElement('link');
	link.setAttribute('id', 'theme');
	link.setAttribute('rel', 'stylesheet');
	link.setAttribute('type', 'text/css');
	link.setAttribute('href', '../../interface/themes/' + theme + '/css/content.css');

	var rangyTags = [];
	var rangyData = [
		"rangy-core.js",
		"rangy-serializer.js",
		"rangy-cssclassapplier.js",
		"rangy-textrange.js",
		"rangy-highlighter.js",
		"rangy-selectionsaverestore.js"
	];
	for (var i in rangyData) {
		var script = target_iframe.contentWindow.document.createElement('script');
		script.src = '../../js_app/lib/rangy/' + rangyData[i];
		rangyTags.push(script);
	}


	var selScript = target_iframe.contentWindow.document.createElement('script');
	selScript.src = "../../js_app/selection.js";
	if (app.currentSection == 'ebook') {
		head.appendChild(link);
		for (i in rangyTags) {
			head.appendChild(rangyTags[i]);
		}
		head.appendChild(selScript);
	}
}

function set_interface_font_size(size) {
	app.saveUISettings(current_theme, current_interface_font_size, current_content_font_size, current_content_font_family);

	var body = document.getElementsByTagName('body')[0];
	body.style.fontSize = size + 'px';
	responsive();
}

function set_content_font_size(target_iframe, size) {
	app.saveUISettings(current_theme, current_interface_font_size, current_content_font_size, current_content_font_family);

	var body = target_iframe.contentWindow.document.getElementsByTagName('body')[0];

	body.style.fontSize = size + 'px';
}

function set_content_font_family(target_iframe, font) {
	app.saveUISettings(current_theme, current_interface_font_size, current_content_font_size, current_content_font_family);

	var old_link = target_iframe.contentWindow.document.getElementById('font');

	if (old_link) {
		old_link.outerHTML = '';
	}

	var head = target_iframe.contentWindow.document.getElementsByTagName('head')[0];
	var link = target_iframe.contentWindow.document.createElement('link');
	head.appendChild(link);

	link.setAttribute('id', 'font');
	link.setAttribute('rel', 'stylesheet');
	link.setAttribute('type', 'text/css');
	link.setAttribute('href', '../../interface/fonts/' + font + '/stylesheet.css');

	var body = target_iframe.contentWindow.document.getElementsByTagName('body')[0];
	body.style.fontFamily = fonts_list[font];
}

/* ADAPTIVE FUNCTIONS */

$(window).resize(responsive);

function responsive() { // Расставляет классы S (small), M (medium), L (large) элементам #navbar и #toolbar
	var win_width = $(window).width();
	var point_1 = Math.floor(current_interface_font_size * 60);
	var point_2 = Math.floor(current_interface_font_size * 90);
	var point_3 = Math.floor(current_interface_font_size * 125);
	var navbar = $('#navbar');
	var toolbar = $('#toolbar');
	var container = $('#container');

	switch (true) {
		case win_width < point_1:
			navbar.removeClass('S M L');
			navbar.addClass('xS');
			toolbar.removeClass('S M L');
			toolbar.addClass('xS');
			container.removeClass('S M L');
			container.addClass('xS');
			break;
		case point_1 < win_width && win_width < point_2:
			navbar.removeClass('xS M L');
			navbar.addClass('S');
			toolbar.removeClass('xS M L');
			toolbar.addClass('S');
			container.removeClass('xS M L');
			container.addClass('S');
			break;
		case point_2 < win_width && win_width < point_3:
			navbar.removeClass('xS S L');
			navbar.addClass('M');
			toolbar.removeClass('xS S L');
			toolbar.addClass('M');
			container.removeClass('xS S L');
			container.addClass('M');
			break;
		case win_width > point_3:
			navbar.removeClass('xS S M');
			navbar.addClass('L');
			toolbar.removeClass('xS S M');
			toolbar.addClass('L');
			container.removeClass('xS S M');
			container.addClass('L');
			break;
	}

	progressBarHack();

}

/* UTILITIES */

function History() {
	var store = [];
	var toolpanel;

	this.addPage = function(nav, group, item) { // Запись навигационной цепочки в массив history
		store.push({
			'nav': nav,
			'group': group,
			'item': item
		});
	};

	this.clear = function() {
		store.length = 0;
	};

	this.stepPrev = function() {
		var item = store.pop();
		open_navbar(item.nav, item.group, item.item);
	};

	this.setToolPanel = function(tpanel) {
		toolpanel = tpanel;
	};

	this.isEmpty = function() {
		return !store.length;
	};

	this.getStore = function() {
		return store;
	};
}

/* THE END */