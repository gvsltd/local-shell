$.fn.hyphenate = function() {
	var All = "[абвгдеёжзийклмнопрстуфхцчшщъыьэюя]";
	var Glas = "[аеёиоуыэю\я]";
	var Sogl = "[бвгджзклмнпрстфхцчшщ]";
	//var RusX = "[йъь]";
	var Hyphen = "\xAD";

	//Частные случаи
	var re12 = new RegExp("("+Glas+"рст"+")("+"венн"+")","ig"); 		/*	государст-венной	*/
	var re13 = new RegExp("("+Glas+")("+"креп"+")","ig");						/*	за-креплено	*/
	var re14 = new RegExp("("+Sogl+")("+"креп"+")","ig");						/*	от-креплено	*/
	var re15 = new RegExp("("+Glas+")("+"тельств"+")","ig"); 				/*	обяза-тельств	*/
	var re17 = new RegExp("("+Glas+")("+"произ"+")","ig"); 					/*	дело-производства	*/
	var re18 = new RegExp("("+"домст"+")("+"вом"+")","ig"); 				/*	ведомст-вом	*/
	var re19 = new RegExp("("+Glas+Glas+")("+"грамма"+")","ig"); 		/*	видео-грамма	*/
	var re20 = new RegExp("("+Glas+Glas+")("+"времен"+")","ig"); 		/*	свое-временным	*/

	//Общий набор правил
	var re2 = new RegExp("("+Glas+")("+Glas+All+")","ig");
	var re3 = new RegExp("("+Glas+Sogl+")("+Sogl+Glas+")","ig");
	var re4 = new RegExp("("+Sogl+Glas+")("+Sogl+Glas+")","ig");
	var re5 = new RegExp("("+Glas+Sogl+")("+Sogl+Sogl+Glas+")","ig");
	var re6 = new RegExp("("+Glas+Sogl+Sogl+")("+Sogl+Sogl+Glas+")","ig");

	//Вероятно удалить
	//var re1 = new RegExp("("+RusX+")("+All+All+")","ig");					/* Общий набор правил */

	console.log(this);
	this.each(function(){
		var text=$(this).html();

		//Сначала частные случаи
		text = text.replace(re12, "$1"+Hyphen+"$2");
		text = text.replace(re13, "$1"+Hyphen+"$2");
		text = text.replace(re14, "$1"+Hyphen+"$2");
		text = text.replace(re15, "$1"+Hyphen+"$2");
		text = text.replace(re17, "$1"+Hyphen+"$2");
		text = text.replace(re18, "$1"+Hyphen+"$2");
		text = text.replace(re19, "$1"+Hyphen+"$2");
		text = text.replace(re20, "$1"+Hyphen+"$2");

		//Затем общие
		text = text.replace(re2, "$1"+Hyphen+"$2");
		text = text.replace(re3, "$1"+Hyphen+"$2");
		text = text.replace(re4, "$1"+Hyphen+"$2");
		text = text.replace(re5, "$1"+Hyphen+"$2");
		text = text.replace(re6, "$1"+Hyphen+"$2");

		$(this).html(text);
	});
};

$(function(){
	$('p').hyphenate(); $('li').hyphenate(); $('td').hyphenate(); $('th').hyphenate();
});