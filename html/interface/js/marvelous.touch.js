/*
autor: Leo
*/
function initTouchEvents()
{
	
	//$('body').css('-ms-touch-action', 'none');
	//$('body').css('touch-action', 'none');
	
	document.addEventListener("touchstart", touchHandler, true);
	document.addEventListener("touchmove", touchHandler, true);
	document.addEventListener("touchend", touchHandler, true);
	document.addEventListener("touchcancel", touchHandler, true);
	window.addEventListener("MSPointerDown", touchHandler, true);
	window.addEventListener("MSPointerMove", touchHandler, true);
	window.addEventListener("MSPointerUp", touchHandler, true);
	window.addEventListener("MSPointerCancel", touchHandler, true);
}

function uninitTouchEvents()
{
	document.removeEventListener("touchstart", touchHandler, true);
	document.removeEventListener("touchmove", touchHandler, true);
	document.removeEventListener("touchend", touchHandler, true);
	document.removeEventListener("touchcancel", touchHandler, true);
	window.removeEventListener("MSPointerDown", touchHandler, true);
	window.removeEventListener("MSPointerMove", touchHandler, true);
	window.removeEventListener("MSPointerUp", touchHandler, true);
	window.removeEventListener("MSPointerCancel", touchHandler, true);
}

function touchHandler(event)
{	
	event.preventDefault();

	var touches = event.changedTouches;
	//var touches = event.touches;
	var first;// = touches[0];
	var type = "";

		switch(event.type)
	{
		case "touchstart": type = "mousedown"; first = touches[0]; break;
		case "touchmove":  type="mousemove"; first = touches[0]; break;        
		case "touchend":   type="mouseup"; first = touches[0]; break;
		case "MSPointerDown": type = "mousedown"; first = event; break;
		case "MSPointerMove":  type="mousemove"; first = event; break;        
		case "MSPointerUp":   type="mouseup"; first = event; break;
		default: return;
	}

	// initMouseEvent(type, canBubble, cancelable, view, clickCount,
	//                screenX, screenY, clientX, clientY, ctrlKey,
	//                altKey, shiftKey, metaKey, button, relatedTarget);

	var simulatedEvent = document.createEvent("MouseEvent");
	simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
	
	first.target.dispatchEvent(simulatedEvent);
	
	/*if((event.type == "touchend") || (event.type == "MSPointerUp")){
		var simulatedEvent2 = document.createEvent("MouseEvent");
		simulatedEvent2.initMouseEvent('click', true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left* /, null);
		
		first.target.dispatchEvent(simulatedEvent);
	}*/

}