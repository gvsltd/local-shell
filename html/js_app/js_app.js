function addClass(o, c) {
	var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g");
	if (re.test(o.className)) return;
	o.className = (o.className + " " + c).replace(/\s+/g, " ").replace(/(^ | $)/g, "")
}

function removeClass(o, c) {
	var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g");
	o.className = o.className.replace(re, "$1").replace(/\s+/g, " ").replace(/(^ | $)/g, "")
}
Object.defineProperty(Array.prototype, "sortOn", {
	enumerable: false,
	writable: true
});

Array.prototype.sortOn = function(){
	var dup = this.slice();
	if(!arguments.length) return dup.sort();
	var args = Array.prototype.slice.call(arguments);
	return dup.sort(function(a,b){
		var props = args.slice();
		var prop = props.shift();
		while(a[prop] == b[prop] && props.length) prop = props.shift();
		return a[prop] == b[prop] ? 0 : a[prop] > b[prop] ? 1 : -1;
	});
};

app = new ShellApp();

function ShellApp() {
	this.name = "Javascript Shell Application";
	this.doc = "Это приложение для связи Python-объекта с html-шаблоном";
	this.currentPageID = 0;
	this.currentSection = '';
	this.currentGroup = '';


	this.openPage = function (pageName) {
		window.location = pageName + ".html";
	}; // end method

	this.login = function () {
		var login = $('#login').val();
		var password = $('#password').val();
		backendBridge.login(login, password, function(success){
			if (success) {
				backendBridge.openWorkPage();
			} else {
				$(".messages.form-row").show();
			}
		});
	};

	this.regMe = function () {
		backendBridge.regMe($("#login").val(), $("#password").val(), $("#fio").val(), function(result){
			if (result == "exist") {
				alert("Логин занят");
			} else {
				window.location = "auth.html";
			}
		});
	};

	this.buildProjectList = function (configs) {
		var list = $('#projectList').empty();
		var configList = [];
		for (var dir in configs) {
			configList.push(configs[dir]);
		}
		configList = configList.sortOn('title');
		
		for (var i in configList) {
			var item = configList[i];
			list.append('<li><a href="#"><img src="' + 'content/' + item.dir + '/' + item.preview + '"><span class="title" dir="' + item.dir + '">' + item.title + '</span></a></li>');
		}
		list.children('li').click(function () {
			var projectCat = $(this).find('.title').attr('dir');
			backendBridge.openWorkPage(projectCat);
		});
	};

	this.setPage = function (id) {
		app.currentPage = id;
	};

	this.setNavigation = function () {
		backendBridge.getNavigation(function(nav){
			app.navigation = nav;
			app.navigation.ebook = app.sortNav(nav.ebook);
			app.navigation.eapp = app.sortNav(nav.eapp);
			app.buildNavbar();

			if(!app.navigation.ebook.length || !app.navigation.eapp.length) $('#navbar-menu').remove();
			
		});
	};
	
	this.buildNavbar = function () {
		var ebook = app.navigation.ebook;
		var eapp = app.navigation.eapp;
		
		var ebookUl = app.createBranch(ebook);
		var eappUl = app.createBranch(eapp);
		
		var contents = $('#ebook-contents');
		var appContents = $('#eapp-contents');
		contents.html(ebookUl);
		appContents.html(eappUl);

		/* PROGRESS */

		var ebook_progress = $( '<div class="progress-list"/>' );
		var eapp_progress = $( '<div class="progress-list"/>' );
		
		ebookUl.find( '.item' ).each( function() {
			var item = $( this ).clone( true );
			ebook_progress.append( $( item ).removeClass( 'item' ).addClass( 'progress-item' ).empty() );
		});
		var bWrapper = $( '<div class="progress-wrapper"/>' ).appendTo( $( '#ebook-progress' ) );
		bWrapper.html( ebook_progress );
		
		eappUl.find( '.item' ).each( function() {
			var item = $( this ).clone( true );
			eapp_progress.append( $( item ).removeClass( 'item' ).addClass( 'progress-item' ).empty() );
		});
		var aWrapper = $( '<div class="progress-wrapper"/>' ).appendTo( $( '#eapp-progress' ) );
		aWrapper.html( eapp_progress );

		/* BUTTONS */

		contents.add(appContents).find('a.group-head').click(function () {
			var item = $(this);
			open_navbar(item.attr('target_nav'), item.attr('target_group'));
		});

		contents.add(appContents).find('.i-tutor').click(function (e) {
			backendBridge.openTutorial($(this).prev().children('h4').text());
			e.stopImmediatePropagation();
		});
		
		contents.add(appContents).find('a.item-head').click(function () {
			var item = $(this);
			var parent = item.parent();
			if (parent.hasClass('item')) {
				open_navbar(item.attr('target_nav'), item.attr('target_group'), item.attr('target_id'));
				backendBridge.setLastViewedPage(item.attr('target_nav'), item.attr('target_group'), item.attr('target_id'));
			} else if (parent.hasClass('link')){
				open_modal(item.attr('target_id'));
			} else alert('Error: Undefined element type: "item" or "link"!');
			surfingHistory.clear();
		});

		var navbar = $('#navbar');
		navbar.find('.navbar-menu-item').click(function () {
			open_navbar($(this).attr('target_nav'));
		});
		
		//этот левое меню
		// contents.add(appContents).mCustomScrollbar({
		// 	autoDraggerLength: true,
		// 	scrollInertia: 800,
		// 	mouseWheelPixels: 300,
		// 	advanced: {
		// 		updateOnBrowserResize: true,
		// 		updateOnContentResize: true
		// 	}
		// });
	};
	
	this.createBranch = function (data) {
		var root = $("<div>");
		var ul = $("<div>").addClass("list id0");
		root.append(ul);

		for (var i in data) {
			var item = data[i];
			var itemParent = root.find('.id' + item.parent).eq(0);

			switch (item.type){
				case 'group':
					var parentNode = itemParent.children('.items').eq(0);
					if (parentNode.length == 0) parentNode = itemParent;

					parentNode.append(app.formatString('\
						<div class="group id%id ' + app.getIfExists(item.class).replace('i-tutor', '') + '" id="id_%id">\
							<a href="#" class="group-head" target_nav="%section" target_group="%id">'
								+
								app.getIfExists(item.preview,
								'<div class="preview">\
									<img src="%preview">\
								</div>')
								+
								'<div class="text">' +
									app.getIfExists(item.textName,
									'<h3>%textName</h3>') +
									'<h4>%textTitle</h4>\
								</div>'
								+
								((item.class.search('i-tutor') != -1) ?
								'<div class="i-tutor">\
								</div>'
								: '')
								+
								'<div class="arrow">\
									<span/>\
								</div>\
							</a>\
							\
							<div class="items"/>\
						</div>\
					', item));
					break;

				case 'item':
					var parentNode = itemParent.children('.items').eq(0);

					parentNode.append(app.formatString('\
						<div class="item id%id" id="id_%id">\
							<a href="#" target_id="%id" target_nav="%section" target_group="%parent" class="item-head ' + app.getIfExists(item.class) + '">' +
								app.getIfExists(item.textName,
								'%textName. ') +
								'%textTitle\
							</a>\
						</div>\
					', item));
					break;

				case 'separator':
					itemParent.append(app.formatString('\
						<div class="separator id%id">\
							<div class="text">\
								<h3>%textName</h3><h4>%textTitle</h4>\
							</div>\
						</div>\
					', item));
					break;

				case 'link':
					var parentNode = itemParent.find('.links').eq(0);
					if(parentNode.length == 0){
						parentNode = $('<ul>').addClass('links');
						itemParent.append(parentNode);
					}

					parentNode.append(app.formatString('\
						<div class="link id%id">\
							<a class="item-head" target_id="%resource">\
								%textTitle\
							</a>\
						</div>\
					', item));
					break;
			}
		}
		return ul;
	};
	
	this.getIfExists = function(record, returnValue){
		if(record && record.length && (record != "None")){
			return (returnValue) ? returnValue : record;
		} else {
			return '';
		}
	};
	
	this.formatString = function(string, parameters){
		for(var key in parameters){
			string = string.replace(new RegExp('%' + key, 'g'), parameters[key]);
		}
		return string;
	};

	/**сортирует данные так, чтобы итемы шли подряд*/
	this.sortNav = function (navData) {
		var items = [];
		var groups = [];
		var lastItem;
		
		for (var i = 0; i < navData.length; i++) {
			var item = navData[i];
			if (item.type == 'item' || item.type == 'link') {
				//связываем страницы с соседними
				if(lastItem) {
					lastItem.next = item;
					item.prev = lastItem;
				}
				
				items.push(item);				
				lastItem = item;
			} else if (item.type == 'group' || item.type == 'separator') {
				groups.push(item);
			}
		}
		return groups.concat(items);
	};

	this.showLastViewedPage = function () {
		var pageData = this.getFirstPage();
		
		backendBridge.getLastViewedPage(function(data){
			if (data) {
				pageData = data;
			}
			open_navbar(pageData.section, pageData.parent, pageData.id);
		});
	};

	this.getFirstPage = function () {
		var navData = app.navigation.ebook;
		for (var i = 0; i < navData.length; i++) {
			if (navData[i].type == 'item') {
				return navData[i];
			}
		}
	};

	this.addNote = function (callback) {
		var text = $(".note-box > textarea").val();
		$(".toolspanel-button.note").addClass("noted");
		backendBridge.addNote(app.currentPageID, text, callback);
	};

	this.delNote = function (callback) {
		$(".toolspanel-button.note").removeClass("noted");
		backendBridge.delNote(app.currentPageID, callback);
	};

// #######################################################
	this.getJournal = function () {
		backendBridge.getJournal(function(journal){
			app.journal = journal;
			app.buildJournal();
		});
	};
	
	function prepareJournalData(){
		var data = {
			lang: langPack,
			journal: []
		};

		for(var i in app.journal){
			var rec = {};
			//shallow copy of journal records
			for(var j in app.journal[i]){
				rec[j] = app.journal[i][j];
			}

			var date = rec['date'].split('.');
			var start = rec['time_start'].split(':');
			var end = rec['time_end'].split(':');
			var spentTimeInSec = (new Date(date[2], date[1] - 1, date[0], end[0], end[1], end[2]).getTime() - new Date(date[2], date[1] - 1, date[0], start[0], start[1], start[2]).getTime()) / 1000;

			rec.percentage = Math.round(100 * rec['questions_correct'] / rec['questions_total']);
			rec.stat = (rec.percentage > 60) ? "high" : "low";
			rec.spentTime = formatAs2Digits(parseInt(spentTimeInSec/3600)) + ':' + formatAs2Digits(parseInt((spentTimeInSec%3600)/60)) + ':' + formatAs2Digits(spentTimeInSec%60);
			rec.module = app.getNavItemByID(rec['target_item']);
			rec.parent = app.getNavItemByID(rec.module['parent']);
			
			data.journal.push(rec);
		}
		return data;
	}

	this.buildJournal = function () {
		$.get('templates/journal.html', function(journalTemplate){
			var html = doT.template(journalTemplate)(prepareJournalData());

			var journalContents = $("#journal-contents").html(html);
			journalContents.find('a.head').click(function () {
				var $this = $(this);
				open_navbar($this.attr('target_nav'), $this.attr('target_group'), $this.attr('target_item'));
			});

			journalContents.mCustomScrollbar({
				autoDraggerLength: true,
				scrollInertia: 800,
				mouseWheelPixels: 300,
				advanced: {
					updateOnBrowserResize: true,
					updateOnContentResize: true
				}
			});
		});
	};

	function formatAs2Digits(n) {
		return ('00' + n).substr(-2);
	}
	
	this.getNavItemByName = function(itemName){
		var navTypes = ['ebook', 'eapp'];
		for(var i in navTypes){
			var type = navTypes[i];
			var navData = app.navigation[type];
			for (var i in navData) {
				if (navData[i].resource.indexOf(itemName) != -1) {
					return navData[i];
				}
			}
		}
	};

	/**
	 * @param navItem
	 * @param direction "prev" or "next"
	 */
	this.getAdjacentItem = function(navItem, direction){
		var adjacent = navItem[direction];
		if (adjacent)
			while (adjacent.class == "disabled") {
				adjacent = adjacent[direction];
				if (!adjacent) break;
			}
		return adjacent;
	};
	
	this.getNavItemByID = function(id){
		return app.getNavItemByTypeAndID('ebook', id) || app.getNavItemByTypeAndID('eapp', id);
	};
	
	this.getNavItemByTypeAndID = function(navType, id){
		var navData = app.navigation[navType];
		for (var i = 0; i < navData.length; i++) {
			if (navData[i].id == id) {
				return navData[i];
			}
		}
	};

	this.sortJournal = function (by) {
		switch (by) {
			case 'Contents': // По главе
				app.journal.sort(function (x, y) {
					return x[1] - y[1]
				});
				break;
			case 'Date': // По дате
				app.journal.sort(function (x, y) {
					var firstDate = x[6];
					var secondDate = y[6];
					var firstTime = x[11];
					var secondTime = y[11];
					firstDate = firstDate.split('.').reverse();
					secondDate = secondDate.split('.').reverse();
					firstTime = firstTime.split(':');
					secondTime = secondTime.split(':');
					firstDate = firstDate.concat(firstTime);
					secondDate = secondDate.concat(secondTime);

					if (firstDate >= secondDate) {
						return 1;
					} else {
						return -1;
					}
				});
				break;
			case 'Result': // По успеваемости
				app.journal.sort(function (x, y) {
					return y[10] - x[10];
				});
				break;
			default:
				break;
		}
		app.buildJournal();
	};

	this.addJournal = function (rec) {
		var user_id = JSON.parse(shellObj.getUserSettings())['user_id'];
		var parse_str = JSON.parse(rec);
	
		parse_str[parse_str.length - 1] = parseInt(user_id);
		rec = JSON.stringify(parse_str);
		backendBridge.addJournalRec(rec, app.getJournal);
	};
	
	this.saveNewRes = function(_id, _type, _date, _startTime, _endTime, _questionNum, scores){
		console.log("id: " + _id + "	      type: " + _type + "       date: " + _date + "      startTime: " + _startTime + "      _endTime: " + _endTime + "       questionNum: " + _questionNum + "       scores: " + scores);
	};

	this.delJournal = function () {
		backendBridge.clearJournal(app.getJournal);
	};

	this.getEappID = function () {
		var id = this.currentPopupModuleID;
		if (id) return parseInt(id);

		return parseInt(app.currentPageID);
	};

	this.getPopupModuleID = function (path) {
		for (var i = 0; i < this.navigation.eapp.length; i++) {
			if (this.navigation.eapp[i].resource) {
				if (this.navigation.eapp[i].resource == path) {
					var id = this.navigation.eapp[i].id;
					break;
				}
			}
		}
		return id;
	};

	this.printWorkContents = function () {
		backendBridge.printWorkContents();
	};

	this.printContentPage = function () {
		backendBridge.printContentPage(app.currentPageID);
	};

	this.html2printer = function () {
		backendBridge.html2printer();
	};
// #######################################################

	this.getResources = function () {
		backendBridge.getResources(function(resources){
			resources_list_constructor(resources);
		});
	};

	this.getVocabulary = function () {
		backendBridge.getVocabulary(function(vocabulary){
			vocabularyConstructor(vocabulary);
		});
	};

	this.saveExcel = function () {
		backendBridge.saveExcel();
	};

	this.toggleBookmark = function () {
		$(function () {
			backendBridge.inBookmarks(app.currentPageID, function(contains){
				if (contains) {
					backendBridge.removeBookmark(app.currentPageID, function(){
						if ($(".toolspanel-button.bookmark").first().hasClass("bookmarked")) {
							$(".toolspanel-button.bookmark").first().removeClass("bookmarked");
						}
						work_list_constructor();
					});
				} else {
					backendBridge.appendBookmark(app.currentPageID, function(){
						if (!($(".toolspanel-button.bookmark").first().hasClass("bookmarked"))) {
							$(".toolspanel-button.bookmark").first().addClass("bookmarked")
						}
						work_list_constructor();
					});
				}
			});
			
		})
	};

	this.getNote = function (callback) {
		backendBridge.getNote(app.currentPageID, callback);
	};

	function getAndPrepareWorkMenuData(callback){
		backendBridge.getWorkmenu(function(workMenu){
			var pageInfo;
			var parent;

			//data prepare
			for (var i in workMenu['bookmarks']){
				var id = workMenu['bookmarks'][i];

				pageInfo = app.getNavItemByID(id);
				pageInfo = {
					id: pageInfo.id,
					parent: pageInfo.parent,
					textName: app.getIfExists(pageInfo.textName),
					textTitle: app.getIfExists(pageInfo.textTitle)
				};

				parent = app.getNavItemByID(pageInfo['parent']);
				parent = {
					textName: app.getIfExists(parent.textName, '<h3>' + parent.textName + '</h3>'),
					textTitle: app.getIfExists(parent.textTitle)
				};

				workMenu['bookmarks'][i] = {
					pageInfo: pageInfo,
					parent: parent
				};
			}

			for(i in workMenu['notes']){
				var note = workMenu['notes'][i];

				pageInfo = app.getNavItemByID(note['resource_id']);
				pageInfo = {
					id: pageInfo.id,
					parent: pageInfo.parent,
					textName: app.getIfExists(pageInfo.textName),
					textTitle: app.getIfExists(pageInfo.textTitle)
				};

				parent = app.getNavItemByID(pageInfo['parent']);
				parent = {
					textName: app.getIfExists(parent.textName, '<h3>' + parent.textName + '</h3>'),
					textTitle: app.getIfExists(parent.textTitle)
				};

				note.pageInfo = pageInfo;
				note.parent = parent;
			}

			for(i in workMenu['comments']){
				var comment = workMenu['comments'][i];

				pageInfo = app.getNavItemByID(comment['pageId']);
				pageInfo = {
					id: pageInfo.id,
					parent: pageInfo.parent,
					textName: app.getIfExists(pageInfo.textName),
					textTitle: app.getIfExists(pageInfo.textTitle)
				};

				parent = app.getNavItemByID(pageInfo['parent']);
				parent = {
					textName: app.getIfExists(parent.textName, '<h3>' + parent.textName + '</h3>'),
					textTitle: app.getIfExists(parent.textTitle)
				};

				comment.pageInfo = pageInfo;
				comment.parent = parent;
			}

			for(i in workMenu['sels']){
				var sel = workMenu['sels'][i];
				pageInfo = app.getNavItemByID(sel['pageId']);
				pageInfo = {
					id: pageInfo.id,
					parent: pageInfo.parent,
					textName: app.getIfExists(pageInfo.textName),
					textTitle: app.getIfExists(pageInfo.textTitle)
				};

				parent = app.getNavItemByID(pageInfo['parent']);
				parent = {
					textName: app.getIfExists(parent.textName, '<h3>' + parent.textName + '</h3>'),
					textTitle: app.getIfExists(parent.textTitle)
				};

				sel.pageInfo = pageInfo;
				sel.parent = parent;
			}
			
			callback(workMenu);
		});
	}
	
	this.getWorkMenu = function (callback) {
		getAndPrepareWorkMenuData(function(workMenu){
			$.get('templates/workMenu.html', function(template){
				var workHtml = doT.template(template)(workMenu);

				callback(workHtml);
			});
		});
	};
	
	this.getWorkMenuForPrint = function (userName) {
		var workHtml;
		getAndPrepareWorkMenuData(function(workMenu){
			workMenu.userName = userName;
			var date = new Date();
			workMenu.date = formatAs2Digits(date.getDate()) + '.' + formatAs2Digits(date.getMonth() + 1) + '.' + date.getFullYear();
			
			$.ajax({url: 'templates/printWorkMenu.html', async: false}).done(function(data){
				workHtml = doT.template(data)(workMenu);
			});
		});
		
		return workHtml;
	};

// Блок инициализации
	this.initLogin = function () {
		$(function () {
			$("#signin").click(function () {
				app.login();
			});
			$("#signup").click(function () {
				app.openPage("reg");
			});
			$("#login").keypress(function (e) {
				$(".messages.form-row").hide();
				if (e.charCode == 13) {
					app.login();
				}
			});
			$("#password").keypress(function (e) {
				$(".messages.form-row").hide();
				if (e.charCode == 13) {
					app.login();
				}
			});
		})
	};

	this.initReg = function () {
		$(function () {
			$("#back").click(function () {
				app.openPage("auth");
			});
			$("#signup").click(function () {
				app.regMe();
			})
		})
	};

	this.initIndex = function (config) {
		app.applyLang(function(){
			init();
			
			app.setNavigation();
			app.initSearch();
			app.getResources();
			app.getVocabulary();

			work_list_constructor();
		});
		
		$('body').show();
		
		path = 'content/' + config.dir;
		$('#ELMC').html(config.title);
		var img = $('<img />').appendTo($('#preview').empty());
		img.attr('src', path + '/' + config.preview);
		$('#version').html(config.ver);
		$('#product-name').html(config.title);
		$('[licence-product-name]').html(config.title);
		
		app.getJournal();
		app.showLastViewedPage();

		$("#journal-Export").click(app.saveExcel);

		$("#work-menu").click(work_list_constructor);
		
		$("#work-Clear").click(function () {
			$('#highlight-note').remove();
			$("#work-contents").find("ul.list").empty();
			backendBridge.clearWorkmenu();
			toolPanel.reset();
			while (document.getElementsByTagName("iframe")[0].contentDocument.getElementsByClassName("selection").length > 0) {
				var selections = document.getElementsByTagName("iframe")[0].contentDocument.getElementsByClassName("selection");
				for (var i = 0; i < selections.length; i++) {
					selections[i].outerHTML = selections[i].innerHTML;
				}
			}
		});

		$('#licence-open').click(function () {
			$("#licence").css("display", "block");
		});
		$('#licence-close').click(function () {
			$("#licence").css("display", "");
		});

		$("#search-Start").click(function () {
			app.find($('#searchField').val());
		});
		$('#searchField').keydown(function (e) {
			var KEYBOARD_ENTER = 13;
			if (e.keyCode == KEYBOARD_ENTER) app.find($(this).val());
		});
	};
	
	this.applyLang = function(callback){
		backendBridge.getLanguagePack('rus', function(langPack){
			window.langPack = langPack;
			
			for (var item in langPack) {
				var type = item.substr(0, 1);
				var nameParam = item.substring(1, item.length);
				switch (type) {
					case "v":
						if (document.getElementById(nameParam)) document.getElementById(nameParam).value = langPack[item];
						break;
					case "i":
						if (document.getElementById(nameParam)) document.getElementById(nameParam).innerHTML = langPack[item];
						break;
				}
			}
			callback();
		});
	};

	this.initSearch = function () {
		var fileData = [];
		var ebook = app.navigation.ebook;
		for (var i in ebook) {
			if (ebook[i].type != 'item') continue;
			fileData.push({name: ebook[i].resource, id: ebook[i].id});
		}
		backendBridge.initSearch(JSON.stringify(fileData));

		var searchField = $('#searchField');
		var defaultText = searchField.val();
		searchField.focusin(function () {
			if (searchField.val() == defaultText) {
				searchField.val('');
			}
		});
		searchField.focusout(function () {
			if (!searchField.val()) {
				searchField.val(defaultText);
			}
		});
	};

	this.find = function (phrase) {
		backendBridge.find(phrase, function(res){
			var ebook = app.navigation.ebook;
			var html = '';
			var content = $('#search-contents');

			/*html += '<div class="tab-filter">';
			 html += '<label for="search-filter">Показать:</label>';
			 html += '<select id="search-filter">';
			 html += '<option id="search-All" value="search-All">Все</option>';
			 html += '<option id="search-Book" value="search-Book">ЭУ</option>';
			 html += '<option id="search-App" value="search-App">ЭП</option>';
			 html += '<option id="search-Resources" value="search-Resources">Ресурсы</option>';
			 html += '</select>';
			 html += '</div>';*/

			if ($.isEmptyObject(res)) {
				html += langPack['sno_Search'];//'По вашему запросу ничего не найдено';
				console.log(langPack['sno_Search']);
				content.html(html);
				return;
			}

			html += '<ul class="list">';

			for (var fileID in res) {
				html += '<li class="item">';

				for (var i in ebook) {
					if (ebook[i].id == fileID) {
						html += '<a href="#" class="head" target_nav="' + ebook[i].section + '" target_group="' + ebook[i].parent + '" target_item="' + fileID + '">';

						for (var j in ebook) {
							if (ebook[j].id == ebook[i].parent) {
								html += '<span class="group-name">' + ((ebook[i].textName && ebook[i].textName != 'None') ? ('<h3>' + ebook[j].textName + '</h3>') : '') + '<h4>' + ebook[j].textTitle + '</h4></span>';
								break;
							}
						}

						html += '<span class="item-name">' + ((ebook[i].textName && ebook[i].textName != 'None') ? ('<h3>' + ebook[i].textName + '</h3>') : '') + '<h4>' + ebook[i].textTitle + '</h4></span>';
						break;
					}
				}
				html += '</a>';
				html += '<div class="context">' + res[fileID] + '</div>';
				html += '</li>';
			}
			html += '</ul>';

			content.html(html);
			content.find('a.head').click(function () {
				var item = $(this);
				open_navbar(item.attr('target_nav'), item.attr('target_group'), item.attr('target_item'));
				backendBridge.setLastViewedPage(item.attr('target_nav'), item.attr('target_group'), item.attr('target_id'));
			});

			// content.mCustomScrollbar({
			// 	autoDraggerLength: true,
			// 	scrollInertia: 800,
			// 	mouseWheelPixels: 300,
			// 	advanced: {
			// 		updateOnBrowserResize: true,
			// 		updateOnContentResize: true
			// 	}
			// });
		});
	};

	this.saveUISettings = function (theme, interfaceFontSize, contentFontSize, font) {
		backendBridge.saveUISettings(theme, interfaceFontSize, contentFontSize, font);
	};

	this.saveSel = function (htmlText, updateOnComplete) {
		if(typeof updateOnComplete == 'undefined') updateOnComplete = true;
		
		backendBridge.saveSel(app.currentPageID, htmlText, function(){
			if(updateOnComplete) work_list_constructor();
		});
	};

	this.delSelections = function () {
		backendBridge.delSelections(app.currentPageID);
	};

	this.delSelCom = function (nom) {	// удаляет комментарий к текущему выделению (app.high2com по умолч.)
		nom = nom || app.high2com;
		backendBridge.delHighlightComment(app.currentPageID, nom);
		var note = document.getElementById("highlight-note");
		if (note && note.getElementsByTagName("textarea")[0]) {
			note.getElementsByTagName("textarea")[0].value = "";
			$(document.getElementsByTagName("iframe")[0].contentDocument.getElementsByClassName(nom)).find(".noted").remove();
		}
	};

	this.getHighlightComment = function (num, callback) {
		num = num || app.high2com;
		backendBridge.getHighlightComment(app.currentPageID, num, callback);
	};

	this.addHighlightComment = function (selClass, offX) {
		app.high2com = selClass;
		app.getHighlightComment(selClass, function(lastComment){
			createCommentBox(selClass, lastComment);
		});
	};
	
	function createCommentBox(selClass, lastComment){
		lastComment = lastComment || '';
		$('#highlight-note').remove();
		var iframe = $('#ebook  > iframe')[0];
		var iframeDoc = $(iframe.contentDocument);
		var iframeWin = iframe.contentWindow;
		
		var donor = $("#ebook");
		var combox = document.createElement("div");
		combox.id = "highlight-note";
		combox.setAttribute("class", "highlight-note-box");
		
		combox.innerHTML = 
			"<div class='highlight-note-text'>\
				<textarea>" + lastComment + "</textarea>\
			</div>\
			<div class='highlight-note-buttons'>\
				<input type='button' id='highlight-note-Save' value='Cохранить'>\
				<input type='button' id='highlight-note-Delete' value='Удалить'>\
				<input type='button' id='highlight-note-Close' value='Скрыть'>\
			</div>";
		
		combox.setAttribute("style", "left: 100px; top:" + (window.innerHeight - 290) + 'px');
		$(window).resize(locateCommentBox);
		donor.prepend(combox);

		$("#highlight-note-Save").click(function () {
			var notSaved = iframeDoc.find(".notSaved");
			notSaved.removeClass('notSaved').click(iframeWin.addConListener);
			var selText = notSaved.text();
			
			//add comment mark
			var selEnd = iframeDoc.find('.' + selClass).last();
			if(!selEnd.find('.noted').length){
				var notedSpan = document.createElement("span");
				notedSpan.classList.add("noted");
				selEnd.append(notedSpan);
			}

			//save comment
			var comment = $.trim($('.highlight-note-text textarea').val());
			if (comment.length) {
				app.saveSel(iframeDoc.find("#content").html(), false);
				backendBridge.saveHighlightComment(app.currentPageID, selClass, comment, selText, work_list_constructor);
			}
			combox.remove();
		});

		$("#highlight-note-Delete").click(function () {
			app.delRecSel(selClass);
			app.delSelCom(selClass);
			combox.remove();

			iframeDoc.find('.' + selClass).each(function(){
				var selNode = $(this);
				selNode.replaceWith(selNode.html());
			});
		});

		$("#highlight-note-Close").click(function () {
			combox.remove();
		});
	}
	
	function locateCommentBox(){
		var combox = $('#highlight-note');
		if(combox.length)
		combox.css({left: '100px', top: (window.innerHeight - 290) + 'px'});
	}

	this.copyToClipboard = function () {
		shellObj.copyToClipboard();
	};

	this.recSel = function (num, selection) {
		backendBridge.recSel(app.currentPageID, num, selection);
	};

	this.delRecSel = function (num) {
		backendBridge.delRecSel(app.currentPageID, num);
	};

	this.printDownloadUpdateDataStatus = function (status) {
		var updateStatusBlock = $('#updateStatus');
		switch (status) {
			case 100:
				updateStatusBlock.html('Распаковка архива. Не закрывайте приложение до завершения распаковки.');
				break;
			case 101:
				updateStatusBlock.html('Загрузка обновления завершена. Приложение перезапускается, чтобы обновления вступили в силу.');
				break;
			case 102:
				updateStatusBlock.html('Установка соединения с сервером.');
				break;
			case 103:
				updateStatusBlock.html($('#update-box').clone().show());
				updateStatusBlock.find('#update-now').click(function(){
					updateStatusBlock.empty();
					backendBridge.doUpdate();
				});
				updateStatusBlock.find('#update-later').click(function(){
					$('#update-the-box').show();
					updateStatusBlock.hide();
				});
				break;
			case 500:
				$('#update').show();
				updateStatusBlock.html('Нет соединения с интернетом.');
				break;
			case 200:
				$('#update').show();
				updateStatusBlock.html('Установлена последняя версия продукта.');
				break;
			default:
				updateStatusBlock.html('Загрузка обновления: ' + status + '%');
				break;
		}
	};
}