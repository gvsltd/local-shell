/**
 * Created by kravchenko.as on 05.05.2015.
 */

$(function () {
	rangy.init();
	var container = $("#content")[0];
	var contextmenu = null;
	var app = window.parent.parent.app;

	$(window).resize(resizeListener);
	
	function resizeListener() {
		if (!contextmenu) return;
		if (parseInt(contextmenu.style.left) + contextmenu.clientWidth > container.clientWidth) contextmenu.style.left = (container.clientWidth - contextmenu.clientWidth) + 'px';
	}

	window.addConListener = function (e) {
		$("#contextmenu").remove();

		if (window.stopFlag == false) {
			contextmenu = document.createElement("div");
			contextmenu.target = e.currentTarget || e.toElement;
			contextmenu.setAttribute("id", "contextmenu");
			contextmenu.innerHTML = "<ul><li id=\"copyBut\">Копировать</li><li id=\"comBut\">Комментировать</li><li id=\"delBut\">Удалить</li></ul>";
			container.appendChild(contextmenu);
			var contextMenuWidth = contextmenu.clientWidth;
			container.removeChild(contextmenu);

			var x = e.pageX;
			if (x + contextMenuWidth > container.clientWidth) x = container.clientWidth - contextMenuWidth;
			var y = Math.max(e.pageY, lastMousePos.y) + 30;

			contextmenu.style.left = x + "px";
			contextmenu.style.top = y + "px";
			container.appendChild(contextmenu);
		}

		e.preventDefault();
	};

	$(".selection").click(addConListener);

	document.onmousedown = function (e) {
		window.stopFlag = e.toElement.parentNode.parentNode.id == "contextmenu";

		switch (e.toElement.id) {
			case "saveBut":
				save();
				break;

			case "copyBut":
				copy();
				break;

			case "delBut":
				deleteSelection();
				break;

			case "comBut":
				comment();
				break;

			default :
				$("#contextmenu").remove();
				window.getSelection().removeAllRanges();
				var rDoc = window.parent.parent.document;
				if (rDoc.getElementById("highlight-note")) {
					rDoc.getElementById("highlight-note").remove();
				}

				clearHighlight('.notSaved');
		}
	};

	//button handlers
	//////////////////

	function save() {
		$("#contextmenu").remove();

		var newID = getMaxSelectionIndex() + 1 || 1;
		var highlighter = rangy.createHighlighter();
		highlighter.addClassApplier(rangy.createCssClassApplier("selection n" + newID));
		highlighter.highlightSelection("selection n" + newID);
		rangy.getSelection().removeAllRanges();

		$(".selection").click(addConListener);

		var selText = $('.n' + newID).text();
		app.recSel("n" + newID, selText);
		app.saveSel(container.innerHTML);
	}

	function copy() {
		var selected = contextmenu.target;
		$("#contextmenu").remove();

		if (window.getSelection().toString().length) {
			app.copyToClipboard();
			rangy.getSelection().removeAllRanges();
		} else {
			var nodes = $('.' + selected.classList[1]);
			var copyNode = $("<div/>");
			copyNode.append(nodes.clone());
			$("body").append(copyNode);

			var sel = rangy.getSelection();
			var range = rangy.createRange();
			range.selectNode(copyNode[0]);
			sel.setSingleRange(range);
			app.copyToClipboard();
			copyNode.remove();
			sel.removeAllRanges();
		}
	}

	function deleteSelection() {
		var selected = contextmenu.target;
		$("#contextmenu").remove();

		var selector = selected.classList[1];
		clearHighlight('.' + selector);

		app.delRecSel(selector);
		app.saveSel(container.innerHTML);
	}

	function comment() {
		var selClass = '';
		if (window.getSelection().toString().length) {
			var newID = getMaxSelectionIndex() + 1 || 1;
			var highlighter = rangy.createHighlighter();
			highlighter.addClassApplier(rangy.createCssClassApplier("notSaved selection n" + newID));
			highlighter.highlightSelection("notSaved selection n" + newID);
			window.getSelection().removeAllRanges();
			
			selClass = "n" + newID;
		} else {
			var selected = contextmenu.target;
			selClass = selected.classList[1];
		}
		
		app.addHighlightComment(selClass, contextmenu.style.left);
		$("#contextmenu").remove();
	}

	//////////////////////

	function clearHighlight(selector){
		$(selector).each(function(){
			var selNode = $(this);
			selNode.replaceWith(selNode.html());
		});
	}
	
	function getMaxSelectionIndex(){
		var maxNum = -1;
		$(".selection").each(function () {
			var sel = $(this);
			if (!sel.hasClass('notSaved')) {
				var selID = parseInt(sel[0].classList[1].substr(1));
				if (selID > maxNum) maxNum = selID;
			}
		});
		return maxNum;
	}

	var lastMousePos = null;
	container.onmousedown = function (e) {
		window.blockSel = !!e.toElement.classList.contains("selection");
		lastMousePos = {
			x: e.pageX,
			y: e.pageY
		};
	};

	container.onmouseup = function (e) {
		if (e.button == 0 && window.blockSel == false && window.stopFlag == false) {
			var selection = window.getSelection();

			var ourRange;
			if (selection.rangeCount > 0) {
				ourRange = selection.getRangeAt(selection.rangeCount - 1);
			} else {
				return;
			}

			if (ourRange.toString().length > 0) {
				rangy.getSelection().expand("word", {
					trim: true,
					wordOptions: {
						wordRegex: /[\n\s\.,;:a-z0-9]+(['\-][a-z0-9\s]+)*/gi
					}
				});
				$("#contextmenu").remove();

				if (window.stopFlag == false) {
					contextmenu = document.createElement("div");
					contextmenu.setAttribute("id", "contextmenu");
					contextmenu.innerHTML = "<ul><li id=\"copyBut\">Копировать</li><li id=\"saveBut\">Выделить</li><li id=\"comBut\">Комментировать</li></ul>";
					container.appendChild(contextmenu);
					var contextMenuWidth = contextmenu.clientWidth;
					container.removeChild(contextmenu);

					var x = e.pageX;
					if (x + contextMenuWidth > container.clientWidth) x = container.clientWidth - contextMenuWidth;
					var y = Math.max(e.pageY, lastMousePos.y) + 30;

					contextmenu.style.left = x + "px";
					contextmenu.style.top = y + "px";
					container.appendChild(contextmenu);
				}
			}
		} else if (window.stopFlag == false) {
			rangy.getSelection().removeAllRanges();
		}
	}
});