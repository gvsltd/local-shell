/**
 * Created with PyCharm.
 * User: Alex
 * Date: 22.12.2014
 * Time: 20:05
 * To change this template use File | Settings | File Templates.
 */

(function () {

	var srcData = {

		/*config: {
		 dir: 'templates',
		 title: 'Название учебного комплекта <span class="special">Небольшая подпись под&nbsp;названием</span>',
		 preview: 'design/preview.png'
		 },

		 lastViewedPage: {
		 section: 'ebook',
		 parent: 1,
		 id: 2
		 },

		 nav: {
		 "ebook": [{
		 "type": "group",
		 "number": 0,
		 "resource": "",
		 "id": 1,
		 "class": "",
		 "textName": "",
		 "parent": 0,
		 "textTitle": "Введение",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 1,
		 "resource": "ebook_00_00",
		 "id": 2,
		 "class": "page",
		 "textName": "",
		 "parent": 1,
		 "textTitle": "Титульная страница",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 2,
		 "resource": "ebook_00_01",
		 "id": 3,
		 "class": "page",
		 "textName": "",
		 "parent": 1,
		 "textTitle": "Предисловие",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "separator",
		 "number": 0,
		 "resource": "",
		 "id": 4,
		 "class": "",
		 "textName": "Раздел 1",
		 "parent": 0,
		 "textTitle": "Название раздела",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "group",
		 "number": 0,
		 "resource": "",
		 "id": 5,
		 "class": "",
		 "textName": "Глава 1",
		 "parent": 0,
		 "textTitle": "Глава электронного учебника",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 3,
		 "resource": "ebook_01_00",
		 "id": 6,
		 "class": "page",
		 "textName": "",
		 "parent": 5,
		 "textTitle": "Введение",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 4,
		 "resource": "ebook_01_01",
		 "id": 7,
		 "class": "page",
		 "textName": "1.1",
		 "parent": 5,
		 "textTitle": "Заголовки",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 5,
		 "resource": "ebook_01_02",
		 "id": 8,
		 "class": "page",
		 "textName": "1.2",
		 "parent": 5,
		 "textTitle": "Абзацы",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 6,
		 "resource": "ebook_01_03",
		 "id": 9,
		 "class": "page",
		 "textName": "1.3",
		 "parent": 5,
		 "textTitle": "Гиперссылки",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 7,
		 "resource": "ebook_01_04",
		 "id": 10,
		 "class": "page",
		 "textName": "1.4",
		 "parent": 5,
		 "textTitle": "Списки",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 8,
		 "resource": "ebook_01_05",
		 "id": 11,
		 "class": "page",
		 "textName": "1.5",
		 "parent": 5,
		 "textTitle": "Таблицы",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 9,
		 "resource": "ebook_01_06",
		 "id": 12,
		 "class": "page",
		 "textName": "1.6",
		 "parent": 5,
		 "textTitle": "Графика",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 10,
		 "resource": "ebook_01_07",
		 "id": 13,
		 "class": "page",
		 "textName": "1.7",
		 "parent": 5,
		 "textTitle": "Формулы",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 11,
		 "resource": "ebook_01_08",
		 "id": 14,
		 "class": "page",
		 "textName": "1.8",
		 "parent": 5,
		 "textTitle": "Типографика",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 12,
		 "resource": "ebook_01_09",
		 "id": 15,
		 "class": "page",
		 "textName": "1.9",
		 "parent": 5,
		 "textTitle": "Ресурсы и модули",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "link",
		 "number": 0,
		 "resource": "eapp_template",
		 "id": 16,
		 "class": "image",
		 "textName": "",
		 "parent": 15,
		 "textTitle": "Ресурс 1",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "link",
		 "number": 0,
		 "resource": "eapp_template",
		 "id": 17,
		 "class": "image",
		 "textName": "",
		 "parent": 15,
		 "textTitle": "Ресурс 2",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "link",
		 "number": 0,
		 "resource": "eapp_template",
		 "id": 18,
		 "class": "image",
		 "textName": "",
		 "parent": 15,
		 "textTitle": "Ресурс 3",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 13,
		 "resource": "ebook_01_10",
		 "id": 19,
		 "class": "page",
		 "textName": "1.10",
		 "parent": 5,
		 "textTitle": "Нумерация страниц",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "group",
		 "number": 0,
		 "resource": "",
		 "id": 20,
		 "class": "",
		 "textName": "",
		 "parent": 0,
		 "textTitle": "Примеры верстки",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 14,
		 "resource": "ebook_02_01",
		 "id": 21,
		 "class": "page",
		 "textName": "2.1",
		 "parent": 20,
		 "textTitle": "Пример верстки 1",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 15,
		 "resource": "ebook_02_02",
		 "id": 22,
		 "class": "page",
		 "textName": "2.2",
		 "parent": 20,
		 "textTitle": "Пример страницы с&nbsp;совмещенными шапками главы и&nbsp;параграфа",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "separator",
		 "number": 0,
		 "resource": "",
		 "id": 23,
		 "class": "",
		 "textName": "",
		 "parent": 0,
		 "textTitle": "Приложение",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "group",
		 "number": 0,
		 "resource": "",
		 "id": 24,
		 "class": "",
		 "textName": "",
		 "parent": 0,
		 "textTitle": "Приложение",
		 "section": "ebook",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 16,
		 "resource": "ebook_03_01",
		 "id": 25,
		 "class": "page",
		 "textName": "",
		 "parent": 24,
		 "textTitle": "Список литературы",
		 "section": "ebook",
		 "preview": ""
		 }],
		 "eapp": [{
		 "type": "group",
		 "number": 0,
		 "resource": "",
		 "id": 26,
		 "class": "",
		 "textName": "",
		 "parent": 0,
		 "textTitle": "GVS",
		 "section": "eapp",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 1,
		 "resource": "eapp_gvs_slideshow_1",
		 "id": 27,
		 "class": "module theory",
		 "textName": "1.1",
		 "parent": 26,
		 "textTitle": "intro",
		 "section": "eapp",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 2,
		 "resource": "eapp_gvs_slideshow_2",
		 "id": 28,
		 "class": "module theory",
		 "textName": "1.2",
		 "parent": 26,
		 "textTitle": "slideshow",
		 "section": "eapp",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 3,
		 "resource": "eapp_gvs_interactiveTabs",
		 "id": 29,
		 "class": "module theory",
		 "textName": "1.3",
		 "parent": 26,
		 "textTitle": "interactive Tabs",
		 "section": "eapp",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 4,
		 "resource": "eapp_gvs_interactive_scheme",
		 "id": 30,
		 "class": "module theory",
		 "textName": "1.4",
		 "parent": 26,
		 "textTitle": "interactive scheme",
		 "section": "eapp",
		 "preview": ""
		 } , {
		 "type": "item",
		 "number": 5,
		 "resource": "eapp_gvs_interactive_picture",
		 "id": 31,
		 "class": "module theory",
		 "textName": "1.5",
		 "parent": 26,
		 "textTitle": "interactive picture",
		 "section": "eapp",
		 "preview": ""
		 },  {
		 "type": "item",
		 "number": 6,
		 "resource": "miss",
		 "id": 32,
		 "class": "module theory",
		 "textName": "1.6",
		 "parent": 26,
		 "textTitle": "interactive cards",
		 "section": "eapp",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 7,
		 "resource": "eapp_gvs_links_picture",
		 "id": 33,
		 "class": "module practice",
		 "textName": "1.6",
		 "parent": 26,
		 "textTitle": "Mapping with image",
		 "section": "eapp",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 8,
		 "resource": "eapp_gvs_sorting",
		 "id": 34,
		 "class": "module practice",
		 "textName": "1.6",
		 "parent": 26,
		 "textTitle": "Sorting",
		 "section": "eapp",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 9,
		 "resource": "eapp_gvs_interactive_dragDrop",
		 "id": 35,
		 "class": "module practice",
		 "textName": "1.6",
		 "parent": 26,
		 "textTitle": "Drag and drop",
		 "section": "eapp",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 10,
		 "resource": "eapp_gvs_dropdown_2",
		 "id": 36,
		 "class": "module practice",
		 "textName": "1.6",
		 "parent": 26,
		 "textTitle": "Drop down list",
		 "section": "eapp",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 11,
		 "resource": "eapp_gvs_links",
		 "id": 37,
		 "class": "module control",
		 "textName": "1.6",
		 "parent": 26,
		 "textTitle": "Mapping",
		 "section": "eapp",
		 "preview": ""
		 }, {
		 "type": "item",
		 "number": 12,
		 "resource": "miss",
		 "id": 38,
		 "class": "module control",
		 "textName": "1.6",
		 "parent": 26,
		 "textTitle": "Interactive game",
		 "section": "eapp",
		 "preview": ""
		 }]
		 },*/

		config: {
			dir: '301117502',
			title: 'Light Motor Vehicle Maintenance and Repair',
			preview: 'design/preview.png'
		},

		lastViewedPage: {
			section: 'eapp',
			parent: 0,
			id: 26
		},

		nav: {
			"ebook": [],

			"eapp": [
				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 26,
					"class": "",
					"textName": "",
					"parent": 0,
					"textTitle": "Safety and Information Signage",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs1_1_intro",
					"id": 27,
					"class": "module theory",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Ask: Aims and Objectives",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 2,
					"resource": "eapp_gvs_slideshow_2",
					"id": 28,
					"class": "module theory",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Ask: Signs and Signals in Communication",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 3,
					"resource": "eapp_gvs_interactiveTabs",
					"id": 29,
					"class": "module theory",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Ask: Eight Categories of Signs",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 4,
					"resource": "eapp_gvs_interactive_scheme",
					"id": 30,
					"class": "module theory",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Ask: Permanently Fixed and Temporary Signs",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 5,
					"resource": "eapp_gvs_interactive_picture",
					"id": 31,
					"class": "module theory",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Ask: Use of Signs and Signals in the Workplace",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 6,
					"resource": "eapp_gvs_interactive_cards",
					"id": 32,
					"class": "module theory",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Ask: Use of Signs and Signals of Different Categories",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 7,
					"resource": "gvs1_7_mapping_image",
					"id": 33,
					"class": "module practice",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Check: Signs and Their Images",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 8,
					"resource": "gvs1_8_sorting",
					"id": 34,
					"class": "module practice",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Check: Signs and Signals of Different Categories",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 9,
					"resource": "eapp_gvs_interactive_dragDrop",
					"id": 35,
					"class": "module practice",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Check: Signs and Signals in the Workplace",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 10,
					"resource": "gvs1_10_dropdown",
					"id": 36,
					"class": "module practice",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Check: Signs of Different Categories",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 11,
					"resource": "gvs1_11_mapping_text",
					"id": 37,
					"class": "module control",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Test: The Role of Signs of Different Categories",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 12,
					"resource": "gvs1_12_game",
					"id": 38,
					"class": "module control",
					"textName": "",
					"parent": 26,
					"textTitle": "i-Test: Safety and Information Signs",
					"section": "eapp",
					"preview": ""
				},

				// второй айкласс

				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 39,
					"class": "",
					"textName": "",
					"parent": 0,
					"textTitle": "Braking System Units and Components",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs2_1_intro",
					"id": 40,
					"class": "module theory",
					"textName": "",
					"parent": 39,
					"textTitle": "i-Ask: Aims and Objectives",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 2,
					"resource": "eapp_gvs2_interactive3DTabs",
					"id": 42,
					"class": "module theory",
					"textName": "",
					"parent": 39,
					"textTitle": "i-Ask: Units and Components of Braking System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 3,
					"resource": "eapp_gvs2_interactive_picture",
					"id": 43,
					"class": "module theory",
					"textName": "",
					"parent": 39,
					"textTitle": "i-Ask: Construction of Braking System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 4,
					"resource": "brake",
					"id": 41,
					"class": "module theory",
					"textName": "",
					"parent": 39,
					"textTitle": "i-Ask: Operation of Braking System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 5,
					"resource": "gvs2_3d_animation",
					"id": 1141,
					"class": "module theory",
					"textName": "",
					"parent": 39,
					"textTitle": "i-Ask: 3D-Braking System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 6,
					"resource": "eapp_gvs_lv041001010102_interactive_cards_dropdown",
					"id": 44,
					"class": "module practice",
					"textName": "",
					"parent": 39,
					"textTitle": "i-Check: Components of Braking System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 7,
					"resource": "eapp_gvs2_dd_create_picture",
					"id": 45,
					"class": "module practice",
					"textName": "",
					"parent": 39,
					"textTitle": "i-Check: Construction of Braking System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 8,
					"resource": "gvs2_7_name_components",
					"id": 46,
					"class": "module practice",
					"textName": "",
					"parent": 39,
					"textTitle": "i-Check: Components and Operation of Braking System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 9,
					"resource": "gvs2_8_radiobutton",
					"id": 47,
					"class": "module control",
					"textName": "",
					"parent": 39,
					"textTitle": "i-Test: Units and Components of Braking System",
					"section": "eapp",
					"preview": ""

				},

				// третий айкласс

				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 48,
					"class": "",
					"textName": "",
					"parent": 0,
					"textTitle": "Front Brake Pads and Discs Replacement",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs3_1_intro",
					"id": 49,
					"class": "module theory",
					"textName": "",
					"parent": 48,
					"textTitle": "i-Ask: Aims and Objectives",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 2,
					"resource": "eapp_gvs3_interactiveTabs",
					"id": 50,
					"class": "module theory",
					"textName": "",
					"parent": 48,
					"textTitle": "i-Ask: Removal and Replacement of Brake Disk",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 3,
					"resource": "eapp_gvs3_interactive_scheme",
					"id": 51,
					"class": "module theory",
					"textName": "",
					"parent": 48,
					"textTitle": "i-Ask: Tools for Removal and Replacement of Brake Disk",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 4,
					"resource": "gvs3_4_mapping_dd_text",
					"id": 52,
					"class": "module practice",
					"textName": "",
					"parent": 48,
					"textTitle": "i-Check: Removal and Replacement of Brake Disk",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 5,
					"resource": "eapp_gvs3_sequencing",
					"id": 53,
					"class": "module practice",
					"textName": "",
					"parent": 48,
					"textTitle": "i-Check: Sequence of Brake Pad and Disc Replacement",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 6,
					"resource": "gvs3_6_dd_image",
					"id": 54,
					"class": "module practice",
					"textName": "",
					"parent": 48,
					"textTitle": "i-Check: Tools for Removal and Replacement of Brake Disk",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 7,
					"resource": "gvs3_7_radiobutton",
					"id": 55,
					"class": "module control",
					"textName": "",
					"parent": 48,
					"textTitle": "i-Test: Front Brake Pads and Discs Replacement",
					"section": "eapp",
					"preview": ""
				},

				

				//пятый айкласс
/*
				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 70,
					"class": "",
					"textName": "",
					"parent": 0,
					"textTitle": "i-Class 5",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "brake_1",
					"id": 71,
					"class": "i-class 5",
					"textName": "",
					"parent": 70,
					"textTitle": "Slideshow",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 2,
					"resource": "eapp_gvs5_interactive scheme",
					"id": 72,
					"class": "i-class 5",
					"textName": "",
					"parent": 70,
					"textTitle": "Interactive Scheme",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 3,
					"resource": "eapp_iTest_gvs_Legislation",
					"id": 73,
					"class": "i-class 5",
					"textName": "",
					"parent": 70,
					"textTitle": "Legisation",
					"section": "eapp",
					"preview": ""
				},				
*/
				// айкласс для тестирования
/*
				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 74,
					"class": "",
					"textName": "",
					"parent": 0,
					"textTitle": "i-Class Testing",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "eapp_gvs3_interactiveTabs",
					"id": 75,
					"class": "module theory",
					"textName": "",
					"parent": 74,
					"textTitle": "Tabs",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs1_7_mapping_image",
					"id": 76,
					"class": "module practice",
					"textName": "",
					"parent": 74,
					"textTitle": "Mapping с рисунком практика",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs1_7_mapping_imageTest",
					"id": 77,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "Mapping с рисунком тест",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs1_11_mapping_text",
					"id": 78,
					"class": "module practice",
					"textName": "",
					"parent": 74,
					"textTitle": "mapping text practice",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs1_11_mapping_textTest",
					"id": 79,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "mapping text control",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs1_8_sorting",
					"id": 80,
					"class": "module practice",
					"textName": "",
					"parent": 74,
					"textTitle": "Распределение на группы практика",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs1_8_sortingTest",
					"id": 81,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "Распределение на группы тест",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs3_4_dd_text",
					"id": 82,
					"class": "module practice",
					"textName": "",
					"parent": 74,
					"textTitle": "DragDrop текст практика",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs3_4_dd_textTest",
					"id": 83,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "DragDrop текст тест",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs3_6_dd_image",
					"id": 84,
					"class": "module practice",
					"textName": "",
					"parent": 74,
					"textTitle": "DragDrop картинка практика",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs3_6_dd_imageTest",
					"id": 85,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "DragDrop картинка тест",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs1_10_dropdown",
					"id": 86,
					"class": "module practice",
					"textName": "",
					"parent": 74,
					"textTitle": "Выпадающее меню практика",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs1_10_dropdownTest",
					"id": 87,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "Выпадающее меню тест",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "input_gvs",
					"id": 88,
					"class": "module practice",
					"textName": "",
					"parent": 74,
					"textTitle": "Ввод текста практика",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "input_gvsTest",
					"id": 89,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "Ввод текста тест",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs2_7_name_components",
					"id": 90,
					"class": "module practice",
					"textName": "",
					"parent": 74,
					"textTitle": "Name components",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs2_7_name_componentsTest",
					"id": 91,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "Name components",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs3_1_intro",
					"id": 92,
					"class": "module theory",
					"textName": "",
					"parent": 74,
					"textTitle": "Intro module",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs2_1_intro",
					"id": 93,
					"class": "module theory",
					"textName": "",
					"parent": 74,
					"textTitle": "Intro module with table",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs1_12_game",
					"id": 94,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "Интерактивная игра тест",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "eapp_gvs2_dd_create_picture",
					"id": 95,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "Прилипалка sticker",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "eapp_gvs2_interactive_picture",
					"id": 96,
					"class": "module control",
					"textName": "",
					"parent": 74,
					"textTitle": "Интерактивный рисунок picture",
					"section": "eapp",
					"preview": ""
				},



				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 110,
					"class": "",
					"textName": "",
					"parent": 0,
					"textTitle": "Carousel Testing",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "radiobuttonCarousel",
					"id": 111,
					"class": "module control",
					"textName": "",
					"parent": 110,
					"textTitle": "Карусель radiobutton",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "car1",
					"id": 112,
					"class": "module control",
					"textName": "",
					"parent": 110,
					"textTitle": "short 3 click + dragUi id1",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "car2",
					"id": 113,
					"class": "module control",
					"textName": "",
					"parent": 110,
					"textTitle": "long_grand_arrows 5 click + dragUi id2",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "car3",
					"id": 114,
					"class": "module control",
					"textName": "",
					"parent": 110,
					"textTitle": "long_grand_arrows_5 dragOwl + dragUi id3",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "car4",
					"id": 115,
					"class": "module control",
					"textName": "",
					"parent": 110,
					"textTitle": "long_grand_arrows_5 dragUi + dragUi id4",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "car5",
					"id": 116,
					"class": "module control",
					"textName": "",
					"parent": 110,
					"textTitle": "long_grand_arrows_5 scroll",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "carOwl7",
					"id": 117,
					"class": "module control",
					"textName": "",
					"parent": 110,
					"textTitle": "carOwl7",
					"section": "eapp",
					"preview": ""
				},
				*/

				// шестой айкласс Wiper system

				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 118,
					"class": "module theory",
					"textName": "",
					"parent": 0,
					"textTitle": "Wiper System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs6_1_intro",
					"id": 126,
					"class": "module theory",
					"textName": "",
					"parent": 118,
					"textTitle": "i-Ask: Aims and Objectives",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 2,
					"resource": "gvs6_2_interactive_picture",
					"id": 120,
					"class": "module theory",
					"textName": "",
					"parent": 118,
					"textTitle": "i-Ask: Wiper System Components",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 3,
					"resource": "gvs6_3_slideshow1",
					"id": 121,
					"class": "module theory",
					"textName": "",
					"parent": 118,
					"textTitle": "i-Ask: Wiper System Operation",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 4,
					"resource": "gvs6_4_slideshow2",
					"id": 122,
					"class": "module theory",
					"textName": "",
					"parent": 118,
					"textTitle": "i-Ask: Regulations of the Wiper System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 5,
					"resource": "gvs6_5_interactive_cards",
					"id": 119,
					"class": "module theory",
					"textName": "",
					"parent": 118,
					"textTitle": "i-Ask: Servicing the Wiper System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 6,
					"resource": "gvs6_6_dd_mapping_text",
					"id": 123,
					"class": "module practice",
					"textName": "",
					"parent": 118,
					"textTitle": "i-Check: Common Faults in Wiper System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 7,
					"resource": "gvs6_6_mapping_image",
					"id": 124,
					"class": "module practice",
					"textName": "",
					"parent": 118,
					"textTitle": "i-Check: Wiper System Components",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 8,
					"resource": "gvs6_8_radiobutton",
					"id": 125,
					"class": "module control",
					"textName": "",
					"parent": 118,
					"textTitle": "i-Test: Wiper System",
					"section": "eapp",
					"preview": ""
				},

				//седьмой айкласс

				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 127,
					"class": "module theory",
					"textName": "",
					"parent": 0,
					"textTitle": "Starter System",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs7_1_intro",
					"id": 128,
					"class": "module theory",
					"textName": "",
					"parent": 127,
					"textTitle": "i-Ask: Aims and Objectives",
					"section": "eapp",
					"preview": ""
				}, {
                    "type": "item",
                    "number": 3,
                    "resource": "gvs7_interactive_picture",
                    "id": 129,
                    "class": "module theory",
                    "textName": "",
                    "parent": 127,
                    "textTitle": "i-Ask: Starter System",
                    "section": "eapp",
                    "preview": ""
                },{
					"type": "item",
					"number": 2,
					"resource": "gvs7_1_slideshow1",
					"id": 130,
					"class": "module theory",
					"textName": "",
					"parent": 127,
					"textTitle": "i-Ask: Starter Circuit",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 4,
					"resource": "gvs7_4_interactive_cards",
					"id": 131,
					"class": "module theory",
					"textName": "",
					"parent": 127,
					"textTitle": "i-Ask: Starter Motor Components",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 5,
					"resource": "gvs7_5_starter_anim",
					"id": 132,
					"class": "module theory",
					"textName": "",
					"parent": 127,
					"textTitle": "i-Ask: Starter System Testing",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 6,
					"resource": "gvs7_6_mapping_text",
					"id": 133,
					"class": "module practice",
					"textName": "",
					"parent": 127,
					"textTitle": "i-Check: Starter System Components",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 7,
					"resource": "gvs7_7_radiobutton",
					"id": 134,
					"class": "module control",
					"textName": "",
					"parent": 127,
					"textTitle": "i-Test: Starter System Components",
					"section": "eapp",
					"preview": ""
				},

				// id c 129 по 134

				// восьмой айкласс

				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 135,
					"class": "module theory",
					"textName": "",
					"parent": 0,
					"textTitle": "Electrical Principles",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs8_1_intro",
					"id": 136,
					"class": "module theory",
					"textName": "",
					"parent": 135,
					"textTitle": "i-Ask: Aims and Objectives",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 2,
					"resource": "gvs8_2_ohm",
					"id": 137,
					"class": "module theory",
					"textName": "",
					"parent": 135,
					"textTitle": "i-Ask: Ohm’s Law",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 3,
					"resource": "gvs8_3_slideshow",
					"id": 138,
					"class": "module theory",
					"textName": "",
					"parent": 135,
					"textTitle": "i-Ask: Testing Circuits and Ciring Types",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 5,
					"resource": "gvs8_5_dropdown",
					"id": 140,
					"class": "module practice",
					"textName": "",
					"parent": 135,
					"textTitle": "i-Check: Basic Electrical Terms",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 6,
					"resource": "gvs8_6_radiobutton",
					"id": 141,
					"class": "module control",
					"textName": "",
					"parent": 135,
					"textTitle": "i-Test: Basic Electrical Terms",
					"section": "eapp",
					"preview": ""
				},

				// id c 136 по 141

				// девятый айкласс

				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 142,
					"class": "module theory",
					"textName": "",
					"parent": 0,
					"textTitle": "Personal Protective Equipment",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 1,
					"resource": "gvs9_1_intro",
					"id": 143,
					"class": "module theory",
					"textName": "",
					"parent": 142,
					"textTitle": "i-Ask: Aims and Objectives",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 2,
					"resource": "gvs9_2_scheme",
					"id": 144,
					"class": "module theory",
					"textName": "",
					"parent": 142,
					"textTitle": "i-Ask: A Hazard and a Risk",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 3,
					"resource": "gvs9_3_interactive_cards",
					"id": 145,
					"class": "module theory",
					"textName": "",
					"parent": 142,
					"textTitle": "i-Ask: Types of Personal and Vehicle Protective Equipment",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 4,
					"resource": "gvs9_9_dd_clothes",
					"id": 146,
					"class": "module practice",
					"textName": "",
					"parent": 142,
					"textTitle": "i-Check: Personal Protective Equipment",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"number": 5,
					"resource": "gvs9_4_mapping_image",
					"id": 147,
					"class": "module practice",
					"textName": "",
					"parent": 142,
					"textTitle": "i-Check: Risks and Safety Measures",
					"section": "eapp",
					"preview": ""
				}, 
				/*
				{
					"type": "item",
					"number": 6,
					"resource": "gvs9_5_checkbox",
					"id": 149,
					"class": "module control",
					"textName": "",
					"parent": 142,
					"textTitle": "i-Test: Risks and Safety ",
					"section": "eapp",
					"preview": ""
				}, 
				*/
				{
					"type": "item",
					"number": 7,
					"resource": "gvs9_6_radiobutton",
					"id": 148,
					"class": "module control",
					"textName": "",
					"parent": 142,
					"textTitle": "i-Test: Personal Protective Equipment",
					"section": "eapp",
					"preview": ""
				},
                // четвертый айкласс

				{
					"type": "group",
					"number": 0,
					"resource": "",
					"id": 56,
					"class": "",
					"textName": "",
					"parent": 0,
					"textTitle": "Legislative and organisational responsibilities, requirements and procedures",
					"section": "eapp",
					"preview": ""
				}, {
                    "type": "item",
                    "number": 1,
                    "resource": "gvs10_1_intro",
                    "id": 151,
                    "class": "module theory",
                    "textName": "",
                    "parent": 56,
                    "textTitle": "i-Ask: Aims and Objectives",
                    "section": "eapp",
                    "preview": ""
                }, {
					"type": "item",
					"number": 2,
					"resource": "eapp_gvs4_slideshow_2",
					"id": 1157,
					"class": "module theory",
					"textName": "",
					"parent": 56,
					"textTitle": "i-Ask: Health and Safety at Work Act",
					"section": "eapp",
					"preview": ""
				},
				/*
				{
					"type": "item",
					"number": 1,
					"resource": "eapp_gvs4_slideshow_1",
					"id": 57,
					"class": "module theory",
					"textName": "",
					"parent": 56,
					"textTitle": "Slideshow",
					"section": "eapp",
					"preview": ""
				},
				
				{
					"type": "item",
					"number": 2,
					"resource": "eapp_gvs4_slideshow_2",
					"id": 58,
					"class": "module theory",
					"textName": "",
					"parent": 56,
					"textTitle": "Slideshow_2",
					"section": "eapp",
					"preview": ""
				},
				*/
				{
					"type": "item",
					"number": 3,
					"resource": "gvs4_3_interactive_cards",
					"id": 159,
					"class": "module practice",
					"textName": "",
					"parent": 56,
					"textTitle": "i-Ask: Health and Safety at Work Act. Questions",
					"section": "eapp",
					"preview": ""
				},
				/*
				 {
					"type": "item",
					"number": 3,
					"resource": "eapp_gvs4_input",
					"id": 59,
					"class": "module practice",
					"textName": "",
					"parent": 56,
					"textTitle": "Legislative and organisational responsibilities, requirements and procedures",
					"section": "eapp",
					"preview": ""
				}, 
				
				{
					"type": "item",
					"number": 4,
					"resource": "eapp_gvs4_interactive_scheme",
					"id": 60,
					"class": "module theory",
					"textName": "",
					"parent": 56,
					"textTitle": "interacticve scheme",
					"section": "eapp",
					"preview": ""
				},
				*/
				{
					"type": "item",
					"number": 5,
					"resource": "gvs4_5_sorting",
					"id": 61,
					"class": "module control",
					"textName": "",
					"parent": 56,
					"textTitle": "i-Ask: Health and Safety at Work Act. Questions",
					"section": "eapp",
					"preview": ""
				}, 
				/*
				{
					"type": "item",
					"number": 6,
					"resource": "eapp_gvs4_crossword",
					"id": 62,
					"class": "module control",
					"textName": "",
					"parent": 56,
					"textTitle": "Crossword",
					"section": "eapp",
					"preview": ""
				}, {
					"type": "item",
					"resource": "eapp_gvs3_dropdown",
					"id": 63,
					"class": "module control",
					"textName": "",
					"parent": 56,
					"textTitle": "dropdown",
					"section": "eapp",
					"preview": ""
				},
				*/
			]
		},

		vocabulary: {
			"Мо­ляр­ная мас­са": "мас­са од­но­го мо­ля ве­ще­ства.",
			"Омы­ле­ние жи­ров": "гид­ро­лиз слож­ных эфи­ров кар­бо­но­вых кис­лот с об­ра­зо­ва­ни­ем спир­та и кис­ло­ты (или ее со­ли, ко­гда для омы­ле­ния бе­рут рас­твор ще­ло­чи).",
			"Кис­ло­ты": "со­еди­не­ния, в со­став ко­то­рых обыч­но вхо­дят ато­мы во­до­ро­да, спо­соб­ные за­ме­щать­ся на ато­мы ме­тал­лов и кис­лот­ный оста­ток.",
			"Мо­ляр­ная кон­цен­тра­ция (мо­ляр­ность)": "чис­ло мо­лей рас­тво­рен­но­го ве­ще­ства, со­дер­жа­ще­го­ся в 1 л рас­тво­ра.",
			"Ос­но­ва­ния": "со­еди­не­ния, в со­став ко­то­рых вхо­дят ато­мы ме­тал­ла или иона ам­мо­ния и гид­рок­со­груп­пы (—OH).",
			"Вул­ка­ни­за­ция": "тех­но­ло­ги­че­ский про­цесс вза­и­мо­дей­ствия ка­у­чу­ков с вул­ка­ни­зу­ю­щим аген­том (се­ра, ок­си­ды ме­тал­лов и др.), при ко­то­ром про­ис­хо­дит сши­ва­ние мо­ле­кул ка­у­чу­ка в еди­ную про­стран­ствен­ную сет­ку, что по­вы­ша­ет проч­ност­ные ха­рак­те­ри­сти­ки ка­у­чу­ка — эла­стич­ность, твер­дость.",
			"Азео­тро­пы": "жид­кие сме­си, ха­рак­те­ри­зу­ю­щи­е­ся ра­вен­ством со­ста­вов рав­но­вес­ных жид­кой и па­ро­вой фаз, при их пе­ре­гон­ке об­ра­зу­ет­ся кон­ден­сат то­го же со­ста­ва, что и ис­ход­ный рас­твор.",
			"Ва­лент­ность": "спо­соб­ность ато­мов хи­ми­че­ских эле­мен­тов об­ра­зо­вы­вать опре­де­лен­ное чис­ло хи­ми­че­ских свя­зей с ато­ма­ми дру­гих эле­мен­тов.",
			"Изо­ме­рия": "су­ще­ство­ва­ние со­еди­не­ний, оди­на­ко­вых по со­ста­ву и мо­ле­ку­ляр­ной мас­се, но раз­ли­ча­ю­щих­ся по стро­е­нию или рас­по­ло­же­нию ато­мов в про­стран­стве и, вслед­ствие это­го, по свой­ствам.",
			"Кри­стал­ло­гид­ра­ты": "кри­стал­ли­че­ские ве­ще­ства, со­дер­жа­щие в сво­ем со­ста­ве во­ду.",
			"Га­ло­ге­ны": "хи­ми­че­ские эле­мен­ты глав­ной под­груп­пы VII груп­пы Пе­ри­о­ди­че­ской таб­ли­цы хи­ми­че­ских эле­мен­тов, в ко­то­рую вхо­дят фтор, хлор, бром, иод, астат.",
			"Пра­ви­ло Вант-Гоф­фа": "при по­вы­ше­нии тем­пе­ра­ту­ры на каж­дые 10 гра­ду­сов кон­стан­та ско­ро­сти го­мо­ген­ной эле­мен­тар­ной ре­ак­ции уве­ли­чи­ва­ет­ся в два-че­ты­ре ра­за.",
			"Кри­стал­ли­за­ция": "про­цесс фа­зо­во­го пе­ре­хо­да ве­ще­ства из жид­ко­го со­сто­я­ния в твер­дое кри­стал­ли­че­ское с об­ра­зо­ва­ни­ем кри­стал­лов.",
			"Гид­ра­та­ция": "при­со­еди­не­ние мо­ле­кул во­ды к мо­ле­ку­лам или ионам.",
			"Во­до­род­ная связь": "хи­ми­че­ская связь меж­ду по­ло­жи­тель­но за­ря­жен­ным ато­мом во­до­ро­да од­ной мо­ле­ку­лы и от­ри­ца­тель­но за­ря­жен­ным ато­мом дру­гой мо­ле­ку­лы.",
			"Али­фа­ти­че­ские (ацик­ли­че­ские) уг­ле­во­до­ро­ды": "со­еди­не­ния с от­кры­той уг­ле­род­ной це­пью; мо­гут быть на­сы­щен­ны­ми (ал­ка­ны и их про­из­вод­ные) и не­на­сы­щен­ны­ми (ал­ке­ны, ал­ки­ны, ал­ка­ди­е­ны и их про­из­вод­ные).",
			"Бен­зол": "ро­до­на­чаль­ник уг­ле­во­до­ро­дов аро­ма­ти­че­ско­го ря­да, в мо­ле­ку­лах ко­то­рых со­дер­жит­ся од­но или не­сколь­ко бен­золь­ных ко­лец.",
			"Бла­го­род­ные га­зы": "хи­ми­че­ские эле­мен­ты глав­ной под­груп­пы VIII груп­пы, в ко­то­рую вхо­дят ге­лий, не­он, ар­гон, крип­тон, ксе­нон, ра­дон.",
			"Гид­ро­лиз со­лей": "об­мен­ная ре­ак­ция меж­ду со­лью и во­дой, в ре­зуль­та­те ко­то­рой об­ра­зу­ют­ся кис­ло­та (или кис­лая соль) и ос­но­ва­ние (или ос­нов­ная соль).",
			"Ок­си­ды": "со­еди­не­ния, со­сто­я­щие из двух эле­мен­тов, од­ним из ко­то­рых яв­ля­ет­ся кис­ло­род в сте­пе­ни окис­ле­ния —2.",
			"Бел­ки": "при­род­ные по­ли­пеп­ти­ды с вы­со­ким зна­че­ни­ем мо­ле­ку­ляр­ной мас­сы (от де­сят­ков ты­сяч до де­сят­ков мил­ли­о­нов), со­сто­я­щие из со­еди­нен­ных в це­поч­ку α-ами­но­кис­лот.",
			"Моль­ная до­ля рас­тво­рен­но­го ве­ще­ства": "от­но­ше­ние ко­ли­че­ства рас­тво­рен­но­го ве­ще­ства к об­ще­му ко­ли­че­ству ве­ществ в рас­тво­ре.",
			"Изо­то­пы": "раз­но­вид­но­сти ато­мов од­но­го хи­ми­че­ско­го эле­мен­та с раз­ным ко­ли­че­ством ней­тро­нов в яд­ре и мас­со­вым чис­лом, но с оди­на­ко­вым за­ря­дом яд­ра.",
			"Гид­ри­ро­ва­ние": "ре­ак­ция при­со­еди­не­ния во­до­ро­да по крат­ной свя­зи в мо­ле­ку­ле ве­ще­ства.",
			"Мас­со­вая до­ля рас­тво­рен­но­го ве­ще­ства": "от­но­ше­ние мас­сы рас­тво­рен­но­го ве­ще­ства к об­щей мас­се рас­тво­ра, вы­ра­жа­ет­ся в до­лях еди­ни­цы или про­цен­тах.",
			"Моль": "ко­ли­че­ство ве­ще­ства, ко­то­рое со­дер­жит столь­ко же ча­стиц (мо­ле­кул, ато­мов, ионов, элек­тро­нов), сколь­ко ато­мов уг­ле­ро­да со­дер­жит­ся в 12 г изо­то­па уг­ле­ро­да-12.",
			"Окис­ле­ние": "про­цесс от­да­чи элек­тро­нов ато­мом, мо­ле­ку­лой или ионом.",
			"Вос­ста­нов­ле­ние": "про­цесс при­со­еди­не­ния элек­тро­нов ато­мом, мо­ле­ку­лой или ионом.",
			"Де­гид­ри­ро­ва­ние": "ре­ак­ция от­щеп­ле­ния во­до­ро­да от мо­ле­ку­лы ор­га­ни­че­ско­го со­еди­не­ния.",
			"Ка­та­ли­за­тор": "хи­ми­че­ское ве­ще­ство, из­ме­ня­ю­щее ско­рость хи­ми­че­ской ре­ак­ции, но не вхо­дя­щее в со­став про­дук­тов ре­ак­ции."
		},

		languagePack: {
			"ihelp": "Помощь",
			"sresources": "Ресурсы",
			"iresources_app": "Ресурсы",
			"sShow_journal": "Показать",
			"iw_SortByContents": "По@содержанию",
			"ij_SortByResult": "Успеваемость",
			"ij_SortByContents": "Содержание",
			"sall": "Все",
			"iabout-menu": "О@продукте",
			"ihide": "Скрыть",
			"vj_Export": "Экспорт",
			"ij_SortByDate": "Дата",
			"iglossary_app": "Словарь",
			"vebook-menu": "Учебник",
			"vwork-Clear": "Очистить",
			"iw_SortByDate": "По@дате",
			"sselected": "Выделенное",
			"vj_Clear": "Очистить",
			"ilinks_app": "Ссылки",
			"ijouirnal_app": "Журнал",
			"idelete": "Удалить",
			"seu": "ЭУ",
			"vwork-Print": "Печать",
			"sno_Search": "По@вашему@запросу@ничего@не@найдено",
			"ifavourites": "Заметки",
			"sbookmarks": "Закладки",
			"iview-menu": "Внешний@вид",
			"vj_Print": "Печать",
			"iadd_l": "Дополнительно",
			"veapp-mebu": "Приложение",
			"vsearch-Start": "Поиск",
			"isave": "Сохранить",
			"snotes": "Комментарии",
			"iaccount-menu": "Ваш@профиль",
			"sShow": "Показать",
			"sall_journal": "Все",
			"sep": "ЭП",
			"vsearchField": "Поиск@по@содержанию"
		},

		hrefs: {
			"1": {
				"title": "АЛХИМИК",
				"href": "http://www.alhimik.ru",
				"description": "Химические новости, полезная информация о химических веществах и явлениях",
				"img": null
			},
			"2": {
				"title": "Химическая энциклопедия",
				"href": "http://www.edudic.ru/hie",
				"description": "Научно-справочный ресурс, охватывающий все разделы химии и химической технологии",
				"img": null
			},
			"3": {
				"title": "Сайт  Федерального центра информационно-образовательных ресурсов (ФЦИОР)",
				"href": "http://fcior.edu.ru/card/2461/ponyatie-o-himicheskoy-reakcii-klassifikaciya-himicheskih-reakciy-v-neorganicheskoy-i-organicheskoy.html",
				"description": "Понятие о химической реакции. Классификация химических реакций в неорганической и органической химии",
				"img": null
			},
			"4": {
				"title": "Сайт  Федерального центра информационно-образовательных ресурсов (ФЦИОР)",
				"href": "http://fcior.edu.ru/card/5896/obshie-sposoby-polucheniya-metallov.html",
				"description": "Общие способы получения металлов",
				"img": null
			},
			"5": {
				"title": "Сайт  Федерального центра информационно-образовательных ресурсов (ФЦИОР)",
				"href": "http://fcior.edu.ru/card/2573/fizicheskie-svoystva-metallov.html",
				"description": "Физические свойства металлов",
				"img": null
			},
			"6": {
				"title": "Сайт  Федерального центра информационно-образовательных ресурсов (ФЦИОР)",
				"href": "http://fcior.edu.ru/card/13217/rol-himii-v-zhizni-cheloveka-himiya-v-stroitelstve.html",
				"description": "Роль химии в жизни человека. Химия в строительстве",
				"img": null
			},
			"7": {
				"title": "Сайт  Федерального центра информационно-образовательных ресурсов (ФЦИОР)",
				"href": "http://fcior.edu.ru/card/4761/rol-himii-v-zhizni-cheloveka-himiya-v-avtomobilestroenii.html",
				"description": "Роли химии в жизни человека. Химия в автомобилестроении",
				"img": null
			},
			"8": {
				"title": "Сайт  Федерального центра информационно-образовательных ресурсов (ФЦИОР)",
				"href": "История химии",
				"description": "http://fcior.edu.ru/card/10825/istoriya-himii-uglublennyy-uroven-slozhnosti.html",
				"img": null
			}
		},

		workMenu: {
			bookmarks: [],
			notes: [],
			sels: []
		}
	};

	window.backendBridge = {
		getConfig: function () {
			return srcData.config;
		},

		login: function (login, password, completeCallback) {

		},

		regMe: function (login, password, fio, completeCallback) {

		},

		openWorkPage: function (contentDir) {

		},

		getLanguagePack: function (lang, completeCallback) {
			completeCallback(srcData.languagePack);
		},

		getNavigation: function (completeCallback) {
			completeCallback(srcData.nav);
		},

		setLastViewedPage: function (type, parent, id, completeCallback) {

		},

		getLastViewedPage: function (completeCallback) {
			completeCallback(srcData.lastViewedPage);
		},

		saveUISettings: function (theme, interfaceFontSize, contentFontSize, font, completeCallback) {

		},

		getUserSettings: function (completeCallback) {
			completeCallback(null);
		},

		getWorkmenu: function (completeCallback) {
			completeCallback(srcData.workMenu);
		},

		clearWorkmenu: function (completeCallback) {
			completeCallback && completeCallback();
		},

		inBookmarks: function (pageID, completeCallback) {
			completeCallback(false);
		},

		appendBookmark: function (pageID, completeCallback) {
			completeCallback && completeCallback();
		},

		removeBookmark: function (pageID, completeCallback) {
			completeCallback && completeCallback();
		},

		addNote: function (pageID, text, completeCallback) {
			completeCallback && completeCallback();
		},

		getNote: function (pageID, completeCallback) {
			completeCallback('');
		},

		delNote: function (pageID, completeCallback) {
			completeCallback && completeCallback();
		},

		saveSel: function (pageID, htmlText, completeCallback) {
			completeCallback && completeCallback();
		},

		recSel: function (pageID, classNum, completeCallback) {
			completeCallback && completeCallback();
		},

		delRecSel: function (pageID, classNum, completeCallback) {
			completeCallback && completeCallback();
		},

		delSelections: function (pageID, completeCallback) {
			completeCallback && completeCallback();
		},

		getSelection: function (pageID, completeCallback) {
			completeCallback();
		},

		delHighlightComment: function (pageID, num, completeCallback) {
			completeCallback && completeCallback();
		},

		saveHighlightComment: function (pageID, classNum, comment, completeCallback) {
			completeCallback && completeCallback();
		},

		getHighlightComment: function (pageID, num, completeCallback) {
			completeCallback();
		},

		getJournal: function (completeCallback) {
			completeCallback([]);
		},

		addJournalRec: function (rec, completeCallback) {
			completeCallback();
		},

		clearJournal: function (completeCallback) {
			completeCallback();
		},

		getHrefs: function (completeCallback) {
			completeCallback(srcData.hrefs);
		},

		getResources: function (completeCallback) {
			completeCallback();
		},

		getVocabulary: function (completeCallback) {
			completeCallback(srcData.vocabulary);
		},

		openUrl: function (url) {

		},

		printWorkContents: function () {

		},

		printContentPage: function (pageID) {

		},

		html2printer: function () {

		},

		initSearch: function (fileData, completeCallback) {
			completeCallback && completeCallback();
		},

		find: function (phrase, completeCallback) {
			completeCallback();
		},

		saveExcel: function () {

		},

		checkForUpdate: function () {

		}
	};
})();