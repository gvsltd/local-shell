#include <QCoreApplication>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>
#include <QJsonArray>

QList<QString> extensionFilters;
QList<QString> dirFilters;
qint64 offset = 4;
qint64 count = 0;
qint64 contentSize;
QVariantMap mapDict;
QFile container("../../container.evl");

void processDir(QDir dir, void (*callback)(QString, QFileInfo) );
void assembleJson(QString filePath, QFileInfo fileInfo);
void assembleContent(QString filePath, QFileInfo fileInfo);
QByteArray encrypt(QByteArray source);

int main(int argc, char *argv[]){
    QCoreApplication a(argc, argv);

    QDir rootdir("../../html");

    extensionFilters << "cdr" << "cdx" << "eps" << "psd" << "zip" << "rar" << "indd" << "doc" << "docx" << "rtf" << "tbl" << "pdf" << "fla" << "xls" << "xlsx" << "ai" << "gitignore" << "pmd" << "p65";
    dirFilters << ".git" << ".idea";

    container.open(QIODevice::WriteOnly);

    QDir contentDirs("../../html/content/");
    contentDirs.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    QVariantMap json;
    QFileInfoList list = contentDirs.entryInfoList();
    QStringList dirs;
    foreach (QFileInfo dir, list) {
        dirs.append(dir.fileName());
    }
    json["dirs"] = dirs;

    processDir(rootdir, &assembleJson);
    json["map"] = mapDict;
    QByteArray jsonBytes = QJsonDocument::fromVariant(json).toJson();

    QByteArray encryptedJson = encrypt(jsonBytes);

    qint32 size = encryptedJson.size();
    QByteArray jsonSize((const char *)&size, 4);

    container.write(encrypt(jsonSize));
    qDebug() << "json assembled";
    container.write(encryptedJson);

    contentSize = offset;
    offset = 0;

    processDir(rootdir, &assembleContent);

    container.close();
    qDebug() << "complete";

    return a.exec();
}

void processDir(QDir dir, void (*callback)(QString, QFileInfo) ){
    dir.setFilter(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    dir.setSorting(QDir::Type);
    QFileInfoList content = dir.entryInfoList();

    foreach (QFileInfo fileInfo, content){
        QString filePath = fileInfo.filePath();
        if (fileInfo.isFile()){
            QString extension = fileInfo.suffix().toLower();
            if (extensionFilters.contains(extension)) continue;

            callback(filePath.toLower(), fileInfo);
        }
        else if (fileInfo.isDir()){
            if (dirFilters.contains(fileInfo.fileName())) continue;
            processDir(QDir(filePath), callback);
        }
    }
}

void assembleJson(QString filePath, QFileInfo fileInfo){
    QVariantMap data;
    data["offset"] = offset;
    data["size"] = fileInfo.size();

    mapDict[filePath.replace("../../" , "")] = data;
    offset += fileInfo.size();
}

void assembleContent(QString filePath, QFileInfo fileInfo){
    offset += fileInfo.size();
    if(!(count % 100)) qDebug() << count << " : " << offset / 1024 << " / " << contentSize / 1024 << "; " << (float(offset) / float(contentSize) * 100) << '%';

    QFile file(filePath);
    file.open(QIODevice::ReadOnly);
    QByteArray encryptedFile = encrypt(file.readAll());

    container.write(encryptedFile);
    file.close();

    count++;
}

QByteArray encrypt(QByteArray source){
    int key = 56;
    int letter;
    QByteArray cipher;
    int encSymbol;
    int sourceSize = source.size();

    for (int i = 0; i < sourceSize; ++i) {
      letter = int(source[i]) + 128;
      encSymbol = letter ^ key;
      cipher.append(char(encSymbol - 128));

      key = ((key * key * ((letter * encSymbol) % 213)) + (((letter + encSymbol) % 43) + i)) % 256;
    }

    return cipher;
}
