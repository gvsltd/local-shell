#include <QFile>
#include <QVariant>
#include <QJsonDocument>
#include <QJsonObject>

#ifndef STORAGE_H
#define STORAGE_H


class Storage {
public:
    Storage(QString path);
    QVariantMap pathMap;
    QFile storage;
    QByteArray getFile(QString path);
    QByteArray decrypt(QByteArray source);

private:
    qint64 offset;
};

#endif // STORAGE_H
