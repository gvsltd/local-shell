#include <QCoreApplication>
#include <QDir>
#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>
#include <QWidget>
#include <ui_dialog.h>
#include "updatethread.h"
#include "worker.h"
#include <iostream>
using namespace std;

Ui::Dialog ui;

QString updPath;
QString appPath;

int main(int argc, char *argv[]){
    QApplication a(argc, argv);


    if(argc > 1){
        updPath = QString(argv[1]);
        appPath = QString(argv[2]);
    }else{
        updPath = "C:/Users/kazurov.vs/AppData/Local/cppShell/update/";
        appPath = "C:/Users/kazurov.vs/Desktop/GIT-HTML/";
    }

    //вызывающее приложение передает апдейтеру свои параметры
    QStringList appArgs;
    for(int i = 3; i < argc; i++){
        appArgs << argv[i];
    }

    UpdateThread *updThread = new UpdateThread(updPath, appPath, appArgs);
    Worker *worker = new Worker(&ui);

    QObject::connect(updThread, SIGNAL(updateProgress(int)), worker, SLOT(setProgressBarValue(int)));

    QDialog *dialog = new QDialog;
    ui.setupUi(dialog);

    dialog->show();

    updThread->start();

    return a.exec();
}
