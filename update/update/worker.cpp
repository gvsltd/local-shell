#include "worker.h"

Worker::Worker(Ui::Dialog *ui){
    this->ui = ui;
}

void Worker::setProgressBarValue(int num){
    ui->progressBar->setValue(num);
}
