#-------------------------------------------------
#
# Project created by QtCreator 2014-12-15T16:47:06
#
#-------------------------------------------------

QT       += core widgets

QT       -= gui

TARGET = update
#CONFIG   += console
CONFIG   += c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    storage.cpp \
    dialog.cpp \
    updatethread.cpp \
    worker.cpp

HEADERS += \
    storage.h \
    dialog.h \
    updatethread.h \
    worker.h

FORMS += \
    dialog.ui
