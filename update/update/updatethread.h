#ifndef UPDATETHREAD_H
#define UPDATETHREAD_H

#include <QtCore>
#include <QDir>
#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>
#include "storage.h"
#include <ui_dialog.h>

class UpdateThread : public QThread{
    Q_OBJECT

    QStringList appArgs;
    QString updPath;
    QString appPath;
    QString updEvlContainerName = "updContainer.evl";
    qint64 offset = 4;
    qint64 contentSize = 0;

public:
    UpdateThread(QString updPath, QString appPath, QStringList appArgs);
    void remaking(Storage *updStorage, Storage *appStorage);
    void sizeFiles(QDir dir);
    void transferFiles(QDir dir);
    void run();
signals:
    void updateProgress(int);
};

#endif // UPDATETHREAD_H
