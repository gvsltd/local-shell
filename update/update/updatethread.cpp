#include "updatethread.h"
#include <math.h>

UpdateThread::UpdateThread(QString updPath, QString appPath, QStringList appArgs){
    this->appArgs = appArgs;
    this->updPath = updPath;
    this->appPath = appPath;
}

void UpdateThread::remaking(Storage *updStorage, Storage *appStorage){
    contentSize = contentSize - QFileInfo(updPath + updEvlContainerName).size();
    QFile container(updPath + "cocontainer.evl");
    container.open(QIODevice::WriteOnly);

    QVariantMap json;
    json["dirs"] = appStorage->pathMap["dirs"].toStringList();

    QVariantMap appStorageMap = appStorage->pathMap["map"].toMap();
    QVariantMap updStorageMap = updStorage->pathMap["map"].toMap();

    QStringList storageKeys = appStorageMap.keys();
    QStringList updStorageKeys = updStorageMap.keys();

    //сливаем ключи
    for(int i = 0; i < updStorageKeys.size(); i++){
        if(!storageKeys.contains(updStorageKeys[i])) storageKeys.append(updStorageKeys[i]);
    }

    QVariantMap mapDict;
    for(int i = 0; i < storageKeys.size(); i++){
         QVariantMap data;
         QString key = storageKeys[i];
         qint64 size = 0;
         if(updStorageMap.contains(key)) size = updStorageMap[key].toMap()["size"].toInt();
         else size = appStorageMap[key].toMap()["size"].toInt();
         data["offset"] = offset;
         data["size"] = size;
         mapDict[key] = data;
         offset += size;
    }

    json["map"] = mapDict;
    QByteArray jsonBytes = QJsonDocument::fromVariant(json).toJson();
    QByteArray encryptedJson = updStorage->decrypt(jsonBytes);
    qint32 size = encryptedJson.size();
    QByteArray jsonSize((const char *)&size, 4);
    container.write(updStorage->decrypt(jsonSize));
    container.write(encryptedJson);

    contentSize += offset;
    offset = 0;

    for(int i = 0; i < storageKeys.size(); i++){
         QString key = storageKeys[i];
         QByteArray bytes;
         if(updStorageMap.contains(key)) bytes = updStorage->getFile(key);
         else bytes = appStorage->getFile(key);
         container.write(bytes);
         offset += bytes.size();
         emit updateProgress(round(float(offset) / float(contentSize) * 100));
    }

    container.close();
    updStorage->storage.close();
    appStorage->storage.close();

    QFile(updPath + updEvlContainerName).remove();
}

void UpdateThread::sizeFiles(QDir dir){
    dir.setFilter(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    dir.setSorting(QDir::Type);
    foreach (QFileInfo fileInfo, dir.entryInfoList()){
        if (fileInfo.isFile()) contentSize += fileInfo.size();
        else if (fileInfo.isDir()) sizeFiles(QDir(fileInfo.absoluteFilePath()));
    }
}

void UpdateThread::transferFiles(QDir dir){
    dir.setFilter(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    dir.setSorting(QDir::Type);
    QFileInfoList content = dir.entryInfoList();

    foreach (QFileInfo fileInfo, content){
        QString filePath = fileInfo.absoluteFilePath().replace(updPath , "");
        QString updFilePath = updPath + filePath.toLower();
        QString appFilePath = appPath + filePath.toLower();
        if (fileInfo.isFile()){
            if (QFile::exists(appFilePath)){
                QFile::remove(appFilePath);
            }
            QFile::copy(updFilePath, appFilePath);
            offset += fileInfo.size();
            emit updateProgress(round(float(offset) / float(contentSize) * 100));
        }
        else if (fileInfo.isDir()){
            if (!QDir(appFilePath).exists()) QDir().mkpath(appFilePath);
            transferFiles(QDir(updFilePath));
        }
    }
}

void UpdateThread::run(){
    sizeFiles(updPath);
    if(QDir(updPath).exists(updEvlContainerName)){
        Storage updStorage(updPath + updEvlContainerName);
        Storage appStorage(appPath + "container.evl");
        //remaking(&updStorage, &appStorage);
    }
    transferFiles(updPath);

    while (QDir(updPath).exists()){
        QDir(updPath).removeRecursively();
    }

    QProcess update;
    update.startDetached("Academia.exe", QStringList() << appArgs);
    QApplication::quit();
}
