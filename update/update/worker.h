#ifndef WORKER_H
#define WORKER_H
#include <ui_dialog.h>


class Worker : public QObject{
    Q_OBJECT

    Ui::Dialog *ui;

public:
    Worker(Ui::Dialog *ui);
public slots:
    void setProgressBarValue(int num);
};

#endif // WORKER_H
